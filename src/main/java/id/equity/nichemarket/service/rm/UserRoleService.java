package id.equity.nichemarket.service.rm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.rm.UserRole.CreateUserRole;
import id.equity.nichemarket.dto.rm.UserRole.UserRoleDto;
import id.equity.nichemarket.model.rm.UserRole;
import id.equity.nichemarket.repository.rm.UserRoleRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class UserRoleService {

	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Channel
	public ResponseEntity<List<UserRoleDto>> listUserRole(){
		List<UserRole> lsUserRole= userRoleRepository.findAll();
		Type targetType = new TypeToken<List<UserRoleDto>>() {}.getType();
		List<UserRoleDto> response = modelMapper.map(lsUserRole, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Channel By Id
	public ResponseEntity<UserRoleDto> getUserRoleById(Long id) {
		UserRole userRole = userRoleRepository.findById(id).orElseThrow(()-> new NotFoundException("User Role id " + id + "is not exist"));
		UserRoleDto response = modelMapper.map(userRole, UserRoleDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Channel
	public ResponseEntity<UserRoleDto> addUserRole(CreateUserRole newUserRole) {
		UserRole userRole = modelMapper.map(newUserRole, UserRole.class);
		UserRole userChannelSaved = userRoleRepository.save(userRole);
		UserRoleDto response = modelMapper.map(userChannelSaved, UserRoleDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Channel
	public ResponseEntity<UserRoleDto> editUserRole(CreateUserRole updateUserRole, Long id) {
		UserRole userRole = userRoleRepository.findById(id).orElseThrow(()-> new NotFoundException("User Role id " + id + "is not exist"));
		userRole.setUserRoleCode(updateUserRole.getUserRoleCode());
		userRole.setUsername(updateUserRole.getUsername());
		userRole.setRoleCode(updateUserRole.getRoleCode());
		userRole.setActive(updateUserRole.isActive());
		userRoleRepository.save(userRole);
		UserRoleDto response = modelMapper.map(userRole, UserRoleDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Channel	
	public ResponseEntity<UserRoleDto> deleteUserRole(Long id) {
		UserRole findChannel = userRoleRepository.findById(id).orElseThrow(()-> new NotFoundException("User Role id " + id + " is not exist"));
		userRoleRepository.deleteById(id);
		UserRoleDto response = modelMapper.map(findChannel, UserRoleDto.class);

		return ResponseEntity.ok(response);
	}
}