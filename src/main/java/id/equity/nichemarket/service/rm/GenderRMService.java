package id.equity.nichemarket.service.rm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.rm.GenderRM.CreateGenderRM;
import id.equity.nichemarket.dto.rm.GenderRM.GenderRMDto;
import id.equity.nichemarket.dto.si.Gender.CreateGender;
import id.equity.nichemarket.model.rm.GenderRM;
import id.equity.nichemarket.repository.rm.GenderRMRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class GenderRMService {

	@Autowired
	private GenderRMRepository genderRMRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Gender
	public ResponseEntity<List<GenderRMDto>> listGender(){
		List<GenderRM> lsGender= genderRMRepository.findAll();
		Type targetType = new TypeToken<List<GenderRMDto>>() {}.getType();
		List<GenderRMDto> response = modelMapper.map(lsGender, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get Gender By Id
	public ResponseEntity<GenderRMDto> getGenderById(Long id) {
		GenderRM genderRM = genderRMRepository.findById(id).orElseThrow(()-> new NotFoundException("Gender id " + id + "is not exist"));
		GenderRMDto response = modelMapper.map(genderRM, GenderRMDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Gender
	public ResponseEntity<GenderRMDto> addGender(CreateGender newGender) {
		GenderRM genderRM = modelMapper.map(newGender, GenderRM.class);
		GenderRM genderSaved = genderRMRepository.save(genderRM);
		GenderRMDto response = modelMapper.map(genderSaved, GenderRMDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Gender
	public ResponseEntity<GenderRMDto> editGender(CreateGenderRM updateGender, Long id) {
		GenderRM genderRM = genderRMRepository.findById(id).orElseThrow(()-> new NotFoundException("Gender id " + id + "is not exist"));
		genderRM.setGenderCode(updateGender.getGenderCode());
		genderRM.setDescription(updateGender.getDescription());
		genderRM.setActive(updateGender.isActive());
		genderRMRepository.save(genderRM);
		GenderRMDto response = modelMapper.map(genderRM, GenderRMDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Gender	
	public ResponseEntity<GenderRMDto> deleteGender(Long id) {
		GenderRM findGender = genderRMRepository.findById(id).orElseThrow(()-> new NotFoundException("Gender id " + id + " is not exist"));
		genderRMRepository.deleteById(id);
		GenderRMDto response = modelMapper.map(findGender, GenderRMDto.class);

		return ResponseEntity.ok(response);
	}
}