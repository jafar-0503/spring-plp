package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.Proposal.CreateProposal;
import id.equity.nichemarket.dto.es.Proposal.ProposalDto;
import id.equity.nichemarket.model.es.Proposal;
import id.equity.nichemarket.repository.es.ProposalRepository;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProposalService {
	
	@Autowired
	private ProposalRepository proposalRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
	//List Proposal
	public ResponseEntity<List<ProposalDto>> listProposal() {
		List<Proposal> listProposals = proposalRepository.findAll();
		Type targetType = new TypeToken <List<ProposalDto>>() {}.getType();
		List<ProposalDto> response = modelMapper.map(listProposals, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List Proposal ById
	public ResponseEntity<ProposalDto> getProposalById(Long id) {
		Proposal proposal = proposalRepository.findById(id).orElseThrow(()-> new NotFoundException("Proposal id " + id + " is not exist"));
		ProposalDto response = modelMapper.map(proposal, ProposalDto.class);

		return ResponseEntity.ok(response);
	}

	@Transactional
	public ResponseEntity<ProposalDto> addProposal(CreateProposal newProposal) {
		Proposal proposal = modelMapper.map(newProposal, Proposal.class);
		Proposal proposalSaved = proposalRepository.save(proposal);
		em.refresh(proposalSaved);
		ProposalDto response = modelMapper.map(proposalSaved, ProposalDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit Proposal
	public ResponseEntity<ProposalDto> editProposal(CreateProposal updateProposal, Long id) {
		Proposal proposal = proposalRepository.findById(id).orElseThrow(()-> new NotFoundException("Proposal id " + id + " is not exist"));
		
		//manual map
		proposal.setProposalNo(updateProposal.getProposalNo());
		proposal.setActive(updateProposal.isActive());
		
		proposalRepository.save(proposal);
		ProposalDto response = modelMapper.map(proposal, ProposalDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete Proposal
	public ResponseEntity<ProposalDto> deleteProposal(Long id) {
		Proposal proposal = proposalRepository.findById(id).orElseThrow(()-> new NotFoundException("Proposal id " + id + " is not exist"));
		
		proposalRepository.deleteById(id);
		ProposalDto response = modelMapper.map(proposal, ProposalDto.class);

		return ResponseEntity.ok(response);
	}
}