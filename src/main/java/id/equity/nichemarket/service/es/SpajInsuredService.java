package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajInsured.CreateSpajInsured;
import id.equity.nichemarket.dto.es.SpajInsured.SpajInsuredDto;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.model.es.SpajInsured;
import id.equity.nichemarket.repository.es.SpajInsuredRepository;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionBaseData;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajInsuredService {
    @Autowired
    private SpajInsuredRepository spajInsuredRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityManager em;

    //Get All Data
    public ResponseEntity<List<SpajInsuredDto>> listSpajInsured() {
        List<SpajInsured> spajInsured= spajInsuredRepository.findAll();
        Type targetType = new TypeToken<List<SpajInsuredDto>>() {}.getType();
        List<SpajInsuredDto> response = modelMapper.map(spajInsured, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajInsuredDto> getSpajInsuredById(Long id) {
        SpajInsured spajInsured = spajInsuredRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Insured id " + id + " is not exist"));
        SpajInsuredDto response = modelMapper.map(spajInsured, SpajInsuredDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajInsuredDto> addSpajInsured(CreateSpajInsured newSpajInsured) {
        SpajInsured spajInsured = modelMapper.map(newSpajInsured, SpajInsured.class);
        SpajInsured spajInsuredSaved = spajInsuredRepository.save(spajInsured);
        SpajInsuredDto response = modelMapper.map(spajInsuredSaved, SpajInsuredDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajInsuredDto> editSpajInsured(CreateSpajInsured updateSpajInsured, Long id) {
        SpajInsured spajInsured = spajInsuredRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Insured id " + id + " is not exist"));
        spajInsured.setSpajPolicyInsuredCode(updateSpajInsured.getSpajPolicyHolderCode());
        spajInsured.setSpajDocumentCode(updateSpajInsured.getSpajDocumentCode());
        spajInsured.setSpajNo(updateSpajInsured.getSpajNo());
        spajInsured.setFullName(updateSpajInsured.getFullName());
        spajInsured.setPlaceOfBirth(updateSpajInsured.getPlaceOfBirth());
        spajInsured.setDateOfBirth(updateSpajInsured.getDateOfBirth());
        spajInsured.setIdentityType(updateSpajInsured.getIdentityType());
        spajInsured.setIdentityNo(updateSpajInsured.getIdentityNo());
        spajInsured.setCitizenship(updateSpajInsured.getCitizenship());
        spajInsured.setNpwpNo(updateSpajInsured.getNpwpNo());
        spajInsured.setGender(updateSpajInsured.getGender());
        spajInsured.setMaritalStatus(updateSpajInsured.getMaritalStatus());
        spajInsured.setReligion(updateSpajInsured.getReligion());
        spajInsured.setReligionOther(updateSpajInsured.getReligionOther());
        spajInsured.setHomeAddress1(updateSpajInsured.getHomeAddress1());
        spajInsured.setHomeAddress2(updateSpajInsured.getHomeAddress2());
        spajInsured.setHomeAddress3(updateSpajInsured.getHomeAddress3());
        spajInsured.setHomeCity(updateSpajInsured.getHomeCity());
        spajInsured.setHomeZipcode(updateSpajInsured.getHomeZipcode());
        spajInsured.setHomeProvince(updateSpajInsured.getHomeProvince());
        spajInsured.setHomeCountry(updateSpajInsured.getHomeCountry());
        spajInsured.setHomePhone(updateSpajInsured.getHomePhone());
        spajInsured.setHomeHandphone(updateSpajInsured.getHomeHandphone());
        spajInsured.setEmailAddress(updateSpajInsured.getEmailAddress());
        spajInsured.setLastEducation(updateSpajInsured.getLastEducation());
        spajInsured.setOffice(updateSpajInsured.getOffice());
        spajInsured.setPosition(updateSpajInsured.getPosition());
        spajInsured.setIndustry(updateSpajInsured.getIndustry());
        spajInsured.setOfficeAddress1(updateSpajInsured.getOfficeAddress1());
        spajInsured.setOfficeAddress2(updateSpajInsured.getOfficeAddress2());
        spajInsured.setOfficeAddress3(updateSpajInsured.getOfficeAddress3());
        spajInsured.setOfficeCity(updateSpajInsured.getOfficeCity());
        spajInsured.setOfficeZipcode(updateSpajInsured.getOfficeZipcode());
        spajInsured.setOfficeProvince(updateSpajInsured.getOfficeProvince());
        spajInsured.setOfficeCountry(updateSpajInsured.getOfficeCountry());
        spajInsured.setOfficePhone(updateSpajInsured.getOfficePhone());
        spajInsured.setOfficeHandphone(updateSpajInsured.getOfficeHandphone());
        spajInsured.setCorrespondenceAddress(updateSpajInsured.getCorrespondenceAddress());
        spajInsured.setOtherCorrespondenceAddress1(updateSpajInsured.getOtherCorrespondenceAddress1());
        spajInsured.setOtherCorrespondenceAddress2(updateSpajInsured.getOtherCorrespondenceAddress2());
        spajInsured.setOtherCorrespondenceAddress3(updateSpajInsured.getOtherCorrespondenceAddress3());
        spajInsured.setOtherCorrespondenceCity(updateSpajInsured.getOtherCorrespondenceCity());
        spajInsured.setOtherCorrespondenceZipcode(updateSpajInsured.getOtherCorrespondenceZipcode());
        spajInsured.setOtherCorrespondenceProvince(updateSpajInsured.getOtherCorrespondenceProvince());
        spajInsured.setOtherCorrespondenceCountry(updateSpajInsured.getOtherCorrespondenceCountry());
        spajInsured.setOtherCorrespondencePhone(updateSpajInsured.getOtherCorrespondencePhone());
        spajInsured.setOtherCorrespondenceHandphone(updateSpajInsured.getOtherCorrespondenceHandphone());
        spajInsured.setJobLevel(updateSpajInsured.getJobLevel());
        spajInsured.setActive(updateSpajInsured.isActive());
        SpajInsured saveDocument = spajInsuredRepository.save(spajInsured);
        em.refresh(saveDocument);
        SpajInsuredDto response = modelMapper.map(spajInsured, SpajInsuredDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SpajInsuredDto> deleteSpajInsured(Long id) {
        SpajInsured spajInsured = spajInsuredRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Insured id " + id + " is not exist"));
        spajInsuredRepository.deleteById(id);
        SpajInsuredDto response = modelMapper.map(spajInsured, SpajInsuredDto.class);

        return ResponseEntity.ok(response);
    }

    //insert data in file json
    @Transactional
    public void savingSpajInsured(SpajDocument spajDocument, ESubmissionBaseData eSubmissionBaseData, String spajCode){

        SpajInsured spajInsured = modelMapper.map(eSubmissionBaseData, SpajInsured.class);
        spajInsured.setFullName(eSubmissionBaseData.getData().getData_calon_tertanggung().getNama_lengkap());
        spajInsured.setPlaceOfBirth(eSubmissionBaseData.getData().getData_calon_tertanggung().getTempat_lahir());
        spajInsured.setDateOfBirth(eSubmissionBaseData.getData().getData_calon_tertanggung().getTanggal_lahir());
        spajInsured.setIdentityType(eSubmissionBaseData.getData().getData_calon_tertanggung().getJenis_identitas());
        spajInsured.setIdentityNo(eSubmissionBaseData.getData().getData_calon_tertanggung().getNo_identitas());
        spajInsured.setCitizenship(eSubmissionBaseData.getData().getData_calon_tertanggung().getKewarganegaraan());
        spajInsured.setNpwpNo(eSubmissionBaseData.getData().getData_calon_tertanggung().getNo_npwp());
        spajInsured.setGender(eSubmissionBaseData.getData().getData_calon_tertanggung().getJenis_kelamin());
        spajInsured.setMaritalStatus(eSubmissionBaseData.getData().getData_calon_tertanggung().getStatus_kawin());
        spajInsured.setReligion(eSubmissionBaseData.getData().getData_calon_tertanggung().getAgama().getAgama());
        spajInsured.setReligionOther(eSubmissionBaseData.getData().getData_calon_tertanggung().getAgama().getLainnya());
        spajInsured.setHomeAddress1(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_rumah().getAlamat_rumah_1());
        spajInsured.setHomeAddress2(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_rumah().getAlamat_rumah_2());
        spajInsured.setHomeAddress3(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_rumah().getAlamat_rumah_3());
        spajInsured.setHomeCity(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_rumah().getKota());
        spajInsured.setHomeZipcode(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_rumah().getKode_pos());
        spajInsured.setHomeProvince(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_rumah().getKode_provinsi());
        spajInsured.setHomeCountry(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_rumah().getKode_negara());
        spajInsured.setHomePhone(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_rumah().getTelp());
        spajInsured.setHomeHandphone(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_rumah().getHp());
        spajInsured.setEmailAddress(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_email());
        spajInsured.setLastEducation(eSubmissionBaseData.getData().getData_calon_tertanggung().getPendidikan_formal_terakhir());
        spajInsured.setOffice(eSubmissionBaseData.getData().getData_calon_tertanggung().getPekerjaan());
        spajInsured.setPosition(eSubmissionBaseData.getData().getData_calon_tertanggung().getJabatan());
        spajInsured.setIndustry(eSubmissionBaseData.getData().getData_calon_tertanggung().getIndustri());
        spajInsured.setOfficeAddress1(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_pekerjaan().getAlamat_pekerjaan_1());
        spajInsured.setOfficeAddress2(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_pekerjaan().getAlamat_pekerjaan_2());
        spajInsured.setOfficeAddress3(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_pekerjaan().getAlamat_pekerjaan_3());
        spajInsured.setOfficeCity(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_pekerjaan().getKota());
        spajInsured.setOfficeZipcode(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_pekerjaan().getKode_pos());
        spajInsured.setOfficeProvince(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_pekerjaan().getKode_provinsi());
        spajInsured.setOfficeCountry(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_pekerjaan().getKode_negara());
        spajInsured.setOfficePhone(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_pekerjaan().getTelp());
        spajInsured.setOfficeHandphone(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_pekerjaan().getHp());
        spajInsured.setCorrespondenceAddress(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_korespondensi().getAlamat_korespondensi());
        spajInsured.setOtherCorrespondenceAddress1(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_korespondensi().getLainnya().getAlamat_lainnya_1());
        spajInsured.setOtherCorrespondenceAddress2(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_korespondensi().getLainnya().getAlamat_lainnya_2());
        spajInsured.setOtherCorrespondenceAddress3(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_korespondensi().getLainnya().getAlamat_lainnya_3());
        spajInsured.setOtherCorrespondenceCity(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_korespondensi().getLainnya().getKota());
        spajInsured.setOtherCorrespondenceZipcode(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_korespondensi().getLainnya().getKode_pos());
        spajInsured.setOtherCorrespondenceProvince(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_korespondensi().getLainnya().getKode_provinsi());
        spajInsured.setOtherCorrespondenceCountry(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_korespondensi().getLainnya().getKode_negara());
        spajInsured.setOtherCorrespondencePhone(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_korespondensi().getLainnya().getTelp());
        spajInsured.setOtherCorrespondenceHandphone(eSubmissionBaseData.getData().getData_calon_tertanggung().getAlamat_korespondensi().getLainnya().getHp());
        spajInsured.setJobLevel(eSubmissionBaseData.getData().getData_calon_tertanggung().getKelas_pekerjaan());
        spajInsured.setSpajNo(spajCode);

        spajInsured.setSpajDocumentCode(spajDocument.getSpajDocumentCode());
        spajInsured.setActive(true);
        spajInsuredRepository.save(spajInsured);
    }
}