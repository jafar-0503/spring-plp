package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.Spaj.CreateSpaj;
import id.equity.nichemarket.dto.es.Spaj.SpajDto;
import id.equity.nichemarket.model.es.Spaj;
import id.equity.nichemarket.repository.es.SpajRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajService {

    @Autowired
    private SpajRepository spajRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityManager em;

    //Get Spaj
    public ResponseEntity<List<SpajDto>> listSpaj() {
        List<Spaj> listSpajs = spajRepository.findAll();
        Type targetType = new TypeToken <List<SpajDto>>() {}.getType();
        List<SpajDto> response = modelMapper.map(listSpajs, targetType);
        return ResponseEntity.ok(response);
    }

    //Get Spaj ById
    public ResponseEntity<SpajDto> getSpajById(Long id) {
        Spaj spaj = spajRepository.findById(id).orElseThrow(()-> new NotFoundException("Spaj id " + id + " is not exist"));
        SpajDto response = modelMapper.map(spaj, SpajDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Spaj
    @Transactional
    public ResponseEntity<SpajDto> addSpaj(CreateSpaj newSpaj) {
        Spaj spaj = modelMapper.map(newSpaj, Spaj.class);
        Spaj spajSaved = spajRepository.save(spaj);
        em.refresh(spajSaved);
        SpajDto response = modelMapper.map(spajSaved, SpajDto.class);
        return ResponseEntity.ok(response);
    }

    //Edit Spaj
    public ResponseEntity<SpajDto> editSpaj(CreateSpaj updateSpaj, Long id) {
        Spaj spaj = spajRepository.findById(id).orElseThrow(()-> new NotFoundException("Spaj id " + id + " is not exist"));

        //manual map
        spaj.setSpajCode(updateSpaj.getSpajCode());
        spaj.setActive(updateSpaj.isActive());

        spajRepository.save(spaj);
        SpajDto response = modelMapper.map(spaj, SpajDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Spaj
    public ResponseEntity<SpajDto> deleteSpaj(Long id) {
        Spaj spaj = spajRepository.findById(id).orElseThrow(()-> new NotFoundException("Spaj id " + id + " is not exist"));

        spajRepository.deleteById(id);
        SpajDto response = modelMapper.map(spaj, SpajDto.class);
        return ResponseEntity.ok(response);
    }

    public SpajDto findLastOne() {
        Spaj spajDto = spajRepository.findTopByOrderByIdDesc();
        return modelMapper.map(spajDto, SpajDto.class);
    }
}