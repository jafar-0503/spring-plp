package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.Currency.CurrencyDto;
import id.equity.nichemarket.dto.es.Currency.CreateCurrency;
import id.equity.nichemarket.model.es.Currency;
import id.equity.nichemarket.repository.es.CurrencyRespository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class CurrencyService {
    @Autowired 
    private CurrencyRespository currencyRespository;
    
    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<CurrencyDto>> listCurrency() {
        List<Currency> currency= currencyRespository.findAll();
        Type targetType = new TypeToken<List<CurrencyDto>>() {}.getType();
        List<CurrencyDto> response = modelMapper.map(currency, targetType);


        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<CurrencyDto> getCurrencyById(Long id) {
        Currency currency = currencyRespository.findById(id).orElseThrow(()-> new NotFoundException("Currency id " + id + " is not exist"));
        CurrencyDto response = modelMapper.map(currency, CurrencyDto.class);


        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<CurrencyDto> addCurrency(CreateCurrency newCurrency) {
        Currency currency = modelMapper.map(newCurrency, Currency.class);
        Currency currencySaved = currencyRespository.save(currency);
        CurrencyDto response = modelMapper.map(currencySaved, CurrencyDto.class);


        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<CurrencyDto> editCurrency(CreateCurrency updateCurrency, Long id) {
        Currency currency = currencyRespository.findById(id).orElseThrow(()-> new NotFoundException("Currency id " + id + " is not exist"));
        currency.setCurrencyCode(updateCurrency.getCurrencyCode());
        currency.setDescription(updateCurrency.getDescription());
        currency.setActive(updateCurrency.isActive());

        currencyRespository.save(currency);
        CurrencyDto response = modelMapper.map(currency, CurrencyDto.class);


        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<CurrencyDto> deleteCurrency(Long id) {
        Currency currency = currencyRespository.findById(id).orElseThrow(()-> new NotFoundException("Currency id " + id + " is not exist"));
        currencyRespository.deleteById(id);
        CurrencyDto response = modelMapper.map(currency, CurrencyDto.class);


        return ResponseEntity.ok(response);
    }
}