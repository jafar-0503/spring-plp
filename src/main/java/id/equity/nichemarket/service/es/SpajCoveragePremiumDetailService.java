package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajCoveragePremiumDetail.CreateSpajCoveragePremiumDetail;
import id.equity.nichemarket.dto.es.SpajCoveragePremiumDetail.SpajCoveragePremiumDetailDto;
import id.equity.nichemarket.model.es.SpajCoveragePremiumDetail;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.repository.es.SpajCoveragePremiumDetailRepository;
import id.equity.nichemarket.service.eSubmissionStore.AsuransiTambahan;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionBaseData;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajCoveragePremiumDetailService {
    @Autowired
    private SpajCoveragePremiumDetailRepository spajCoveragePremiumDetailRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityManager em;

    //Get All Data
    public ResponseEntity<List<SpajCoveragePremiumDetailDto>> listSpajCoveragePremiumDetail() {
        List<SpajCoveragePremiumDetail> spajCoveragePremiumDetail= spajCoveragePremiumDetailRepository.findAll();
        Type targetType = new TypeToken<List<SpajCoveragePremiumDetailDto>>() {}.getType();
        List<SpajCoveragePremiumDetailDto> response = modelMapper.map(spajCoveragePremiumDetail, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajCoveragePremiumDetailDto> getSpajCoveragePremiumDetailById(Long id) {
        SpajCoveragePremiumDetail spajCoveragePremiumDetail = spajCoveragePremiumDetailRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Coverage Premium Detail id " + id + " is not exist"));
        SpajCoveragePremiumDetailDto response = modelMapper.map(spajCoveragePremiumDetail, SpajCoveragePremiumDetailDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajCoveragePremiumDetailDto> addSpajCoveragePremiumDetail(CreateSpajCoveragePremiumDetail newSpajCoveragePremiumDetail) {
        SpajCoveragePremiumDetail spajCoveragePremiumDetail = modelMapper.map(newSpajCoveragePremiumDetail, SpajCoveragePremiumDetail.class);
        SpajCoveragePremiumDetail SpajCoveragePremiumDetailSaved = spajCoveragePremiumDetailRepository.save(spajCoveragePremiumDetail);
        SpajCoveragePremiumDetailDto response = modelMapper.map(SpajCoveragePremiumDetailSaved, SpajCoveragePremiumDetailDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajCoveragePremiumDetailDto> editSpajCoveragePremiumDetail(CreateSpajCoveragePremiumDetail updateSpajCoveragePremiumDetail, Long id) {
        SpajCoveragePremiumDetail spajCoveragePremiumDetail = spajCoveragePremiumDetailRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Coverage Premium Detail id " + id + " is not exist"));
        spajCoveragePremiumDetail.setSpajCoveragePremiumDetailCode(updateSpajCoveragePremiumDetail.getSpajCoveragePremiumDetailCode());
        spajCoveragePremiumDetail.setSpajDocumentCode(updateSpajCoveragePremiumDetail.getSpajDocumentCode());
        spajCoveragePremiumDetail.setSpajNo(updateSpajCoveragePremiumDetail.getSpajNo());
        spajCoveragePremiumDetail.setCurrency(updateSpajCoveragePremiumDetail.getCurrency());
        spajCoveragePremiumDetail.setPremiumPaymentPeriod(updateSpajCoveragePremiumDetail.getPremiumPaymentPeriod());
        spajCoveragePremiumDetail.setPUnitLinkPremium(updateSpajCoveragePremiumDetail.getPUnitLinkPremium());
        spajCoveragePremiumDetail.setPUnitLinkTotalPremium(updateSpajCoveragePremiumDetail.getPUnitLinkTotalPremium());
        spajCoveragePremiumDetail.setPUnitLinkBasicPlan(updateSpajCoveragePremiumDetail.getPUnitLinkBasicPlan());
        spajCoveragePremiumDetail.setPUnitLinkSumInsured(updateSpajCoveragePremiumDetail.getPUnitLinkSumInsured());
        spajCoveragePremiumDetail.setPUnitLinkPremiumPaymentPeriod(updateSpajCoveragePremiumDetail.getPUnitLinkPremiumPaymentPeriod());
        spajCoveragePremiumDetail.setPUnitLinkContractPeriod(updateSpajCoveragePremiumDetail.getPUnitLinkContractPeriod());
        spajCoveragePremiumDetail.setPUnitLinkRider1(updateSpajCoveragePremiumDetail.getPUnitLinkRider1());
        spajCoveragePremiumDetail.setPUnitLinkSumInsuredRider1(updateSpajCoveragePremiumDetail.getPUnitLinkSumInsuredRider1());
        spajCoveragePremiumDetail.setActive(updateSpajCoveragePremiumDetail.isActive());
        spajCoveragePremiumDetailRepository.save(spajCoveragePremiumDetail);
        em.refresh(spajCoveragePremiumDetail);
        SpajCoveragePremiumDetailDto response = modelMapper.map(spajCoveragePremiumDetail, SpajCoveragePremiumDetailDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SpajCoveragePremiumDetailDto> deleteSpajCoveragePremiumDetail(Long id) {
        SpajCoveragePremiumDetail spajCoveragePremiumDetail = spajCoveragePremiumDetailRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Coverage Premium Detail id " + id + " is not exist"));
        spajCoveragePremiumDetailRepository.deleteById(id);
        SpajCoveragePremiumDetailDto response = modelMapper.map(spajCoveragePremiumDetail, SpajCoveragePremiumDetailDto.class);

        return ResponseEntity.ok(response);
    }

    //insert data in file json
    @Transactional
    public void savingSpajCoverage(SpajDocument spajDocument, ESubmissionBaseData eSubmissionBaseData, String spajCode){
        //master Spaj Coverage Premium Detail
        SpajCoveragePremiumDetail spajCoveragePremiumDetail = modelMapper.map(eSubmissionBaseData.getData().getPerincian_pertanggungan_dan_premi(), SpajCoveragePremiumDetail.class);
        spajCoveragePremiumDetail.setCurrency(eSubmissionBaseData.getData().getPerincian_pertanggungan_dan_premi().getMata_uang());
        spajCoveragePremiumDetail.setPremiumPaymentPeriod(eSubmissionBaseData.getData().getPerincian_pertanggungan_dan_premi().getPeriode_pembayaran_premi());
        spajCoveragePremiumDetail.setPUnitLinkPremium(eSubmissionBaseData.getData().getPerincian_pertanggungan_dan_premi().getPremi());
        spajCoveragePremiumDetail.setPUnitLinkTotalPremium(eSubmissionBaseData.getData().getPerincian_pertanggungan_dan_premi().getTotal_premi());
        spajCoveragePremiumDetail.setPUnitLinkBasicPlan(eSubmissionBaseData.getData().getPerincian_pertanggungan_dan_premi().getAsuransi_dasar());
        spajCoveragePremiumDetail.setPUnitLinkSumInsured(eSubmissionBaseData.getData().getPerincian_pertanggungan_dan_premi().getUang_pertanggungan());
        spajCoveragePremiumDetail.setPUnitLinkPremiumPaymentPeriod(eSubmissionBaseData.getData().getPerincian_pertanggungan_dan_premi().getMpp());
        spajCoveragePremiumDetail.setPUnitLinkContractPeriod(eSubmissionBaseData.getData().getPerincian_pertanggungan_dan_premi().getMk());

        //arrary asuransi tambahan master table spaj coverage premium detail
        for (AsuransiTambahan asuransiTambahan: eSubmissionBaseData.getData().getPerincian_pertanggungan_dan_premi().getAsuransi_tambahan()) {
            spajCoveragePremiumDetail.setActive(true);
            spajCoveragePremiumDetail.setPUnitLinkRider1(asuransiTambahan.getAsuransi_tambahan());
            spajCoveragePremiumDetail.setPUnitLinkSumInsuredRider1(asuransiTambahan.getUang_pertanggungan());

            spajCoveragePremiumDetail.setSpajDocumentCode(spajDocument.getSpajDocumentCode());
            spajCoveragePremiumDetail.setSpajNo(spajCode);
            spajCoveragePremiumDetailRepository.save(spajCoveragePremiumDetail);
        }
    }
}