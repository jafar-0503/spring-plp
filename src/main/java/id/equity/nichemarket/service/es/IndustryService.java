package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.Industry.CreateIndustry;
import id.equity.nichemarket.dto.es.Industry.IndustryDto;
import id.equity.nichemarket.model.es.Industry;
import id.equity.nichemarket.repository.es.IndustryRepository;

@Service
public class IndustryService {
	
	@Autowired
	private IndustryRepository industryRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List All Industry
	public ResponseEntity<List<IndustryDto>> listIndustry() {
		List<Industry> listIndustrys = industryRepository.findAll();
		Type targetType = new TypeToken <List<IndustryDto>>() {}.getType();
		List<IndustryDto> response = modelMapper.map(listIndustrys, targetType);

		return ResponseEntity.ok(response);
	}

	//List Industry By Id
	public ResponseEntity<IndustryDto> getIndustryById(Long id) {
		Industry industry = industryRepository.findById(id).orElseThrow(()-> new NotFoundException("Industry id " + id + " is not exist"));
		IndustryDto response = modelMapper.map(industry, IndustryDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create Industry
	public ResponseEntity<IndustryDto> addIndustry(CreateIndustry newIndustry) {
		Industry industry = modelMapper.map(newIndustry, Industry.class);
		Industry IndustrySaved = industryRepository.save(industry);
		IndustryDto response = modelMapper.map(IndustrySaved, IndustryDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit Industry
	public ResponseEntity<IndustryDto> editIndustry(CreateIndustry updateIndustry, Long id) {
		Industry industry = industryRepository.findById(id).orElseThrow(()-> new NotFoundException("Industry id " + id + " is not exist"));
		
		//manual map
		industry.setIndustryCode(updateIndustry.getIndustryCode());
		industry.setDescription(updateIndustry.getDescription());
		industry.setActive(updateIndustry.isActive());
		
		industryRepository.save(industry);
		IndustryDto response = modelMapper.map(industry, IndustryDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete Industry
	public ResponseEntity<IndustryDto> deleteIndustry(Long id) {
		Industry industry = industryRepository.findById(id).orElseThrow(()-> new NotFoundException("Industry id " + id + " is not exist"));
		
		industryRepository.deleteById(id);
		IndustryDto response = modelMapper.map(industry, IndustryDto.class);

		return ResponseEntity.ok(response);
	}
}