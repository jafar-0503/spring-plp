package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;
import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.PaymentMethod.CreatePaymentMethod;
import id.equity.nichemarket.dto.es.PaymentMethod.PaymentMethodDto;
import id.equity.nichemarket.model.es.PaymentMethod;
import id.equity.nichemarket.repository.es.PaymentMethodRepository;

@Service
public class PaymentMethodService {
	
	@Autowired
	private PaymentMethodRepository paymentMethodRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Payment Method
	public ResponseEntity<List<PaymentMethodDto>> listPaymentMethod() {
		List<PaymentMethod> listPaymentMethods = paymentMethodRepository.findAll();
		Type targetType = new TypeToken <List<PaymentMethodDto>>() {}.getType();
		List<PaymentMethodDto> response = modelMapper.map(listPaymentMethods, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List Payment Method By Id
	public ResponseEntity<PaymentMethodDto> getpaymentMethodById(Long id) {
		PaymentMethod paymentMethod = paymentMethodRepository.findById(id).orElseThrow(()-> new NotFoundException("Payment Method id " + id + " is not exist"));
		PaymentMethodDto response = modelMapper.map(paymentMethod, PaymentMethodDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create Payment Method
	public ResponseEntity<PaymentMethodDto> addPaymentMethod(CreatePaymentMethod newPaymentMethod) {
		PaymentMethod paymentMethod = modelMapper.map(newPaymentMethod, PaymentMethod.class);
		PaymentMethod paymentMethodSaved = paymentMethodRepository.save(paymentMethod);
		PaymentMethodDto response = modelMapper.map(paymentMethodSaved, PaymentMethodDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit PaymentMethod
	public ResponseEntity<PaymentMethodDto> editPaymentMethod(CreatePaymentMethod updatePaymentMethod, Long id) {
		PaymentMethod paymentMethod = paymentMethodRepository.findById(id).orElseThrow(()-> new NotFoundException("Payment Method id " + id + " is not exist"));
		
		//manual map
		paymentMethod.setPaymentMethodCode(updatePaymentMethod.getPaymentMethodCode());
		paymentMethod.setDescription(updatePaymentMethod.getDescription());
		paymentMethod.setActive(updatePaymentMethod.isActive());
		
		paymentMethodRepository.save(paymentMethod);
		PaymentMethodDto response = modelMapper.map(paymentMethod, PaymentMethodDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete PaymentMethod
	public ResponseEntity<PaymentMethodDto> deletePaymentMethod(Long id) {
		PaymentMethod paymentMethod = paymentMethodRepository.findById(id).orElseThrow(()-> new NotFoundException("Payment Method id " + id + " is not exist"));
		
		paymentMethodRepository.deleteById(id);
		PaymentMethodDto response = modelMapper.map(paymentMethod, PaymentMethodDto.class);

		return ResponseEntity.ok(response);
	}
}