package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;

import java.util.List;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.RecurringType.CreateRecurringType;
import id.equity.nichemarket.dto.es.RecurringType.RecurringTypeDto;
import id.equity.nichemarket.model.es.RecurringType;
import id.equity.nichemarket.repository.es.RecurringTypeRepository;

@Service
public class RecurringTypeService {
	
	@Autowired
	private RecurringTypeRepository recurringTypeRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List All RecurringType
	public ResponseEntity<List<RecurringTypeDto>> listRecurringType() {
		List<RecurringType> listRecurringTypes = recurringTypeRepository.findAll();
		Type targetType = new TypeToken <List<RecurringTypeDto>>() {}.getType();
		List<RecurringTypeDto> response = modelMapper.map(listRecurringTypes, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List RecurringType By Id
	public ResponseEntity<RecurringTypeDto> getRecurringTypeById(Long id) {
		RecurringType recurringType = recurringTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Recurring Type id " + id + " is not exist"));
		RecurringTypeDto response = modelMapper.map(recurringType, RecurringTypeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create RecurringType
	public ResponseEntity<RecurringTypeDto> addRecurringType(CreateRecurringType newRecurringType) {
		RecurringType recurringType = modelMapper.map(newRecurringType, RecurringType.class);
		RecurringType recurringTypeSaved = recurringTypeRepository.save(recurringType);
		RecurringTypeDto response = modelMapper.map(recurringTypeSaved, RecurringTypeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit RecurringType
	public ResponseEntity<RecurringTypeDto> editRecurringType(CreateRecurringType updateRecurringType, Long id) {
		RecurringType recurringType = recurringTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Recurring Type id " + id + " is not exist"));
		
		//manual map
		recurringType.setRecurringTypeCode(updateRecurringType.getRecurringTypeCode());
		recurringType.setDescription(updateRecurringType.getDescription());
		recurringType.setActive(updateRecurringType.isActive());
		
		recurringTypeRepository.save(recurringType);
		RecurringTypeDto response = modelMapper.map(recurringType, RecurringTypeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete RecurringType
	public ResponseEntity<RecurringTypeDto> deleteRecurringType(Long id) {
		RecurringType recurringType = recurringTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Recurring Type id " + id + " is not exist"));
		
		recurringTypeRepository.deleteById(id);
		RecurringTypeDto response = modelMapper.map(recurringType, RecurringTypeDto.class);

		return ResponseEntity.ok(response);
	}
}