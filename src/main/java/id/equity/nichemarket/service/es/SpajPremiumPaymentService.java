package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.AgenRelation.AgenRelationDto;
import id.equity.nichemarket.dto.es.SpajPremiumPayment.CreateSpajPremiumPayment;
import id.equity.nichemarket.dto.es.SpajPremiumPayment.SpajPremiumPaymentDto;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.model.es.SpajPremiumPayment;
import id.equity.nichemarket.repository.es.SpajPremiumPaymentRespository;

import id.equity.nichemarket.service.eSubmissionStore.ESubmissionBaseData;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajPremiumPaymentService {
    
	@Autowired 
    private SpajPremiumPaymentRespository spajPremiumPaymentRespository;
    
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityManager em;

    //Get All Data
    public ResponseEntity<List<SpajPremiumPaymentDto>> listSpajPremiumPayment() {
    	List<SpajPremiumPayment> listSpajPremiumPayments = spajPremiumPaymentRespository.findAll();
		Type targetType = new TypeToken <List<AgenRelationDto>>() {}.getType();
		List<SpajPremiumPaymentDto> response = modelMapper.map(listSpajPremiumPayments, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajPremiumPaymentDto> getSpajPremiumPaymentById(Long id) {
        SpajPremiumPayment spajPremiumPayment = spajPremiumPaymentRespository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Premium Payment id " + id + " is not exist"));
        SpajPremiumPaymentDto response = modelMapper.map(spajPremiumPayment, SpajPremiumPaymentDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajPremiumPaymentDto> addSpajPremiumPayment(CreateSpajPremiumPayment newSpajPremiumPayment) {
        SpajPremiumPayment spajPremiumPayment = modelMapper.map(newSpajPremiumPayment, SpajPremiumPayment.class);
        SpajPremiumPayment spajPremiumPaymentSaved = spajPremiumPaymentRespository.save(spajPremiumPayment);
        SpajPremiumPaymentDto response = modelMapper.map(spajPremiumPaymentSaved, SpajPremiumPaymentDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajPremiumPaymentDto> editSpajPremiumPayment(CreateSpajPremiumPayment updateSpajPremiumPayment, Long id) {
        SpajPremiumPayment spajPremiumPayment = spajPremiumPaymentRespository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Premium Payment id " + id + " is not exist"));
        
        //manual map
        spajPremiumPayment.setSpajPremiumPaymentCode(updateSpajPremiumPayment.getSpajPremiumPaymentCode());
        spajPremiumPayment.setSpajDocumentCode(updateSpajPremiumPayment.getSpajDocumentCode());
        spajPremiumPayment.setSpajNo(updateSpajPremiumPayment.getSpajNo());
        spajPremiumPayment.setPremiumPaymentMethod(updateSpajPremiumPayment.getPremiumPaymentMethod());
        spajPremiumPayment.setBankName(updateSpajPremiumPayment.getBankName());
        spajPremiumPayment.setOtherBank(updateSpajPremiumPayment.getOtherBank());
        
        spajPremiumPaymentRespository.save(spajPremiumPayment);
        em.refresh(spajPremiumPayment);
        SpajPremiumPaymentDto response = modelMapper.map(spajPremiumPayment, SpajPremiumPaymentDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SpajPremiumPaymentDto> deleteSpajPremiumPayment(Long id) {
        SpajPremiumPayment spajPremiumPayment = spajPremiumPaymentRespository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Premium Payment id " + id + " is not exist"));
        
        spajPremiumPaymentRespository.deleteById(id);
        SpajPremiumPaymentDto response = modelMapper.map(spajPremiumPayment, SpajPremiumPaymentDto.class);

        return ResponseEntity.ok(response);
    }

    //insert data in file json
    @Transactional
    public void savingSpajPremium(SpajDocument spajDocument, ESubmissionBaseData eSubmissionBaseData, String spajCode){
        //master Spaj Premium Payment
        SpajPremiumPayment spajPremiumPayment = modelMapper.map(eSubmissionBaseData, SpajPremiumPayment.class);
        spajPremiumPayment.setPremiumPaymentMethod(eSubmissionBaseData.getData().getPembayaran_premi().getCara_pembayaran_premi_lanjutan());
        spajPremiumPayment.setBankName(eSubmissionBaseData.getData().getPembayaran_premi().getNama_bank().getNama_bank());
        spajPremiumPayment.setOtherBank(eSubmissionBaseData.getData().getPembayaran_premi().getNama_bank().getLainnya());
        spajPremiumPayment.setActive(true);
        spajPremiumPayment.setSpajDocumentCode(spajDocument.getSpajDocumentCode());
        spajPremiumPayment.setSpajNo(spajCode);
        spajPremiumPaymentRespository.save(spajPremiumPayment);
    }
}