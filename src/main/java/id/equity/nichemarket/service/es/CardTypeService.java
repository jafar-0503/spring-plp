package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.CardType.CardTypeDto;
import id.equity.nichemarket.dto.es.CardType.CreateCardType;
import id.equity.nichemarket.model.es.CardType;
import id.equity.nichemarket.repository.es.CardTypeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class CardTypeService {
	
	@Autowired
	private CardTypeRepository cardTypeRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List All CardType
	public ResponseEntity<List<CardTypeDto>> listCardType() {
		List<CardType> listCardTypes = cardTypeRepository.findAll();
		Type targetType = new TypeToken <List<CardTypeDto>>() {}.getType();
		List<CardTypeDto> response = modelMapper.map(listCardTypes, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List CardType By Id
	public ResponseEntity<CardTypeDto> getCardTypeById(Long id) {
		CardType CardTypeDto = cardTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Card Type id " + id + " is not exist"));
		CardTypeDto response = modelMapper.map(CardTypeDto, CardTypeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create CardType
	public ResponseEntity<CardTypeDto> addCardType(CreateCardType newCardType) {
		CardType CardType = modelMapper.map(newCardType, CardType.class);
		CardType CardTypeSaved = cardTypeRepository.save(CardType);
		CardTypeDto response = modelMapper.map(CardTypeSaved, CardTypeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit CardType
	public ResponseEntity<CardTypeDto> editCardType(CreateCardType updateCardType, Long id) {
		CardType CardType = cardTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Card Type id " + id + " is not exist"));
		
		//manual maps
		CardType.setCardTypeCode(updateCardType.getCardTypeCode());
		CardType.setDescription(updateCardType.getDescription());
		CardType.setActive(updateCardType.isActive());
		cardTypeRepository.save(CardType);
		CardTypeDto response = modelMapper.map(CardType, CardTypeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete CardType
	public ResponseEntity<CardTypeDto> deleteCardType(Long id) {
		CardType CardType = cardTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Card Type id " + id + " is not exist"));
		
		cardTypeRepository.deleteById(id);
		CardTypeDto response = modelMapper.map(CardType, CardTypeDto.class);

		return ResponseEntity.ok(response);
	}
}