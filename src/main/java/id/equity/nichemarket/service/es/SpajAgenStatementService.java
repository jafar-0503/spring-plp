package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajAgenStatement.CreateSpajAgenStatement;
import id.equity.nichemarket.dto.es.SpajAgenStatement.SpajAgenStatementDto;
import id.equity.nichemarket.model.es.SpajAgenStatement;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.repository.es.SpajAgenStatementRepository;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionBaseData;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajAgenStatementService {

    @Autowired
    private SpajAgenStatementRepository spajAgenStatementRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityManager em;

    //Get All Data
    public ResponseEntity<List<SpajAgenStatementDto>> listSpajAgenStatement() {
        List<SpajAgenStatement> spajAgenStatement= spajAgenStatementRepository.findAll();
        Type targetType = new TypeToken<List<SpajAgenStatementDto>>() {}.getType();
        List<SpajAgenStatementDto> response = modelMapper.map(spajAgenStatement, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajAgenStatementDto> getSpajAgenStatementById(Long id) {
        SpajAgenStatement spajAgenStatement = spajAgenStatementRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Agen Statement id " + id + " is not exist"));
        SpajAgenStatementDto response = modelMapper.map(spajAgenStatement, SpajAgenStatementDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajAgenStatementDto> addSpajAgenStatement(CreateSpajAgenStatement newSpajAgenStatement) {
        SpajAgenStatement spajAgenStatement = modelMapper.map(newSpajAgenStatement, SpajAgenStatement.class);
        SpajAgenStatement spajAgenStatementSaved = spajAgenStatementRepository.save(spajAgenStatement);
        em.refresh(spajAgenStatement);
        SpajAgenStatementDto response = modelMapper.map(spajAgenStatementSaved, SpajAgenStatementDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajAgenStatementDto> editSpajAgenStatement(CreateSpajAgenStatement updateSpajAgenStatement, Long id) {
        SpajAgenStatement spajAgenStatement = spajAgenStatementRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Agen Statement id " + id + " is not exist"));
        spajAgenStatement.setSpajAgenStatementCode(updateSpajAgenStatement.getSpajAgenStatementCode());
        spajAgenStatement.setSpajDocumentCode(updateSpajAgenStatement.getSpajDocumentCode());
        spajAgenStatement.setSpajNo(updateSpajAgenStatement.getSpajNo());
        spajAgenStatement.setAgenName(updateSpajAgenStatement.getAgenName());
        spajAgenStatement.setAgenCode(updateSpajAgenStatement.getAgenCode());
        spajAgenStatement.setPosition(updateSpajAgenStatement.getPosition());
        spajAgenStatement.setActive(updateSpajAgenStatement.isActive());
        spajAgenStatementRepository.save(spajAgenStatement);
        em.refresh(spajAgenStatement);
        SpajAgenStatementDto response = modelMapper.map(spajAgenStatement, SpajAgenStatementDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SpajAgenStatementDto> deleteSpajAgenStatement(Long id) {
        SpajAgenStatement spajAgenStatement = spajAgenStatementRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Agen Statement id " + id + " is not exist"));
        spajAgenStatementRepository.deleteById(id);
        SpajAgenStatementDto response = modelMapper.map(spajAgenStatement, SpajAgenStatementDto.class);

        return ResponseEntity.ok(response);
    }

    //insert data in file json
    @Transactional
    public void savingSpajAgent(SpajDocument spajDocument, ESubmissionBaseData eSubmissionBaseData, String spajCode){
        //master Spaj Agen Statement
        SpajAgenStatement spajAgenStatement = modelMapper.map(eSubmissionBaseData, SpajAgenStatement.class);
        spajAgenStatement.setAgenName(eSubmissionBaseData.getData().getSurat_pernyataan_agen().getNama_agen());
        spajAgenStatement.setAgenCode(eSubmissionBaseData.getData().getSurat_pernyataan_agen().getKode_agen());
        spajAgenStatement.setPosition(eSubmissionBaseData.getData().getSurat_pernyataan_agen().getJabatan());

        spajAgenStatement.setSpajDocumentCode(spajDocument.getSpajDocumentCode());
        spajAgenStatement.setSpajNo(spajCode);
        spajAgenStatement.setActive(true);
        spajAgenStatementRepository.save(spajAgenStatement);
    }

}