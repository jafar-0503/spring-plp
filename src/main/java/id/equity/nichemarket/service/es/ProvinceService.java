package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.Province.CreateProvince;
import id.equity.nichemarket.dto.es.Province.ProvinceDto;
import id.equity.nichemarket.model.es.Province;
import id.equity.nichemarket.repository.es.ProvinceRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProvinceService {
    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<ProvinceDto>> listProvince() {
        List<Province> lsProvince= provinceRepository.findAll();
        Type targetType = new TypeToken<List<ProvinceDto>>() {}.getType();
        List<ProvinceDto> response = modelMapper.map(lsProvince, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<ProvinceDto> getProvinceById(Long id) {
        Province province = provinceRepository.findById(id).orElseThrow(()-> new NotFoundException("Province id " + id + " is not exist"));
        ProvinceDto response = modelMapper.map(province, ProvinceDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<ProvinceDto> addProvince(CreateProvince newProvince) {
        Province province = modelMapper.map(newProvince, Province.class);
        Province provinceSaved = provinceRepository.save(province);
        ProvinceDto response = modelMapper.map(provinceSaved, ProvinceDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<ProvinceDto> editProvince(CreateProvince updateProvince, Long id) {
        Province province = provinceRepository.findById(id).orElseThrow(()-> new NotFoundException("Province id " + id + " is not exist"));
        province.setProvinceCode(updateProvince.getProvinceCode());
        province.setDescription(updateProvince.getDescription());
        province.setActive(updateProvince.isActive());
        provinceRepository.save(province);
        ProvinceDto response = modelMapper.map(province, ProvinceDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<ProvinceDto> deleteProvince(Long id) {
        Province province = provinceRepository.findById(id).orElseThrow(()-> new NotFoundException("Province id " + id + " is not exist"));
        provinceRepository.deleteById(id);
        ProvinceDto response = modelMapper.map(province, ProvinceDto.class);

        return ResponseEntity.ok(response);
    }
}