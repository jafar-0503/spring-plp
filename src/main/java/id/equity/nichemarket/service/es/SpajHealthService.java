package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajHealth.CreateSpajHealth;
import id.equity.nichemarket.dto.es.SpajHealth.SpajHealthDto;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.model.es.SpajHealth;
import id.equity.nichemarket.repository.es.SpajHealthRepository;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionBaseData;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajHealthService {
    @Autowired
    private SpajHealthRepository spajHealthRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityManager em;

    //Get All Data
    public ResponseEntity<List<SpajHealthDto>> listSpajHealth() {
        List<SpajHealth> spajHealth= spajHealthRepository.findAll();
        Type targetType = new TypeToken<List<SpajHealthDto>>() {}.getType();
        List<SpajHealthDto> response = modelMapper.map(spajHealth, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajHealthDto> getSpajHealthById(Long id) {
        SpajHealth spajHealth = spajHealthRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Health id " + id + " is not exist"));
        SpajHealthDto response = modelMapper.map(spajHealth, SpajHealthDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajHealthDto> addSpajHealth(CreateSpajHealth newspajHealth) {
        SpajHealth spajHealth = modelMapper.map(newspajHealth, SpajHealth.class);
        SpajHealth spajHealthSaved = spajHealthRepository.save(spajHealth);
        SpajHealthDto response = modelMapper.map(spajHealthSaved, SpajHealthDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajHealthDto> editSpajHealth(CreateSpajHealth updatespajHealth, Long id) {
        SpajHealth spajHealth = spajHealthRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Health id " + id + " is not exist"));
        spajHealth.setSpajHealthCode(updatespajHealth.getSpajHealthCode());
        spajHealth.setSpajDocumentCode(updatespajHealth.getSpajDocumentCode());
        spajHealth.setSpajNo(updatespajHealth.getSpajNo());
        spajHealth.setWeightPolicyHolder(updatespajHealth.getWeightPolicyHolder());
        spajHealth.setWeightInsured(updatespajHealth.getWeightInsured());
        spajHealth.setHeightPolicyHolder(updatespajHealth.getHeightPolicyHolder());
        spajHealth.setHeightInsured(updatespajHealth.getHeightInsured());
        spajHealth.setBloodTypePolicyHolder(updatespajHealth.getBloodTypePolicyHolder());
        spajHealth.setBloodTypeInsured(updatespajHealth.getBloodTypeInsured());
        spajHealth.setPolicyHolderQuestion1(updatespajHealth.getPolicyHolderQuestion1());
        spajHealth.setInsuredQuestion1(updatespajHealth.getInsuredQuestion1());
        spajHealth.setPolicyHolderQuestion2A(updatespajHealth.getPolicyHolderQuestion2A());
        spajHealth.setInsuredQuestion2A(updatespajHealth.getInsuredQuestion2A());
        spajHealth.setPolicyHolderQuestion2B(updatespajHealth.getPolicyHolderQuestion2B());
        spajHealth.setInsuredQuestion2B(updatespajHealth.getInsuredQuestion2B());
        spajHealth.setPolicyHolderQuestion2C(updatespajHealth.getPolicyHolderQuestion2C());
        spajHealth.setInsuredQuestion2C(updatespajHealth.getInsuredQuestion2C());
        spajHealth.setPolicyHolderQuestion2D(updatespajHealth.getPolicyHolderQuestion2D());
        spajHealth.setInsuredQuestion2D(updatespajHealth.getInsuredQuestion2D());
        spajHealth.setPolicyHolderQuestion2E(updatespajHealth.getPolicyHolderQuestion2E());
        spajHealth.setInsuredQuestion2E(updatespajHealth.getInsuredQuestion2E());
        spajHealth.setPolicyHolderQuestion2F(updatespajHealth.getPolicyHolderQuestion2F());
        spajHealth.setInsuredQuestion2F(updatespajHealth.getInsuredQuestion2F());
        spajHealth.setPolicyHolderQuestion2G(updatespajHealth.getPolicyHolderQuestion2G());
        spajHealth.setInsuredQuestion2G(updatespajHealth.getInsuredQuestion2G());
        spajHealth.setPolicyHolderQuestion2H(updatespajHealth.getPolicyHolderQuestion2H());
        spajHealth.setInsuredQuestion2H(updatespajHealth.getInsuredQuestion2H());
        spajHealth.setPolicyHolderQuestion2I(updatespajHealth.getPolicyHolderQuestion2I());
        spajHealth.setInsuredQuestion2I(updatespajHealth.getInsuredQuestion2I());
        spajHealth.setPolicyHolderQuestion3(updatespajHealth.getPolicyHolderQuestion3());
        spajHealth.setPolicyHolderTotalQuestion3(updatespajHealth.getPolicyHolderTotalQuestion3());
        spajHealth.setInsuredQuestion3(updatespajHealth.getInsuredQuestion3());
        spajHealth.setPolicyHolderQuestion4(updatespajHealth.getPolicyHolderQuestion4());
        spajHealth.setInsuredQuestion4(updatespajHealth.getInsuredQuestion4());
        spajHealth.setPolicyHolderQuestion5(updatespajHealth.getPolicyHolderQuestion5());
        spajHealth.setInsuredQuestion5(updatespajHealth.getInsuredQuestion5());
        spajHealth.setPolicyHolderQuestion6(updatespajHealth.getPolicyHolderQuestion6());
        spajHealth.setInsuredQuestion6(updatespajHealth.getInsuredQuestion6());
        spajHealth.setPolicyHolderQuestion6A(updatespajHealth.getPolicyHolderQuestion6A());
        spajHealth.setInsuredQuestion6A(updatespajHealth.getInsuredQuestion6A());
        spajHealth.setPolicyHolderQuestion6B(updatespajHealth.getPolicyHolderQuestion6B());
        spajHealth.setInsuredQuestion6B(updatespajHealth.getInsuredQuestion6B());
        spajHealth.setPolicyHolderQuestion6C(updatespajHealth.getPolicyHolderQuestion6C());
        spajHealth.setInsuredQuestion6C(updatespajHealth.getInsuredQuestion6C());
        spajHealth.setPolicyHolderQuestion6D(updatespajHealth.getPolicyHolderQuestion6D());
        spajHealth.setInsuredQuestion6D(updatespajHealth.getInsuredQuestion6D());
        spajHealth.setPolicyHolderQuestion7(updatespajHealth.getPolicyHolderQuestion7());
        spajHealth.setInsuredQuestion7(updatespajHealth.getInsuredQuestion7());
        spajHealth.setPolicyHolderMonthQuestion14A(updatespajHealth.getPolicyHolderMonthQuestion14A());
        spajHealth.setInsuredMonthQuestion14A(updatespajHealth.getInsuredMonthQuestion14A());
        spajHealth.setNotesQuestion(updatespajHealth.getNotesQuestion());
        spajHealth.setActive(updatespajHealth.isActive());
        spajHealthRepository.save(spajHealth);
        em.refresh(spajHealth);
        SpajHealthDto response = modelMapper.map(spajHealth, SpajHealthDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SpajHealthDto> deleteSpajHealth(Long id) {
        SpajHealth spajHealth = spajHealthRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Health id " + id + " is not exist"));
        spajHealthRepository.deleteById(id);
        SpajHealthDto response = modelMapper.map(spajHealth, SpajHealthDto.class);

        return ResponseEntity.ok(response);
    }

    //insert data in file json
    @Transactional
    public void savingSpajHealth(SpajDocument spajDocument, ESubmissionBaseData eSubmissionBaseData, String spajCode){

        //master Spaj Health
        SpajHealth spajHealth = modelMapper.map(eSubmissionBaseData, SpajHealth.class);
        spajHealth.setWeightPolicyHolder(eSubmissionBaseData.getData().getData_kesehatan().getBerat_badan().getPemegang_polis());
        spajHealth.setWeightPolicyHolder(eSubmissionBaseData.getData().getData_kesehatan().getBerat_badan().getTertanggung());
        spajHealth.setHeightPolicyHolder(eSubmissionBaseData.getData().getData_kesehatan().getTinggi_badan().getPemegang_polis());
        spajHealth.setHeightInsured(eSubmissionBaseData.getData().getData_kesehatan().getTinggi_badan().getTertanggung());
        spajHealth.setBloodTypePolicyHolder(eSubmissionBaseData.getData().getData_kesehatan().getGolongan_darah().getPemegang_polis());
        spajHealth.setBloodTypeInsured(eSubmissionBaseData.getData().getData_kesehatan().getGolongan_darah().getTertanggung());
        spajHealth.setPolicyHolderQuestion1(eSubmissionBaseData.getData().getData_kesehatan().getHq_1_P1());
        spajHealth.setInsuredQuestion1(eSubmissionBaseData.getData().getData_kesehatan().getHq_1_L1());
        spajHealth.setPolicyHolderQuestion2A(eSubmissionBaseData.getData().getData_kesehatan().getHq_2a_P1());
        spajHealth.setInsuredQuestion2A(eSubmissionBaseData.getData().getData_kesehatan().getHq_2a_L1());
        spajHealth.setPolicyHolderQuestion2B(eSubmissionBaseData.getData().getData_kesehatan().getHq_2b_P1());
        spajHealth.setInsuredQuestion2B(eSubmissionBaseData.getData().getData_kesehatan().getHq_2b_L1());
        spajHealth.setPolicyHolderQuestion2C(eSubmissionBaseData.getData().getData_kesehatan().getHq_2c_P1());
        spajHealth.setInsuredQuestion2C(eSubmissionBaseData.getData().getData_kesehatan().getHq_2c_L1());
        spajHealth.setPolicyHolderQuestion2D(eSubmissionBaseData.getData().getData_kesehatan().getHq_2d_P1());
        spajHealth.setInsuredQuestion2D(eSubmissionBaseData.getData().getData_kesehatan().getHq_2d_L1());
        spajHealth.setPolicyHolderQuestion2E(eSubmissionBaseData.getData().getData_kesehatan().getHq_2e_P1());
        spajHealth.setInsuredQuestion2E(eSubmissionBaseData.getData().getData_kesehatan().getHq_2e_L1());
        spajHealth.setPolicyHolderQuestion2F(eSubmissionBaseData.getData().getData_kesehatan().getHq_2f_P1());
        spajHealth.setInsuredQuestion2F(eSubmissionBaseData.getData().getData_kesehatan().getHq_2f_L1());
        spajHealth.setPolicyHolderQuestion2G(eSubmissionBaseData.getData().getData_kesehatan().getHq_2g_P1());
        spajHealth.setInsuredQuestion2G(eSubmissionBaseData.getData().getData_kesehatan().getHq_2g_L1());
        spajHealth.setPolicyHolderQuestion2H(eSubmissionBaseData.getData().getData_kesehatan().getHq_2h_P1());
        spajHealth.setInsuredQuestion2H(eSubmissionBaseData.getData().getData_kesehatan().getHq_2h_L1());
        spajHealth.setPolicyHolderQuestion2I(eSubmissionBaseData.getData().getData_kesehatan().getHq_2i_P1());
        spajHealth.setInsuredQuestion2I(eSubmissionBaseData.getData().getData_kesehatan().getHq_2i_L1());
        spajHealth.setPolicyHolderQuestion3(eSubmissionBaseData.getData().getData_kesehatan().getHq_3_P1());
        spajHealth.setInsuredQuestion3(eSubmissionBaseData.getData().getData_kesehatan().getHq_3_L1());
        spajHealth.setPolicyHolderQuestion4(eSubmissionBaseData.getData().getData_kesehatan().getHq_4_P1());
        spajHealth.setInsuredQuestion4(eSubmissionBaseData.getData().getData_kesehatan().getHq_4_L1());
        spajHealth.setPolicyHolderQuestion5(eSubmissionBaseData.getData().getData_kesehatan().getHq_5_P1());
        spajHealth.setInsuredQuestion5(eSubmissionBaseData.getData().getData_kesehatan().getHq_5_L1());
        spajHealth.setPolicyHolderQuestion6A(eSubmissionBaseData.getData().getData_kesehatan().getHq_6a_P1());
        spajHealth.setPolicyHolderMonthQuestion14A(eSubmissionBaseData.getData().getData_kesehatan().getHq_6a_P1_bulan());
        spajHealth.setInsuredQuestion6A(eSubmissionBaseData.getData().getData_kesehatan().getHq_6a_L1());
        spajHealth.setInsuredMonthQuestion14A(eSubmissionBaseData.getData().getData_kesehatan().getHq_6a_L1_bulan());
        spajHealth.setPolicyHolderQuestion6B(eSubmissionBaseData.getData().getData_kesehatan().getHq_6b_P1());
        spajHealth.setInsuredQuestion6B(eSubmissionBaseData.getData().getData_kesehatan().getHq_6b_L1());
        spajHealth.setPolicyHolderQuestion6C(eSubmissionBaseData.getData().getData_kesehatan().getHq_6c_P1());
        spajHealth.setInsuredQuestion6C(eSubmissionBaseData.getData().getData_kesehatan().getHq_6c_L1());
        spajHealth.setPolicyHolderQuestion6D(eSubmissionBaseData.getData().getData_kesehatan().getHq_6d_P1());
        spajHealth.setInsuredQuestion6D(eSubmissionBaseData.getData().getData_kesehatan().getHq_6d_L1());
        spajHealth.setPolicyHolderQuestion7(eSubmissionBaseData.getData().getData_kesehatan().getHq_7_P1());
        spajHealth.setInsuredQuestion7(eSubmissionBaseData.getData().getData_kesehatan().getHq_7_L1());
        spajHealth.setNotesQuestion(eSubmissionBaseData.getData().getData_kesehatan().getHq_keterangan());
        spajHealth.setActive(true);
        spajHealth.setSpajDocumentCode(spajDocument.getSpajDocumentCode());
        spajHealth.setSpajNo(spajCode);
        spajHealthRepository.save(spajHealth);
    }
}