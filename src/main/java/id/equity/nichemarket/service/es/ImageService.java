package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.Image.CreateImage;
import id.equity.nichemarket.dto.es.Image.ImageDto;
import id.equity.nichemarket.model.es.Image;
import id.equity.nichemarket.repository.es.ImageRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ImageService {
    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<ImageDto>> listImage() {
        List<Image> image= imageRepository.findAll();
        Type targetType = new TypeToken<List<ImageDto>>() {}.getType();
        List<ImageDto> response = modelMapper.map(image, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<ImageDto> getImageById(Long id) {
        Image image = imageRepository.findById(id).orElseThrow(()-> new NotFoundException("Image id " + id + " is not exist"));
        ImageDto response = modelMapper.map(image, ImageDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<ImageDto> addImage(CreateImage newImage) {
        Image image = modelMapper.map(newImage, Image.class);
        Image imageSaved = imageRepository.save(image);
        ImageDto response = modelMapper.map(imageSaved, ImageDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<ImageDto> editImage(CreateImage updateImage, Long id) {
        Image image = imageRepository.findById(id).orElseThrow(()-> new NotFoundException("Image id " + id + " is not exist"));
        image.setImageCode(updateImage.getImageCode());
        image.setDescription(updateImage.getDescription());
        image.setObjectImage(updateImage.getObjectImage());
        image.setActive(updateImage.isActive());
        imageRepository.save(image);
        ImageDto response = modelMapper.map(image, ImageDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<ImageDto> deleteImage(Long id) {
        Image image = imageRepository.findById(id).orElseThrow(()-> new NotFoundException("Image id " + id + " is not exist"));
        imageRepository.deleteById(id);
        ImageDto response = modelMapper.map(image, ImageDto.class);

        return ResponseEntity.ok(response);
    }
}