package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRuleSumInsured.CreateProductRuleSumInsured;
import id.equity.nichemarket.dto.si.ProductRuleSumInsured.ProductRuleSumInsuredDto;
import id.equity.nichemarket.model.si.ProductRuleSumInsured;
import id.equity.nichemarket.repository.si.ProductRuleSumInsuredRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleSumInsuredService {
	
	@Autowired
	private ProductRuleSumInsuredRepository productRuleSumInsuredRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleSumInsured
	public ResponseEntity<List<ProductRuleSumInsuredDto>> listProductRuleSumInsured() {
		List<ProductRuleSumInsured> listProductRuleSumInsureds = productRuleSumInsuredRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleSumInsuredDto>>() {}.getType();
		List<ProductRuleSumInsuredDto> response = modelMapper.map(listProductRuleSumInsureds, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRuleSumInsured By Id
	public ResponseEntity<ProductRuleSumInsuredDto> getProductRuleSumInsuredById(Long id) {
		ProductRuleSumInsured productRuleSumInsured = productRuleSumInsuredRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Sum Insured id " + id + " is not exist"));
		ProductRuleSumInsuredDto response = modelMapper.map(productRuleSumInsured, ProductRuleSumInsuredDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleSumInsured
	public ResponseEntity<ProductRuleSumInsuredDto> addProductRuleSumInsured(CreateProductRuleSumInsured newProductRuleSumInsured) {
		ProductRuleSumInsured productRuleSumInsured = modelMapper.map(newProductRuleSumInsured, ProductRuleSumInsured.class);
		ProductRuleSumInsured productRuleSumInsuredSaved = productRuleSumInsuredRepository.save(productRuleSumInsured);
		ProductRuleSumInsuredDto response = modelMapper.map(productRuleSumInsuredSaved, ProductRuleSumInsuredDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleSumInsured
	public ResponseEntity<ProductRuleSumInsuredDto> editProductRuleSumInsured(CreateProductRuleSumInsured updateProductRuleSumInsured, Long id) {
		ProductRuleSumInsured productRuleSumInsured = productRuleSumInsuredRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Sum Insured id " + id + " is not exist"));
		
		//manual map
		productRuleSumInsured.setProductRuleSumInsuredCode(updateProductRuleSumInsured.getProductRuleSumInsuredCode());
		productRuleSumInsured.setProductCode(updateProductRuleSumInsured.getProductCode());
		productRuleSumInsured.setValutaCode(updateProductRuleSumInsured.getValutaCode());
		productRuleSumInsured.setPaymentPeriodCode(updateProductRuleSumInsured.getPaymentPeriodCode());
		productRuleSumInsured.setAsCode(updateProductRuleSumInsured.getAsCode());
		productRuleSumInsured.setMinAge(updateProductRuleSumInsured.getMinAge());
		productRuleSumInsured.setMaxAge(updateProductRuleSumInsured.getMaxAge());
		productRuleSumInsured.setMinPremiumPeriodic(updateProductRuleSumInsured.getMinPremiumPeriodic());
		productRuleSumInsured.setMaxPremiumPeriodic(updateProductRuleSumInsured.getMaxPremiumPeriodic());
		productRuleSumInsured.setMinSumInsuredBase(updateProductRuleSumInsured.getMinSumInsuredBase());
		productRuleSumInsured.setMaxSumInsuredBase(updateProductRuleSumInsured.getMaxSumInsuredBase());
		productRuleSumInsured.setClasses(updateProductRuleSumInsured.getClasses());
		productRuleSumInsured.setRisk(updateProductRuleSumInsured.getRisk());
		productRuleSumInsured.setMinSumInsuredFormula(updateProductRuleSumInsured.getMinSumInsuredFormula());
		productRuleSumInsured.setMaxSumInsuredFormula(updateProductRuleSumInsured.getMaxSumInsuredFormula());
		productRuleSumInsured.setStepSumInsured(updateProductRuleSumInsured.getStepSumInsured());
		productRuleSumInsured.setActive(updateProductRuleSumInsured.isActive());
		
		productRuleSumInsuredRepository.save(productRuleSumInsured);
		ProductRuleSumInsuredDto response = modelMapper.map(productRuleSumInsured, ProductRuleSumInsuredDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleSumInsured
	public ResponseEntity<ProductRuleSumInsuredDto> deleteProductRuleSumInsured(Long id) {
		ProductRuleSumInsured productRuleSumInsured = productRuleSumInsuredRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Sum Insured id " + id + " is not exist"));
		productRuleSumInsuredRepository.deleteById(id);
		ProductRuleSumInsuredDto response = modelMapper.map(productRuleSumInsured, ProductRuleSumInsuredDto.class);

		return ResponseEntity.ok(response);
	}	
}