package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRuleAdmin.BaseRuleAdminDto;
import id.equity.nichemarket.dto.si.BaseRuleAdmin.CreateBaseRuleAdmin;
import id.equity.nichemarket.model.si.BaseRuleAdmin;
import id.equity.nichemarket.repository.si.BaseRuleAdminRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleAdminService {
	
	@Autowired
	private BaseRuleAdminRepository baseRuleAdminRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List BaseRuleAdmin
	public ResponseEntity<List<BaseRuleAdminDto>> listBaseRuleAdmin() {
		List<BaseRuleAdmin> listBaseRules = baseRuleAdminRepository.findAll();
		Type targetType = new TypeToken <List<BaseRuleAdminDto>>() {}.getType();
		List<BaseRuleAdminDto> response = modelMapper.map(listBaseRules, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get BaseRuleAdmin ById
	public ResponseEntity<BaseRuleAdminDto> getBaseRuleAdminById(Long id) {
		BaseRuleAdmin baseRule = baseRuleAdminRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Admin id " + id + " is not exist"));
		BaseRuleAdminDto response = modelMapper.map(baseRule, BaseRuleAdminDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create BaseRuleAdmin
	public ResponseEntity<BaseRuleAdminDto> addBaseRuleAdmin(CreateBaseRuleAdmin newBaseRule) {
		BaseRuleAdmin baseRule = modelMapper.map(newBaseRule, BaseRuleAdmin.class);
		BaseRuleAdmin BaseRuleSaved = baseRuleAdminRepository.save(baseRule);
		BaseRuleAdminDto response = modelMapper.map(BaseRuleSaved, BaseRuleAdminDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit BaseRuleAdmin
	public ResponseEntity<BaseRuleAdminDto> editBaseRuleAdmin(CreateBaseRuleAdmin updateBaserule, Long id) {
		BaseRuleAdmin baseRule  = baseRuleAdminRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Admin id " + id + " is not Exist"));
		
		//manual map
		baseRule.setBaseRuleAdminFeeCode(updateBaserule.getBaseRuleAdminFeeCode());
		baseRule.setProductCode(updateBaserule.getProductCode());
		baseRule.setValutaCode(updateBaserule.getValutaCode());
		baseRule.setPaymentPeriodCode(updateBaserule.getPaymentPeriodCode());
		baseRule.setRate(updateBaserule.getRate());
		baseRule.setActive(updateBaserule.isActive());
		
		baseRuleAdminRepository.save(baseRule);
		BaseRuleAdminDto response = modelMapper.map(baseRule, BaseRuleAdminDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete BaseRuleAdmin
	public ResponseEntity<BaseRuleAdminDto> deleteBaseRuleAdmin(Long id) {
		BaseRuleAdmin baseRule = baseRuleAdminRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Admin id " + id + " is not exist"));
		baseRuleAdminRepository.deleteById(id);
		BaseRuleAdminDto response = modelMapper.map(baseRule, BaseRuleAdminDto.class);

		return ResponseEntity.ok(response);
	}
}