package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRuleInvestType.BaseRuleInvestTypeDto;
import id.equity.nichemarket.dto.si.BaseRuleInvestType.CreateBaseRuleInvestType;
import id.equity.nichemarket.model.si.BaseRuleInvestType;
import id.equity.nichemarket.repository.si.BaseRuleInvestTypeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleInvestTypeService {
    @Autowired
    private BaseRuleInvestTypeRepository baseRuleInvestTypeRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<BaseRuleInvestTypeDto>> listBaseRuleInvestType() {
        List<BaseRuleInvestType> lsBaseRuleInvestType= baseRuleInvestTypeRepository.findAll();
        Type targetType = new TypeToken<List<BaseRuleInvestTypeDto>>() {}.getType();
        List<BaseRuleInvestTypeDto> response = modelMapper.map(lsBaseRuleInvestType, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<BaseRuleInvestTypeDto> getBaseRuleInvestTypeById(Long id) {
        BaseRuleInvestType baseRuleInvestType = baseRuleInvestTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Invest Type id " + id + " is not exist"));
        BaseRuleInvestTypeDto response = modelMapper.map(baseRuleInvestType, BaseRuleInvestTypeDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<BaseRuleInvestTypeDto> addBaseRuleInvestType(CreateBaseRuleInvestType newBaseRuleInvestType) {
        BaseRuleInvestType baseRuleInvestType = modelMapper.map(newBaseRuleInvestType, BaseRuleInvestType.class);
        BaseRuleInvestType baseRuleInvestTypeSaved = baseRuleInvestTypeRepository.save(baseRuleInvestType);
        BaseRuleInvestTypeDto response = modelMapper.map(baseRuleInvestTypeSaved, BaseRuleInvestTypeDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<BaseRuleInvestTypeDto> editBaseRuleInvestType(CreateBaseRuleInvestType updateBaseRuleInvestType, Long id) {
        BaseRuleInvestType baseRuleInvestType = baseRuleInvestTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Invest Type id " + id + " is not exist"));
        baseRuleInvestType.setBaseRuleInvestTypeCode(updateBaseRuleInvestType.getBaseRuleInvestTypeCode());
        baseRuleInvestType.setProductCode(updateBaseRuleInvestType.getProductCode());
        baseRuleInvestType.setValutaCode(updateBaseRuleInvestType.getValutaCode());
        baseRuleInvestType.setInvestTypeCode(updateBaseRuleInvestType.getInvestTypeCode());
        baseRuleInvestType.setLowInterest(updateBaseRuleInvestType.getLowInterest());
        baseRuleInvestType.setMediumInterest(updateBaseRuleInvestType.getMediumInterest());
        baseRuleInvestType.setHighInterest(updateBaseRuleInvestType.getHighInterest());
        baseRuleInvestType.setActive(updateBaseRuleInvestType.isActive());
        baseRuleInvestTypeRepository.save(baseRuleInvestType);
        BaseRuleInvestTypeDto response = modelMapper.map(baseRuleInvestType, BaseRuleInvestTypeDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<BaseRuleInvestTypeDto> deleteBaseRuleInvestType(Long id) {
        BaseRuleInvestType baseRuleInvestType = baseRuleInvestTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Invest Type id " + id + " is not exist"));
        baseRuleInvestTypeRepository.deleteById(id);
        BaseRuleInvestTypeDto response = modelMapper.map(baseRuleInvestType, BaseRuleInvestTypeDto.class);

        return ResponseEntity.ok(response);
    }
}