package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRulePremiumPeriodic.BaseRulePremiumPeriodicDto;
import id.equity.nichemarket.dto.si.BaseRulePremiumPeriodic.CreateBaseRulePremiumPeriodic;
import id.equity.nichemarket.model.si.BaseRulePremiumPeriodic;
import id.equity.nichemarket.repository.si.BaseRulePremiumPeriodicRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRulePremiumPeriodicService {
	
	@Autowired
	private BaseRulePremiumPeriodicRepository baseRulePremiumPeriodicRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List PremiumPeriodic
	public ResponseEntity<List<BaseRulePremiumPeriodicDto>> listBaseRulePremiumPeriodic() {
		List<BaseRulePremiumPeriodic> listPremiumPeriodics = baseRulePremiumPeriodicRepository.findAll();
		Type targetType = new TypeToken <List<BaseRulePremiumPeriodicDto>>() {}.getType();
		List<BaseRulePremiumPeriodicDto> response = modelMapper.map(listPremiumPeriodics, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List PremiumPeriodic By Id
	public ResponseEntity<BaseRulePremiumPeriodicDto> getBaseRulePremiumPeriodicById(Long id) {
		BaseRulePremiumPeriodic baseRulePremiumPeriodic = baseRulePremiumPeriodicRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Periodic id " + id + " is not exist"));
		BaseRulePremiumPeriodicDto response = modelMapper.map(baseRulePremiumPeriodic, BaseRulePremiumPeriodicDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create PremiumPeriodic
	public ResponseEntity<BaseRulePremiumPeriodicDto> addBaseRulePremiumPeriodic(CreateBaseRulePremiumPeriodic newPremiumPeriodic) {
		BaseRulePremiumPeriodic baseRulePremiumPeriodic = modelMapper.map(newPremiumPeriodic, BaseRulePremiumPeriodic.class);
		BaseRulePremiumPeriodic PremiumPeriodicSaved = baseRulePremiumPeriodicRepository.save(baseRulePremiumPeriodic);
		BaseRulePremiumPeriodicDto response = modelMapper.map(PremiumPeriodicSaved, BaseRulePremiumPeriodicDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit PremiumPeriodic
	public ResponseEntity<BaseRulePremiumPeriodicDto> editBaseRulePremiumPeriodic(CreateBaseRulePremiumPeriodic updatePremiumPeriodic,
			Long id) {
		BaseRulePremiumPeriodic baseRulePremiumPeriodic  = baseRulePremiumPeriodicRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Periodic id " + id + " is not Exist"));
		
		//manual map
		baseRulePremiumPeriodic.setBaseRulePremiumPeriodicCode(updatePremiumPeriodic.getBaseRulePremiumPeriodicCode());
		baseRulePremiumPeriodic.setProductCode(updatePremiumPeriodic.getProductCode());
		baseRulePremiumPeriodic.setValutaCode(updatePremiumPeriodic.getValutaCode());
		baseRulePremiumPeriodic.setPaymentPeriodCode(updatePremiumPeriodic.getPaymentPeriodCode());
		baseRulePremiumPeriodic.setMinPremium(updatePremiumPeriodic.getMinPremium());
		baseRulePremiumPeriodic.setMaxPremium(updatePremiumPeriodic.getMaxPremium());
		baseRulePremiumPeriodic.setStepPremium(updatePremiumPeriodic.getStepPremium());
		baseRulePremiumPeriodic.setActive(updatePremiumPeriodic.isActive());

		baseRulePremiumPeriodicRepository.save(baseRulePremiumPeriodic);
		BaseRulePremiumPeriodicDto response = modelMapper.map(baseRulePremiumPeriodic, BaseRulePremiumPeriodicDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete PremiumPeriodic
	public ResponseEntity<BaseRulePremiumPeriodicDto> deleteBaseRulePremiumPeriodic(Long id) {
		BaseRulePremiumPeriodic baseRulePremiumPeriodic = baseRulePremiumPeriodicRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Periodic id " + id + " is not exist"));
		baseRulePremiumPeriodicRepository.deleteById(id);
		BaseRulePremiumPeriodicDto response = modelMapper.map(baseRulePremiumPeriodic, BaseRulePremiumPeriodicDto.class);

		return ResponseEntity.ok(response);
	}
}