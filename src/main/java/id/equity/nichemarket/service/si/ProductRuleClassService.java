package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRuleClass.CreateProductRuleClass;
import id.equity.nichemarket.dto.si.ProductRuleClass.ProductRuleClassDto;
import id.equity.nichemarket.model.si.ProductRuleClass;
import id.equity.nichemarket.repository.si.ProductRuleClassRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleClassService {
	
	@Autowired
	private ProductRuleClassRepository productRuleClassRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleClass
	public ResponseEntity<List<ProductRuleClassDto>> listProductRuleClass() {
		List<ProductRuleClass> listProductRuleClasss = productRuleClassRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleClassDto>>() {}.getType();
		List<ProductRuleClassDto> response = modelMapper.map(listProductRuleClasss, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRuleClass By Id
	public ResponseEntity<ProductRuleClassDto> getProductRuleClassById(Long id) {
		ProductRuleClass productRuleClass = productRuleClassRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Class id " + id + " is not exist"));
		ProductRuleClassDto response = modelMapper.map(productRuleClass, ProductRuleClassDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleActivate
	public ResponseEntity<ProductRuleClassDto> addProductRuleClass(CreateProductRuleClass newProductRuleClass) {
		ProductRuleClass productRuleClass = modelMapper.map(newProductRuleClass, ProductRuleClass.class);
		ProductRuleClass productRuleClassSaved = productRuleClassRepository.save(productRuleClass);
		ProductRuleClassDto response = modelMapper.map(productRuleClassSaved, ProductRuleClassDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleClass
	public ResponseEntity<ProductRuleClassDto> editProductRuleClass(CreateProductRuleClass updateProductRuleClass, Long id) {
		ProductRuleClass productRuleClass = productRuleClassRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Class id " + id + " is not exist"));
		
		//manual map
		productRuleClass.setProductRuleClassCode(updateProductRuleClass.getProductRuleClassCode());
		productRuleClass.setProductCode(updateProductRuleClass.getProductCode());
		productRuleClass.setPaymentPeriodCode(updateProductRuleClass.getPaymentPeriodCode());
		productRuleClass.setMaxSumInsuredBase(updateProductRuleClass.getMaxSumInsuredBase());
		productRuleClass.setMaxPremiumPeriodic(updateProductRuleClass.getMaxPremiumPeriodic());
		productRuleClass.setMaxTuPeriodic(updateProductRuleClass.getMaxTuPeriodic());
		productRuleClass.setClasses(updateProductRuleClass.getClasses());
		productRuleClass.setActive(updateProductRuleClass.isActive());
		
		productRuleClassRepository.save(productRuleClass);
		ProductRuleClassDto response = modelMapper.map(productRuleClass, ProductRuleClassDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleActivate
	public ResponseEntity<ProductRuleClassDto> deleteProductRuleClass(Long id) {
		ProductRuleClass productRuleClass = productRuleClassRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Class id " + id + " is not exist"));
		productRuleClassRepository.deleteById(id);
		ProductRuleClassDto response = modelMapper.map(productRuleClass, ProductRuleClassDto.class);

		return ResponseEntity.ok(response);
	}	
}