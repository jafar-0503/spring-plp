package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRuleBenefitItem.CreateProductRuleBenefitItem;
import id.equity.nichemarket.dto.si.ProductRuleBenefitItem.ProductRuleBenefitItemDto;
import id.equity.nichemarket.model.si.ProductRuleBenefitItem;
import id.equity.nichemarket.repository.si.ProductRuleBenefitItemRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleBenefitItemService {
	
	@Autowired
	private ProductRuleBenefitItemRepository productRuleBenefitItemRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleBenefitItem
	public ResponseEntity<List<ProductRuleBenefitItemDto>> listProductRuleBenefitItem() {
		List<ProductRuleBenefitItem> listProductRuleBenefitItems = productRuleBenefitItemRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleBenefitItemDto>>() {}.getType();
		List<ProductRuleBenefitItemDto> response = modelMapper.map(listProductRuleBenefitItems, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRuleBenefitItem By Id
	public ResponseEntity<ProductRuleBenefitItemDto> getProductRuleBenefitItemById(Long id) {
		ProductRuleBenefitItem productRuleBenefitItem = productRuleBenefitItemRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Benefit Item id " + id + " is not exist"));
		ProductRuleBenefitItemDto response = modelMapper.map(productRuleBenefitItem, ProductRuleBenefitItemDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleBenefitItem
	public ResponseEntity<ProductRuleBenefitItemDto> addProductRuleBenefitItem(CreateProductRuleBenefitItem newProductRuleBenefitItem) {
		ProductRuleBenefitItem productRuleBenefitItem = modelMapper.map(newProductRuleBenefitItem, ProductRuleBenefitItem.class);
		ProductRuleBenefitItem productRuleBenefitItemSaved = productRuleBenefitItemRepository.save(productRuleBenefitItem);
		ProductRuleBenefitItemDto response = modelMapper.map(productRuleBenefitItemSaved, ProductRuleBenefitItemDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleBenefitItem
	public ResponseEntity<ProductRuleBenefitItemDto> editProductRuleBenefitItem(CreateProductRuleBenefitItem updateProductRuleBenefitItem, Long id) {
		ProductRuleBenefitItem productRuleBenefitItem = productRuleBenefitItemRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Benefit Item id " + id + " is not exist"));
		
		//manual map
		productRuleBenefitItem.setProductRuleBenefitItemCode(updateProductRuleBenefitItem.getProductRuleBenefitItemCode());
		productRuleBenefitItem.setProductCode(updateProductRuleBenefitItem.getProductCode());
		productRuleBenefitItem.setOrder(updateProductRuleBenefitItem.getOrder());
		productRuleBenefitItem.setDescription(updateProductRuleBenefitItem.getDescription());
		productRuleBenefitItem.setValue(updateProductRuleBenefitItem.getValue());
		productRuleBenefitItem.setActive(updateProductRuleBenefitItem.isActive());
		
		productRuleBenefitItemRepository.save(productRuleBenefitItem);
		ProductRuleBenefitItemDto response = modelMapper.map(productRuleBenefitItem, ProductRuleBenefitItemDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleBenefitItem
	public ResponseEntity<ProductRuleBenefitItemDto> deleteProductRuleBenefitItem(Long id) {
		ProductRuleBenefitItem productRuleBenefitItem = productRuleBenefitItemRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Benefit Item id " + id + " is not exist"));
		productRuleBenefitItemRepository.deleteById(id);
		ProductRuleBenefitItemDto response = modelMapper.map(productRuleBenefitItem, ProductRuleBenefitItemDto.class);

		return ResponseEntity.ok(response);
	}	
}