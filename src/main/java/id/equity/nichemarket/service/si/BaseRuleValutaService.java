package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRuleValuta.BaseRuleValutaDto;
import id.equity.nichemarket.dto.si.BaseRuleValuta.CreateBaseRuleValuta;
import id.equity.nichemarket.model.si.BaseRuleValuta;
import id.equity.nichemarket.repository.si.BaseRuleValutaRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleValutaService {
	
	@Autowired
	private BaseRuleValutaRepository baseRuleValutaRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List BaseRuleValuta
	public ResponseEntity<List<BaseRuleValutaDto>> listBaseRuleValuta() {
		List<BaseRuleValuta> listRuleValutas = baseRuleValutaRepository.findAll();
		Type targetType = new TypeToken<List<BaseRuleValutaDto>>() {}.getType();
		List<BaseRuleValutaDto> response = modelMapper.map(listRuleValutas, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get BaseRuleValuta By Id
	public ResponseEntity<BaseRuleValutaDto> getBaseRuleValutaById(Long id) {
		BaseRuleValuta ruleValuta = baseRuleValutaRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Valuta id " + id + " is not exist"));
		BaseRuleValutaDto response = modelMapper.map(ruleValuta, BaseRuleValutaDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create BaseRuleValuta
	public ResponseEntity<BaseRuleValutaDto> addBaseRuleValuta(CreateBaseRuleValuta newRuleValuta) {
		BaseRuleValuta ruleValuta = modelMapper.map(newRuleValuta, BaseRuleValuta.class);
		BaseRuleValuta ruleValutaSaved = baseRuleValutaRepository.save(ruleValuta);
		BaseRuleValutaDto response = modelMapper.map(ruleValutaSaved, BaseRuleValutaDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit BaseRuleValuta
	public ResponseEntity<BaseRuleValutaDto> editBaseRuleValuta(CreateBaseRuleValuta updateRuleValuta, Long id) {
		BaseRuleValuta ruleValuta = baseRuleValutaRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Valuta id " + id + " is not exist"));
		
		//manual map
		ruleValuta.setBaseRuleSingleTopupCode(updateRuleValuta.getBaseRuleSingleTopupCode());
		ruleValuta.setProductCode(updateRuleValuta.getProductCode());
		ruleValuta.setValutaCode(updateRuleValuta.getValutaCode());
		ruleValuta.setActive(updateRuleValuta.isActive());

		baseRuleValutaRepository.save(ruleValuta);
		BaseRuleValutaDto response = modelMapper.map(ruleValuta, BaseRuleValutaDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete BaseRuleValuta
	public ResponseEntity<BaseRuleValutaDto> deleteBaseRuleValuta(Long id) {
		BaseRuleValuta ruleValuta = baseRuleValutaRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Valuta id " + id + " is not exist"));
		baseRuleValutaRepository.deleteById(id);
		BaseRuleValutaDto response = modelMapper.map(ruleValuta, BaseRuleValutaDto.class);

		return ResponseEntity.ok(response);
	}
}