package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRate.CreateProductRate;
import id.equity.nichemarket.dto.si.ProductRate.ProductRateDto;
import id.equity.nichemarket.model.si.ProductRate;
import id.equity.nichemarket.repository.si.ProductRateRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRateService {

	@Autowired
	private ProductRateRepository productRateRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	//Get All Data
	public ResponseEntity<List<ProductRateDto>> listProductRate() {
		List<ProductRate> lsProductRate = productRateRepository.findAll();
		Type targetType = new TypeToken<List<ProductRateDto>>() {}.getType();
		List<ProductRateDto> response = modelMapper.map(lsProductRate, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Data by Id
	public ResponseEntity<ProductRateDto> getProductRateById(Long id) {
		ProductRate ProductRate = productRateRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rate id " + id + " is not exist"));
		ProductRateDto response = modelMapper.map(ProductRate, ProductRateDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Data
	public ResponseEntity<ProductRateDto> addProductRate(CreateProductRate newProductRate) {
		ProductRate ProductRate = modelMapper.map(newProductRate, ProductRate.class);
		ProductRate ProductRateSaved = productRateRepository.save(ProductRate);
		ProductRateDto response = modelMapper.map(ProductRateSaved, ProductRateDto.class);

		return ResponseEntity.ok(response);

	}

	//Edit Data By Id
	public ResponseEntity<ProductRateDto> editProductRate(CreateProductRate updateProductRate, Long id) {
		ProductRate productRate = productRateRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rate id " + id + "is not exist"));
		productRate.setProductRateCode(updateProductRate.getProductRateCode());
		productRate.setCategory(updateProductRate.getCategory());
		productRate.setProductCode(updateProductRate.getProductCode());
		productRate.setValutaCode(updateProductRate.getValutaCode());
		productRate.setPaymentPeriodCode(updateProductRate.getPaymentPeriodCode());
		productRate.setAsCode(updateProductRate.getAsCode());
		productRate.setGenderCode(updateProductRate.getGenderCode());
		productRate.setAgeMin(updateProductRate.getAgeMin());
		productRate.setAgeMax(updateProductRate.getAgeMax());
		productRate.setContractPeriod(updateProductRate.getContractPeriod());
		productRate.setPremiumPeriod(updateProductRate.getPremiumPeriod());
		productRate.setClasss(updateProductRate.getClasss());
		productRate.setRisk(updateProductRate.getRisk());
		productRate.setRate(updateProductRate.getRate());
		productRate.setActive(updateProductRate.isActive());
		productRateRepository.save(productRate);
		ProductRateDto response = modelMapper.map(productRate, ProductRateDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Data By Id
	public ResponseEntity<ProductRateDto> deleteProductRate(Long id) {
		ProductRate findProductRate = productRateRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rate id " + id + " is not exist"));
		productRateRepository.deleteById(id);
		ProductRateDto response = modelMapper.map(findProductRate, ProductRateDto.class);

		return ResponseEntity.ok(response);
	}
}