package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.Product.CreateProduct;
import id.equity.nichemarket.dto.si.Product.ProductDto;
import id.equity.nichemarket.model.si.Product;
import id.equity.nichemarket.repository.si.ProductRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;


@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Product
	public ResponseEntity<List<ProductDto>> listProduct() {
		List<Product> listProducts = productRepository.findAll();
		Type targetType = new TypeToken <List<ProductDto>>() {}.getType();
		List<ProductDto> response = modelMapper.map(listProducts, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get Product ById
	public ResponseEntity<ProductDto> getProductById(Long id) {
		Product product = productRepository.findById(id).orElseThrow(()-> new NotFoundException("Product id " + id + " is not exist"));
		ProductDto response = modelMapper.map(product, ProductDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create Product
	public ResponseEntity<ProductDto> addProduct(CreateProduct newProduct) {
		Product product = modelMapper.map(newProduct, Product.class);
		Product productSaved = productRepository.save(product);
		ProductDto response = modelMapper.map(productSaved, ProductDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit Product
	public ResponseEntity<ProductDto> editProduct(CreateProduct updateProduct, Long id) {
		Product product = productRepository.findById(id).orElseThrow(()-> new NotFoundException("Product id " + id + " is not Exist"));
		
		//manual map
		product.setProductCode(updateProduct.getProductCode());
		product.setParentProductCode(updateProduct.getParentProductCode());
		product.setProductCodeMapping(updateProduct.getProductCodeMapping());
		product.setDescription(updateProduct.getDescription());
		product.setOrder(updateProduct.getOrder());
		product.setBasic(updateProduct.isBasic());
		product.setClass(updateProduct.isClass());
		product.setRisk(updateProduct.isRisk());
		product.setContractPeriod(updateProduct.isContractPeriod());
		product.setPremiumPeriod(updateProduct.isPremiumPeriod());
		product.setSumInsured(updateProduct.isSumInsured());
		product.setPremium(updateProduct.isPremium());
		product.setActivate(updateProduct.isActivate());
		product.setRequired(updateProduct.isRequired());
		product.setPayor(updateProduct.isPayor());
		product.setActive(updateProduct.isActive());
		
		productRepository.save(product);
		ProductDto response = modelMapper.map(product, ProductDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete Product
	public ResponseEntity<ProductDto> deleteProduct(Long id) {
		Product product = productRepository.findById(id).orElseThrow(()-> new NotFoundException("Product id " + id + " is not exist"));
		
		productRepository.deleteById(id);
		ProductDto response = modelMapper.map(product, ProductDto.class);

		return ResponseEntity.ok(response);
	}
}