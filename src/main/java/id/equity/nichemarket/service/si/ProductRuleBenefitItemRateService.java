package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRuleBenefitItemRate.CreateProductRuleBenefitItemRate;
import id.equity.nichemarket.dto.si.ProductRuleBenefitItemRate.ProductRuleBenefitItemRateDto;
import id.equity.nichemarket.model.si.ProductRuleBenefitItemRate;
import id.equity.nichemarket.repository.si.ProductRuleBenefitItemRateRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleBenefitItemRateService {
	
	@Autowired
	private ProductRuleBenefitItemRateRepository productRuleBenefitItemRateRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleBenefitItemRateRate
	public ResponseEntity<List<ProductRuleBenefitItemRateDto>> listProductRuleBenefitItemRate() {
		List<ProductRuleBenefitItemRate> listProductRuleBenefitItemRates = productRuleBenefitItemRateRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleBenefitItemRateDto>>() {}.getType();
		List<ProductRuleBenefitItemRateDto> response = modelMapper.map(listProductRuleBenefitItemRates, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRuleBenefitItemRate By Id
	public ResponseEntity<ProductRuleBenefitItemRateDto> getProductRuleBenefitItemRateById(Long id) {
		ProductRuleBenefitItemRate productRuleBenefitItemRate = productRuleBenefitItemRateRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Benefit Item Rate id " + id + " is not exist"));
		ProductRuleBenefitItemRateDto response = modelMapper.map(productRuleBenefitItemRate, ProductRuleBenefitItemRateDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleBenefitItemRate
	public ResponseEntity<ProductRuleBenefitItemRateDto> addProductRuleBenefitItemRate(CreateProductRuleBenefitItemRate newProductRuleBenefitItemRate) {
		ProductRuleBenefitItemRate productRuleBenefitItemRate = modelMapper.map(newProductRuleBenefitItemRate, ProductRuleBenefitItemRate.class);
		ProductRuleBenefitItemRate productRuleBenefitItemRateSaved = productRuleBenefitItemRateRepository.save(productRuleBenefitItemRate);
		ProductRuleBenefitItemRateDto response = modelMapper.map(productRuleBenefitItemRateSaved, ProductRuleBenefitItemRateDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleBenefitItemRate
	public ResponseEntity<ProductRuleBenefitItemRateDto> editProductRuleBenefitItemRate(CreateProductRuleBenefitItemRate updateProductRuleBenefitItemRate, Long id) {
		ProductRuleBenefitItemRate productRuleBenefitItemRate = productRuleBenefitItemRateRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Benefit Item Rate id " + id + " is not exist"));
		
		//manual map
		productRuleBenefitItemRate.setProductRuleBenefitItemRateCode(updateProductRuleBenefitItemRate.getProductRuleBenefitItemRateCode());
		productRuleBenefitItemRate.setProductCode(updateProductRuleBenefitItemRate.getProductCode());
		productRuleBenefitItemRate.setOrder(updateProductRuleBenefitItemRate.getOrder());
		productRuleBenefitItemRate.setClasses(updateProductRuleBenefitItemRate.getClasses());
		productRuleBenefitItemRate.setAsCharge(updateProductRuleBenefitItemRate.getAsCharge());
		productRuleBenefitItemRate.setUseMaxSumInsured(updateProductRuleBenefitItemRate.getUseMaxSumInsured());
		productRuleBenefitItemRate.setRate(updateProductRuleBenefitItemRate.getRate());
		productRuleBenefitItemRate.setClassValue(updateProductRuleBenefitItemRate.getClassValue());
		productRuleBenefitItemRate.setActive(updateProductRuleBenefitItemRate.isActive());
		
		productRuleBenefitItemRateRepository.save(productRuleBenefitItemRate);
		ProductRuleBenefitItemRateDto response = modelMapper.map(productRuleBenefitItemRate, ProductRuleBenefitItemRateDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleBenefitItemRate
	public ResponseEntity<ProductRuleBenefitItemRateDto> deleteProductRuleBenefitItemRate(Long id) {
		ProductRuleBenefitItemRate productRuleBenefitItemRate = productRuleBenefitItemRateRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Benefit Item Rate id " + id + " is not exist"));
		productRuleBenefitItemRateRepository.deleteById(id);
		ProductRuleBenefitItemRateDto response = modelMapper.map(productRuleBenefitItemRate, ProductRuleBenefitItemRateDto.class);

		return ResponseEntity.ok(response);
	}	
}