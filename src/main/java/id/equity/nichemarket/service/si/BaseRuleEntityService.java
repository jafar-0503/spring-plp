package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRuleEntity.BaseRuleEntityDto;
import id.equity.nichemarket.dto.si.BaseRuleEntity.CreateBaseRuleEntity;
import id.equity.nichemarket.model.si.BaseRuleEntity;
import id.equity.nichemarket.repository.si.BaseRuleEntityRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleEntityService {

    @Autowired
    private BaseRuleEntityRepository baseRuleEntityRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<BaseRuleEntityDto>> listBaseRuleEntity() {
        List<BaseRuleEntity> lsBaseRuleEntity= baseRuleEntityRepository.findAll();
        Type targetType = new TypeToken<List<BaseRuleEntityDto>>() {}.getType();
        List<BaseRuleEntityDto> response = modelMapper.map(lsBaseRuleEntity, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<BaseRuleEntityDto> getBaseRuleEntityById(Long id) {
        BaseRuleEntity baseRuleEntity = baseRuleEntityRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Entity id " + id + " is not exist"));
        BaseRuleEntityDto response = modelMapper.map(baseRuleEntity, BaseRuleEntityDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<BaseRuleEntityDto> addBaseRuleEntity(CreateBaseRuleEntity newBaseRuleEntity) {
        BaseRuleEntity baseRuleEntity = modelMapper.map(newBaseRuleEntity, BaseRuleEntity.class);
        BaseRuleEntity baseRuleEntitySaved = baseRuleEntityRepository.save(baseRuleEntity);
        BaseRuleEntityDto response = modelMapper.map(baseRuleEntitySaved, BaseRuleEntityDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<BaseRuleEntityDto> editBaseRuleEntity(CreateBaseRuleEntity updateBaseRuleEntity, Long id) {
        BaseRuleEntity baseRuleEntity = baseRuleEntityRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Entity id " + id + " is not exist"));
        baseRuleEntity.setBaseRuleEntitasCode(updateBaseRuleEntity.getBaseRuleEntitasCode());
        baseRuleEntity.setProductCode(updateBaseRuleEntity.getProductCode());
        baseRuleEntity.setPremiumPeriodic(updateBaseRuleEntity.isPremiumPeriodic());
        baseRuleEntity.setTopupPeriodic(updateBaseRuleEntity.isTopupPeriodic());
        baseRuleEntity.setSingleTopup(updateBaseRuleEntity.isSingleTopup());
        baseRuleEntity.setRetirement(updateBaseRuleEntity.isRetirement());
        baseRuleEntity.setRetirementAge(updateBaseRuleEntity.getRetirementAge());
        baseRuleEntity.setIsInterestThp(updateBaseRuleEntity.getIsInterestThp());
        baseRuleEntity.setInterestThp(updateBaseRuleEntity.getInterestThp());
        baseRuleEntity.setAllocationFund(updateBaseRuleEntity.isAllocationFund());
        baseRuleEntity.setMutationFund(updateBaseRuleEntity.isMutationFund());
        baseRuleEntity.setContractPeriod(updateBaseRuleEntity.isContractPeriod());
        baseRuleEntity.setPremiumPeriodic(updateBaseRuleEntity.isPremiumPeriodic());
        baseRuleEntity.setActive(updateBaseRuleEntity.isActive());
        baseRuleEntityRepository.save(baseRuleEntity);
        BaseRuleEntityDto response = modelMapper.map(baseRuleEntity, BaseRuleEntityDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<BaseRuleEntityDto> deleteBaseRuleEntity(Long id) {
        BaseRuleEntity baseRuleEntity = baseRuleEntityRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Entity id " + id + " is not exist"));
        baseRuleEntityRepository.deleteById(id);
        BaseRuleEntityDto response = modelMapper.map(baseRuleEntity, BaseRuleEntityDto.class);

        return ResponseEntity.ok(response);
    }
}