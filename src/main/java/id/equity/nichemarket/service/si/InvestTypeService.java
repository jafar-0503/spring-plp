package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.InvestType.CreateInvestType;
import id.equity.nichemarket.dto.si.InvestType.InvestTypeDto;
import id.equity.nichemarket.model.si.InvestType;
import id.equity.nichemarket.repository.si.InvestRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class InvestTypeService {
	
	@Autowired
	private InvestRepository investRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Invest
	public ResponseEntity<List<InvestTypeDto>> listInvestType(){
		List<InvestType> listInvestTypes = investRepository.findAll();
		Type targetType = new TypeToken<List<InvestTypeDto>>() {}.getType();
		List<InvestTypeDto> response = modelMapper.map(listInvestTypes, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get Invest ById
	public ResponseEntity<InvestTypeDto> getInvestTypeById(Long id) {
		InvestType investType = investRepository.findById(id).orElseThrow(()-> new NotFoundException("Invest Type id " + id + " is not exist"));
		InvestTypeDto response = modelMapper.map(investType, InvestTypeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create Invest
	public ResponseEntity<InvestTypeDto> addInvestType(CreateInvestType newInvest) {
		InvestType investType = modelMapper.map(newInvest, InvestType.class);
		InvestType investTypeSaved = investRepository.save(investType);
		InvestTypeDto response = modelMapper.map(investTypeSaved, InvestTypeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit Invest
	public ResponseEntity<InvestTypeDto> editInvestType(CreateInvestType updateInvest, Long id) {
		InvestType investType = investRepository.findById(id).orElseThrow(()-> new NotFoundException("Invest Type id " + id + " is not exist"));

		//manual map
		investType.setInvestTypeCode(updateInvest.getInvestTypeCode());
		investType.setDescription(updateInvest.getDescription());
		investType.setActive(updateInvest.isActive());;
		
		investRepository.save(investType);
		InvestTypeDto response = modelMapper.map(investType, InvestTypeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete Invest
	public ResponseEntity<InvestTypeDto> deleteInvestType(Long id) {
		InvestType investType = investRepository.findById(id).orElseThrow(()-> new NotFoundException("Invest Type id " + id + " is not exist"));
		
		investRepository.deleteById(id);
		InvestTypeDto response = modelMapper.map(investType, InvestTypeDto.class);

		return ResponseEntity.ok(response);
	}
}