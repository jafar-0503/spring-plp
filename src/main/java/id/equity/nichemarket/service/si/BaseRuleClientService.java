package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRuleClient.BaseRuleClientDto;
import id.equity.nichemarket.dto.si.BaseRuleClient.CreateBaseRuleClient;
import id.equity.nichemarket.model.si.BaseRuleClient;
import id.equity.nichemarket.repository.si.BaseRuleClientRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleClientService {

    @Autowired
    private BaseRuleClientRepository baseRuleClientRepository;

    @Autowired
    private ModelMapper modelMapper;

    //List All Data
    public ResponseEntity<List<BaseRuleClientDto>> listBaseRuleClient() {
        List<BaseRuleClient> ltBaseRuleClient= baseRuleClientRepository.findAll();
        Type targetType = new TypeToken<List<BaseRuleClientDto>>() {}.getType();
        List<BaseRuleClientDto> response = modelMapper.map(ltBaseRuleClient, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<BaseRuleClientDto> getBaseRuleClientById(Long id) {
        BaseRuleClient baseRuleClient = baseRuleClientRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Client id " + id + " is not exist"));
        BaseRuleClientDto response = modelMapper.map(baseRuleClient, BaseRuleClientDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<BaseRuleClientDto> addBaseRuleClient(CreateBaseRuleClient newBaseRuleClient) {
        BaseRuleClient baseRuleClient = modelMapper.map(newBaseRuleClient, BaseRuleClient.class);
        BaseRuleClient baseRuleClientSaved = baseRuleClientRepository.save(baseRuleClient);
        BaseRuleClientDto response = modelMapper.map(baseRuleClientSaved, BaseRuleClientDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<BaseRuleClientDto> editBaseRuleClient(CreateBaseRuleClient updateBaseRuleClient, Long id) {
        BaseRuleClient baseRuleClient = baseRuleClientRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Client id " + id + " is not exist"));
        baseRuleClient.setBaseRuleClientCode(updateBaseRuleClient.getBaseRuleClientCode());
        baseRuleClient.setProductCode(updateBaseRuleClient.getProductCode());
        baseRuleClient.setAsCode(updateBaseRuleClient.getAsCode());
        baseRuleClient.setTotClient(updateBaseRuleClient.getTotClient());
        baseRuleClient.setRequired(updateBaseRuleClient.isRequired());
        baseRuleClient.setActive(updateBaseRuleClient.isActive());

        baseRuleClientRepository.save(baseRuleClient);
        BaseRuleClientDto response = modelMapper.map(baseRuleClient, BaseRuleClientDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<BaseRuleClientDto> deleteBaseRuleClient(Long id) {
        BaseRuleClient baseRuleClient = baseRuleClientRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Client id " + id + " is not exist"));
        baseRuleClientRepository.deleteById(id);
        BaseRuleClientDto response = modelMapper.map(baseRuleClient, BaseRuleClientDto.class);

        return ResponseEntity.ok(response);
    }
}