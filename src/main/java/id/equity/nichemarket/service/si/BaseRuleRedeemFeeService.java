package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRuleRedeemFee.BaseRuleRedeemFeeDto;
import id.equity.nichemarket.dto.si.BaseRuleRedeemFee.CreateBaseRuleRedeemFee;
import id.equity.nichemarket.model.si.BaseRuleRedeemFee;
import id.equity.nichemarket.repository.si.BaseRuleRedeemFeeDtoRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleRedeemFeeService {

    @Autowired
    private BaseRuleRedeemFeeDtoRepository baseRuleRedeemFeeDtoRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All data
    public ResponseEntity<List<BaseRuleRedeemFeeDto>> listBaseRuleRedeemFee() {
        List<BaseRuleRedeemFee> lstBSRFee= baseRuleRedeemFeeDtoRepository.findAll();
        Type targetType = new TypeToken<List<BaseRuleRedeemFeeDto>>() {}.getType();
        List<BaseRuleRedeemFeeDto> response = modelMapper.map(lstBSRFee, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<BaseRuleRedeemFeeDto> getBaseRuleRedeemFeeById(Long id) {
        BaseRuleRedeemFee bSRFee = baseRuleRedeemFeeDtoRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Redeem Fee id " + id + " is not exist"));
        BaseRuleRedeemFeeDto response = modelMapper.map(bSRFee, BaseRuleRedeemFeeDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<BaseRuleRedeemFeeDto> addBaseRuleRedeemFee(CreateBaseRuleRedeemFee newBSRFee) {
        BaseRuleRedeemFee bSRFee = modelMapper.map(newBSRFee, BaseRuleRedeemFee.class);
        BaseRuleRedeemFee bSRFeeSaved = baseRuleRedeemFeeDtoRepository.save(bSRFee);
        BaseRuleRedeemFeeDto response = modelMapper.map(bSRFeeSaved, BaseRuleRedeemFeeDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<BaseRuleRedeemFeeDto> editBaseRuleRedeemFee(CreateBaseRuleRedeemFee updateBSRFee, Long id) {
        BaseRuleRedeemFee bSRFee = baseRuleRedeemFeeDtoRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Redeem Fee id " + id + " is not exist"));
        bSRFee.setBaseRuleRedeemFeeCode(updateBSRFee.getBaseRuleRedeemFeeCode());
        bSRFee.setProductCode(updateBSRFee.getProductCode());
        bSRFee.setValutaCode(updateBSRFee.getValutaCode());
        bSRFee.setPaymentPeriodCode(updateBSRFee.getPaymentPeriodCode());
        bSRFee.setYearTh(updateBSRFee.getYearTh());
        bSRFee.setRate(updateBSRFee.getRate());
        bSRFee.setActive(updateBSRFee.isActive());
        baseRuleRedeemFeeDtoRepository.save(bSRFee);
        BaseRuleRedeemFeeDto response = modelMapper.map(bSRFee, BaseRuleRedeemFeeDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data
    public ResponseEntity<BaseRuleRedeemFeeDto> deleteBaseRuleRedeemFee(Long id) {
        BaseRuleRedeemFee bSRFee = baseRuleRedeemFeeDtoRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Redeem Fee id " + id + " is not exist"));
        baseRuleRedeemFeeDtoRepository.deleteById(id);
        BaseRuleRedeemFeeDto response = modelMapper.map(bSRFee, BaseRuleRedeemFeeDto.class);

        return ResponseEntity.ok(response);
    }
}
