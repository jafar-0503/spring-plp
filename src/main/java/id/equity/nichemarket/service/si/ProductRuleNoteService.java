package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRuleNote.CreateProductRuleNote;
import id.equity.nichemarket.dto.si.ProductRuleNote.ProductRuleNoteDto;
import id.equity.nichemarket.model.si.ProductRuleNote;
import id.equity.nichemarket.repository.si.ProductRuleNoteRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleNoteService {
	
	@Autowired
	private ProductRuleNoteRepository productRuleNoteRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleNote
	public ResponseEntity<List<ProductRuleNoteDto>> listProductRuleNote() {
		List<ProductRuleNote> listProductRuleNotes = productRuleNoteRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleNoteDto>>() {}.getType();
		List<ProductRuleNoteDto> response = modelMapper.map(listProductRuleNotes, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRuleNote By Id
	public ResponseEntity<ProductRuleNoteDto> getProductRuleNoteById(Long id) {
		ProductRuleNote productRuleNote = productRuleNoteRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Note id " + id + " is not exist"));
		ProductRuleNoteDto response = modelMapper.map(productRuleNote, ProductRuleNoteDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleNote
	public ResponseEntity<ProductRuleNoteDto> addProductRuleNote(CreateProductRuleNote newProductRuleNote) {
		ProductRuleNote productRuleNote = modelMapper.map(newProductRuleNote, ProductRuleNote.class);
		ProductRuleNote productRuleNoteSaved = productRuleNoteRepository.save(productRuleNote);
		ProductRuleNoteDto response = modelMapper.map(productRuleNoteSaved, ProductRuleNoteDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleNote
	public ResponseEntity<ProductRuleNoteDto> editProductRuleNote(CreateProductRuleNote updateProductRuleNote, Long id) {
		ProductRuleNote productRuleNote = productRuleNoteRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Note id " + id + " is not exist"));
		
		//manual map
		productRuleNote.setProductRuleNoteCode(updateProductRuleNote.getProductRuleNoteCode());
		productRuleNote.setProductCode(updateProductRuleNote.getProductCode());
		productRuleNote.setValutaCode(updateProductRuleNote.getValutaCode());
		productRuleNote.setPageCode(updateProductRuleNote.getPageCode());
		productRuleNote.setFillPhPbPf(updateProductRuleNote.getFillPhPbPf());
		productRuleNote.setFillHD(updateProductRuleNote.getFillHD());
		productRuleNote.setOrder(updateProductRuleNote.getOrder());
		productRuleNote.setDescription(updateProductRuleNote.getDescription());
		productRuleNote.setOtherDescription(updateProductRuleNote.getOtherDescription());
		productRuleNote.setActive(updateProductRuleNote.isActive());
		
		productRuleNoteRepository.save(productRuleNote);
		ProductRuleNoteDto response = modelMapper.map(productRuleNote, ProductRuleNoteDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleNote
	public ResponseEntity<ProductRuleNoteDto> deleteProductRuleNote(Long id) {
		ProductRuleNote productRuleNote = productRuleNoteRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Note id " + id + " is not exist"));
		productRuleNoteRepository.deleteById(id);
		ProductRuleNoteDto response = modelMapper.map(productRuleNote, ProductRuleNoteDto.class);

		return ResponseEntity.ok(response);
	}	
}