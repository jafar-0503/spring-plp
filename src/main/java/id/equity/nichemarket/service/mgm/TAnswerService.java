package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.TAnswer.CreateTAnswer;
import id.equity.nichemarket.dto.mgm.TAnswer.TAnswerDto;
import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage;
import id.equity.nichemarket.model.mgm.TAnswer;
import id.equity.nichemarket.repository.mgm.TAnswerRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class TAnswerService {

	@Autowired
	private TAnswerRepository tAnswerRepository;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private EntityManager em;
	
	//List TAnswer
	public ResponseEntity<List<TAnswerDto>> listTAnswer(){
		List<TAnswer> lsTAnswer= tAnswerRepository.findAll();
		Type targetType = new TypeToken<List<TAnswerDto>>() {}.getType();
		List<TAnswerDto> response = modelMapper.map(lsTAnswer, targetType);

		return ResponseEntity.ok(response);
	}

	//Get TAnswer By Id
	public ResponseEntity<TAnswerDto> getTAnswerById(Long id) {
		TAnswer tAnswer = tAnswerRepository.findById(id).orElseThrow(()-> new NotFoundException("TAnswer id " + id + " is not exist"));
		TAnswerDto response = modelMapper.map(tAnswer, TAnswerDto.class);

		return ResponseEntity.ok(response);
	}

	//Post TAnswer
	public ResponseEntity<TAnswerDto> addTAnswer(CreateTAnswer newTAnswer) {
		TAnswer tAnswer = modelMapper.map(newTAnswer, TAnswer.class);
		TAnswer tAnswerSaved = tAnswerRepository.save(tAnswer);
		TAnswerDto response = modelMapper.map(tAnswerSaved, TAnswerDto.class);

		return ResponseEntity.ok(response);
	}

	//Put TAnswer
	public ResponseEntity<TAnswerDto> editTAnswer(CreateTAnswer updateTAnswer, Long id) {
		TAnswer tAnswer = tAnswerRepository.findById(id).orElseThrow(()-> new NotFoundException("TAnswer id " + id + " is not exist"));
		tAnswer.setAnswerCode(updateTAnswer.getAnswerCode());
		tAnswer.setCustomerCode(updateTAnswer.getCustomerCode());
		tAnswer.setAppointmentTimeCode(updateTAnswer.getAppointmentTimeCode());
		tAnswer.setQuestion1(updateTAnswer.getQuestion1());
		tAnswer.setQuestion2(updateTAnswer.getQuestion2());
		tAnswer.setQuestion3(updateTAnswer.getQuestion3());
		tAnswer.setQuestion4(updateTAnswer.getQuestion4());
		tAnswer.setQuestion5(updateTAnswer.getQuestion5());
		tAnswer.setQuestion6(updateTAnswer.getQuestion6());
		tAnswer.setQuestion7(updateTAnswer.getQuestion7());
		tAnswer.setProcessed(updateTAnswer.isProcessed());
		tAnswer.setExtractDate(updateTAnswer.getExtractDate());
		tAnswer.setActive(updateTAnswer.isActive());
		tAnswerRepository.save(tAnswer);
		TAnswerDto response = modelMapper.map(tAnswer, TAnswerDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete TAnswer	
	public ResponseEntity<TAnswerDto> deleteTAnswer(Long id) {
		TAnswer findTAnswer = tAnswerRepository.findById(id).orElseThrow(()-> new NotFoundException("TAnswer id " + id + " is not exist"));
		tAnswerRepository.deleteById(id);
		TAnswerDto response = modelMapper.map(findTAnswer, TAnswerDto.class);

		return ResponseEntity.ok(response);
	}

	//Saving data form landing page
	@Transactional
	public TAnswerDto savingTAnswerData(DataFromLandingPage data, String customerCode){
		TAnswer tAnswer = modelMapper.map(data, TAnswer.class);
		tAnswer.setCustomerCode(customerCode);
		tAnswer.setAppointmentTimeCode(data.getAppointmentTimeCode());
		tAnswer.setQuestion1(data.getQuestion1());
		tAnswer.setQuestion2(data.getQuestion2());
		tAnswer.setQuestion3(data.getQuestion3());
		tAnswer.setQuestion4(data.getQuestion4());
		tAnswer.setQuestion5(data.getQuestion5());
		tAnswer.setQuestion6(data.getQuestion6());
		tAnswer.setQuestion7(data.getQuestion7());
		tAnswer.setProcessed(false);
		tAnswer.setActive(true);
		TAnswer tAnswerSaved = tAnswerRepository.save(tAnswer);
		em.refresh(tAnswerSaved);
		TAnswerDto response = modelMapper.map(tAnswerSaved, TAnswerDto.class);
		return response;
	}
}