package id.equity.nichemarket.service.mgm;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.mgm.Customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.LogMessage.CreateLogMessage;
import id.equity.nichemarket.dto.mgm.TAnswer.TAnswerDto;
import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage;
import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage2;
import id.equity.nichemarket.model.mgm.Recipient;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationResponse;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class LandingPageService {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private TAnswerService tAnswerService;
    @Autowired
    private LogMessageService logMessageService;
    @Autowired
    private EmailNotificationService emailNotificationService;

    public String savingDataFromLandingPage(DataFromLandingPage data){
        CustomerDto dataCustomer =  customerService.savingCustomerData(data);
        TAnswerDto dataTAnswer = tAnswerService.savingTAnswerData(data, dataCustomer.getCustomerCode());
        if (dataTAnswer == null){
            return null;
        }
        return dataCustomer.getCustomerCode();
    }

    public EmailNotificationResponse sendEmail(DataFromLandingPage dataFromLandingPage, String customerCode){
        JsonObject dataToSend = generateDataSendEmail(dataFromLandingPage, customerCode);
        EmailNotificationResponse emailSend = emailNotificationService.sendEmailNotif(dataToSend);
        if (emailSend.getError_code().equals("2001")) {
            boolean sentStatus = true;
            String lastSendingRespose = "sent";
            CreateLogMessage logCustomer = convertCustomerToLog(dataFromLandingPage, customerCode,
                    sentStatus, lastSendingRespose);
            logMessageService.savingRecipientNotif(logCustomer);
        }
        return emailSend;
    }

    public JsonObject generateDataSendEmail(DataFromLandingPage dataFromLandingPage, String customerCode){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter dtfHours = DateTimeFormatter.ofPattern("HHmm");
        LocalDateTime localDateNow = LocalDateTime.now();

        JsonObject jsonEmail = new JsonObject();
        JsonObject jsonValue = new JsonObject();
        jsonValue.addProperty("message_type", "E");
        jsonValue.addProperty("type_code", "E24");
        jsonValue.addProperty("variable_no_1", customerCode);
        jsonValue.addProperty("variable_name_1", dataFromLandingPage.getCustomerName());
        jsonValue.addProperty("variable_date_1", dataFromLandingPage.getDateOfBirth());
        jsonValue.addProperty("variable_sum_1", "");
        jsonValue.addProperty("variable_date_2", "");
        jsonValue.addProperty("variable_sum_2", "");
        jsonValue.addProperty("sent_date", dtf.format(localDateNow));
        jsonValue.addProperty("phone_no", dataFromLandingPage.getPhoneNo());
        jsonValue.addProperty("email_address", dataFromLandingPage.getEmailAddress());
        jsonValue.addProperty("valuta_id", "");
        jsonValue.addProperty("va_bca", "");
        jsonValue.addProperty("va_permata", "");
        jsonValue.addProperty("nav", "");
        jsonValue.addProperty("gender_code", dataFromLandingPage.getGenderCode());
        jsonValue.addProperty("variable_no_2", "");
        jsonValue.addProperty("variable_sum_3", "");
        jsonValue.addProperty("variable_name_2", "Produk Perfect Life");
        jsonValue.addProperty("notes", "");
        jsonValue.addProperty("variable_no_3", dtfHours.format(localDateNow));

        jsonEmail.addProperty("username", "API_MGM");
        jsonEmail.addProperty("signature", "I/vWqxM3DmodUjCALPJOhEjzwbo8kWx2EChS1GEHelU=");
        jsonEmail.addProperty("action", "notification");
        jsonEmail.add("value", jsonValue);

        return jsonEmail;
    }

    public EmailNotificationResponse sendNotif(DataFromLandingPage2 data) {
        Gson gson = new Gson();
        JsonObject notifJson = new JsonObject();
        JSONObject dataNas = new JSONObject(data);
        JsonElement dataNasabah = gson.fromJson(dataNas.toString(), JsonElement.class);
        notifJson.addProperty("username", "API_MGM");
        notifJson.addProperty("signature", "I/vWqxM3DmodUjCALPJOhEjzwbo8kWx2EChS1GEHelU=");
        notifJson.addProperty("action", "notification");
        notifJson.add("value", dataNasabah);
        EmailNotificationResponse notifSend = emailNotificationService.sendNotif(notifJson);
        if (notifSend.getError_code().equals("2001") || notifSend.getError_code().equals("6801")) {
            savingLogMessage(data);
        }
        return notifSend;
    }

    private ResponseEntity savingLogMessage(DataFromLandingPage2 data){
        logMessageService.savingFE2(data);
        return ResponseEntity.ok().body(new BaseResponse(true, data.getEmail_address(), "Success"));
    }

    public EmailNotificationResponse sendNotification(Recipient recipients, String timeMinute){
        JsonObject dataToSend = generateEmailToRecipient(recipients, timeMinute);
        EmailNotificationResponse emailSend = emailNotificationService.sendEmailNotif(dataToSend);
        return emailSend;
    }

    public JsonObject generateEmailToRecipient(Recipient recipients, String timeMinute){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime localDateNow = LocalDateTime.now();

        JsonObject jsonEmail = new JsonObject();
        JsonObject jsonValue = new JsonObject();
        jsonValue.addProperty("message_type", "E");
        jsonValue.addProperty("type_code", recipients.getEmailTypeCode());
        jsonValue.addProperty("variable_no_1", recipients.getRecipientCode());
        jsonValue.addProperty("variable_name_1", recipients.getRecipientName());
        jsonValue.addProperty("variable_date_1", "");
        jsonValue.addProperty("variable_sum_1", "");
        jsonValue.addProperty("variable_date_2", "");
        jsonValue.addProperty("variable_sum_2", "");
        jsonValue.addProperty("sent_date", dtf.format(localDateNow));
        jsonValue.addProperty("phone_no", "");
        jsonValue.addProperty("email_address", recipients.getEmailAddress());
        jsonValue.addProperty("valuta_id", "");
        jsonValue.addProperty("va_bca", "");
        jsonValue.addProperty("va_permata", "");
        jsonValue.addProperty("nav", "");
        jsonValue.addProperty("gender_code", "");
        jsonValue.addProperty("variable_no_2", "");
        jsonValue.addProperty("variable_sum_3", "");
        jsonValue.addProperty("variable_name_2", recipients.getPartnerName());
        jsonValue.addProperty("notes", "");
        jsonValue.addProperty("variable_no_3", timeMinute);
        jsonEmail.addProperty("username", "API_MGM");
        jsonEmail.addProperty("signature", "I/vWqxM3DmodUjCALPJOhEjzwbo8kWx2EChS1GEHelU=");
        jsonEmail.addProperty("action", "notification");
        jsonEmail.add("value", jsonValue);

        return jsonEmail;
    }

    public ResponseEntity savingRecipientLogMessage(Recipient recipients, String timing, boolean sent_status, String lastSendingRespose){
        CreateLogMessage savingLogRecipient = convertReceipentToLogMessage(recipients, timing, sent_status, lastSendingRespose);
        logMessageService.savingRecipientNotif(savingLogRecipient);
        return ResponseEntity.ok().body(new BaseResponse(true, recipients.getEmailAddress(), "Success"));
    }

    private CreateLogMessage convertReceipentToLogMessage(Recipient recipient, String timing, boolean sent_status, String lastSendingRespose) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime localDateNow = LocalDateTime.now();
        CreateLogMessage newLogRecipient = new CreateLogMessage();
        newLogRecipient.setMessageType("E");
        newLogRecipient.setTypeCode(recipient.getEmailTypeCode());
        newLogRecipient.setVariableNo1(recipient.getRecipientCode());
        newLogRecipient.setVariableName1(recipient.getRecipientName());
        newLogRecipient.setSendDate(dtf.format(localDateNow));
        newLogRecipient.setEmailAddress(recipient.getEmailAddress());
        newLogRecipient.setVariableName2(recipient.getPartnerName());
        newLogRecipient.setVariableNo3(timing);
        newLogRecipient.setSentStatus(sent_status);
        newLogRecipient.setLastResponseDescription(lastSendingRespose);
        return newLogRecipient;
    }

    private CreateLogMessage convertCustomerToLog(DataFromLandingPage data, String customerCode,
                                                  boolean sentStatus, String lastSendingRespose) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter dtfHours = DateTimeFormatter.ofPattern("HHmm");
        LocalDateTime localDateNow = LocalDateTime.now();
        CreateLogMessage newLogRecipient = new CreateLogMessage();
        newLogRecipient.setMessageType("E");
        newLogRecipient.setTypeCode("E24");
        newLogRecipient.setVariableNo1(customerCode);
        newLogRecipient.setVariableName1(data.getCustomerName());
        newLogRecipient.setVariableDate1(data.getDateOfBirth());
        newLogRecipient.setSendDate(dtf.format(localDateNow));
        newLogRecipient.setPhoneNo(data.getPhoneNo());
        newLogRecipient.setEmailAddress(data.getEmailAddress());
        newLogRecipient.setGenderCode(data.getGenderCode());
        newLogRecipient.setVariableNo3(dtfHours.format(localDateNow));
        newLogRecipient.setSentStatus(sentStatus);
        newLogRecipient.setLastResponseDescription(lastSendingRespose);
        newLogRecipient.setActive(true);
        return newLogRecipient;
    }

}
