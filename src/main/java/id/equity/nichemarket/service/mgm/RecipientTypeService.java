package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.Recipient.CreateRecipient;
import id.equity.nichemarket.dto.mgm.Recipient.RecipientDto;
import id.equity.nichemarket.dto.mgm.RecipientType.CreateRecipientType;
import id.equity.nichemarket.dto.mgm.RecipientType.RecipientTypeDto;
import id.equity.nichemarket.model.mgm.Recipient;
import id.equity.nichemarket.model.mgm.RecipientType;
import id.equity.nichemarket.repository.mgm.RecipientTypeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class RecipientTypeService {

	@Autowired
	private RecipientTypeRepository recipientTypeRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Recipient Type
	public ResponseEntity<List<RecipientTypeDto>> listRecipientType(){
		List<RecipientType> lsRecipientType = recipientTypeRepository.findAll();
		Type targetType = new TypeToken<List<RecipientTypeDto>>() {}.getType();
		List<RecipientTypeDto> response = modelMapper.map(lsRecipientType, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Recipient Type By Id
	public ResponseEntity<RecipientTypeDto> getRecipientTypeById(Long id) {
		RecipientType recipientType = recipientTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Recipient Type id " + id + " is not exist"));
		RecipientTypeDto response = modelMapper.map(recipientType, RecipientTypeDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Recipient Type
	public ResponseEntity<RecipientTypeDto> addRecipientType(CreateRecipientType newRecipientType) {
		RecipientType recipientType = modelMapper.map(newRecipientType, RecipientType.class);
		RecipientType recipientTypeSaved = recipientTypeRepository.save(recipientType);
		RecipientTypeDto response = modelMapper.map(recipientTypeSaved, RecipientTypeDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Recipient Type
	public ResponseEntity<RecipientTypeDto> editRecipientType(CreateRecipientType updateRecipientType, Long id) {
		RecipientType recipientType = recipientTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Recipient Type id " + id + " is not exist"));
		recipientType.setRecipientTypeCode(updateRecipientType.getRecipientTypeCode());
		recipientType.setDescription(updateRecipientType.getDescription());
		recipientType.setInternal(updateRecipientType.isInternal());
		recipientType.setActive(updateRecipientType.isActive());
		recipientTypeRepository.save(recipientType);
		RecipientTypeDto response = modelMapper.map(recipientType, RecipientTypeDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Recipient Type
	public ResponseEntity<RecipientTypeDto> deleteRecipientType(Long id) {
		RecipientType findRecipientType = recipientTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Recipient Type id " + id + " is not exist"));
		recipientTypeRepository.deleteById(id);
		RecipientTypeDto response = modelMapper.map(findRecipientType, RecipientTypeDto.class);

		return ResponseEntity.ok(response);
	}
}