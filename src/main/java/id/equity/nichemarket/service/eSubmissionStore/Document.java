package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class Document {
    private String file;
}
