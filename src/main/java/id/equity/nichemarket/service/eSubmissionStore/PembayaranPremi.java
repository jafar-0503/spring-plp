package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class PembayaranPremi {
    private String cara_pembayaran_premi_lanjutan;
    private NamaBank nama_bank;
}
