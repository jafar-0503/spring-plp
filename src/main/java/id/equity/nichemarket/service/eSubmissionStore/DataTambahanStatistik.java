package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class DataTambahanStatistik {
    private StatusTempatTinggal status_tempat_tinggal;
    private String penghasilan_kotor_per_tahun;
    private SumberDana sumber_dana;
    private TujuanMembeliAsuransi tujuan_membeli_asuransi;
}

