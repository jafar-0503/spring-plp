package id.equity.nichemarket.service.eSubmissionStore.documentData;

import lombok.Data;

@Data
public class DocumentData {
    private String no_spaj;
    private Document document;
    private Integer total;
}
