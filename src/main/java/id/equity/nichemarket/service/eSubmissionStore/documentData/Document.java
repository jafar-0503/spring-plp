package id.equity.nichemarket.service.eSubmissionStore.documentData;

import lombok.Data;

@Data
public class Document {
    private String id;
    private String file;
    private String dms_code;
    private String document_name;
    private String order;
}
