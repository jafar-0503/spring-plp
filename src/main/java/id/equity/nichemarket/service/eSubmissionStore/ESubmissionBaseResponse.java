package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ESubmissionBaseResponse <T> {
    private String error_code;
    private String error_description;
    private T data;
    private Integer total_data;

    public ESubmissionBaseResponse(String error_code, String error_description, T data, Integer total_data) {
        this.error_code = error_code;
        this.error_description = error_description;
        this.data = data;
        this.total_data = total_data;
    }
}
