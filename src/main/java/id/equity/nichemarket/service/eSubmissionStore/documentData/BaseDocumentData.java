package id.equity.nichemarket.service.eSubmissionStore.documentData;

import lombok.Data;

@Data
public class BaseDocumentData {
    private DocumentData document_data;
}
