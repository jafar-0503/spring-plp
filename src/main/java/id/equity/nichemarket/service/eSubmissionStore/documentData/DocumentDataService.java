package id.equity.nichemarket.service.eSubmissionStore.documentData;

import id.equity.nichemarket.model.es.SpajDocumentFile;
import id.equity.nichemarket.repository.es.SpajDocumentFileRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DocumentDataService {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private SpajDocumentFileRepository spajDocumentFileRepository;

    public void save(BaseDocumentData documentsValue){
        SpajDocumentFile existData = spajDocumentFileRepository.findByNoSpajDmsCode(
                documentsValue.getDocument_data().getNo_spaj(),
                documentsValue.getDocument_data().getDocument().getDms_code()
        );

        if (existData != null) {
            existData.setFile(documentsValue.getDocument_data().getDocument().getFile());
            existData.setDocumentName(documentsValue.getDocument_data().getDocument().getDocument_name());
            spajDocumentFileRepository.save(existData);
        } else {

            SpajDocumentFile spajDocumentFile = modelMapper.map(documentsValue, SpajDocumentFile.class);
            spajDocumentFile.setSpajDocumentFileCode(documentsValue.getDocument_data().getDocument().getId());
            spajDocumentFile.setFile(documentsValue.getDocument_data().getDocument().getFile());
            spajDocumentFile.setDmsCode(documentsValue.getDocument_data().getDocument().getDms_code());
            spajDocumentFile.setDocumentName(documentsValue.getDocument_data().getDocument().getDocument_name());
            spajDocumentFile.setSpajNo(documentsValue.getDocument_data().getNo_spaj());
            spajDocumentFile.setActive(true);
            spajDocumentFileRepository.save(spajDocumentFile);
        }
    }

}
