package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class ESubmissionDataToEdits {
    private ESubmissionDataService data;
}
