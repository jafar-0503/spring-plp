package id.equity.nichemarket.service.eSubmissionStore.CalonTertanggung;

import lombok.Data;

@Data
public class AlamatKorespondensiLainnya {
    private String alamat_lainnya_1;
    private String alamat_lainnya_2;
    private String alamat_lainnya_3;
    private String kota;
    private String kode_pos;
    private String kode_provinsi;
    private String kode_negara;
    private String telp;
    private String hp;
}
