package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class Termaslahat {
    private String yang_menerima_manfaat;
    private String hub_dengan_calon_tertanggung;
    private String tanggal_lahir;
    private String jenis_kelamin;
    private String persentase;
}
