package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name="m_products")
public class Product extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_code")
	private String productCode;
	
	@Column(nullable = false, length = 8, name = "parent_product_code")
	private String parentProductCode;
	
	@Column(nullable = false, length = 10, name = "product_code_mapping")
	private String productCodeMapping;
	
	@Column(nullable = false, length = 30, name = "description")
	private String description;
	
	@Column(nullable = false, name = "\"order\"")
	private Integer order;
	
	@Column(nullable = false, name = "is_basic")
	private boolean isBasic;
	
	@Column(nullable = false, name = "is_class")
	private boolean isClass;
	
	@Column(nullable = false, name = "is_risk")
	private boolean isRisk;
	
	@Column(nullable = false, name = "is_contract_period")
	private boolean isContractPeriod;
	
	@Column(nullable = false, name = "is_premium_period")
	private boolean isPremiumPeriod;
	
	@Column(nullable = false, name = "is_sum_insured")
	private boolean isSumInsured;
	
	@Column(nullable = false, name = "is_premium")
	private boolean isPremium;
	
	@Column(nullable = false, name = "is_activate")
	private boolean isActivate;
	
	@Column(nullable = false, name = "is_required")
	private boolean isRequired;
	
	@Column(nullable = false, name = "is_payor")
	private boolean isPayor;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}