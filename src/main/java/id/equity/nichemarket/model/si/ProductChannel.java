package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name="m_product_channel")
public class ProductChannel extends Auditable<String> {

    @Column(unique = true, length = 10, nullable = false, name = "product_channel_code")
    private String productChannelCode;

    @Column(length = 10, nullable = false, name = "channel_code")
    private String channelCode;

    @Column(length = 10, nullable = false, name = "product_code")
    private String productCode;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
