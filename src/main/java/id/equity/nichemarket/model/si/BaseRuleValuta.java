package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_valuta")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class BaseRuleValuta extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "base_rule_single_topup_code")
	private String baseRuleSingleTopupCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 10, name = "valuta_code")
	private String valutaCode;
	
	@Column(name = "is_active")
	private boolean isActive;
}
