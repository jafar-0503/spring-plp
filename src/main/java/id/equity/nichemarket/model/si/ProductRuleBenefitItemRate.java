package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_benefit_item_rate")
public class ProductRuleBenefitItemRate extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_benefit_item_rate_code")
	private String productRuleBenefitItemRateCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(nullable = false, name = "\"order\"")
	private Integer order;

	@Column(length = 12, name = "\"class\"")
	private String classes;

	@Column(length = 12, name = "asCharge")
	private String asCharge;

	@Column(length = 12, name = "use_max_sum_insured")
	private String useMaxSumInsured;

	@Column(nullable = false, name = "rate")
	private Double rate;

	@Column(length = 12, name = "class_value")
	private String classValue;
	
	@Column(name = "is_active")
	private boolean isActive;
}