package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_contract_period")
public class ProductRuleContractPeriod extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_contract_period_code")
	private String productRuleContractPeriodCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 30, name = "min_contract_period")
	private String minContractPeriod;

	@Column(length = 30, name = "max_contract_period")
	private String maxContractPeriod;

	@Column(nullable = false, name = "step_contract_period")
	private Integer stepContractPeriod;

	@Column(nullable = false, name = "max_age_contract_period")
	private Integer maxAgeContractPeriod;

	@Column(name = "is_active")
	private boolean isActive;
}