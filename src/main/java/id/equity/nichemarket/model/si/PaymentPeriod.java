package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import id.equity.nichemarket.audit.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "m_payment_period")
public class PaymentPeriod extends Auditable<String>{
	
	@Column(unique = true, length = 3, nullable = false, name = "payment_period_code")
	private String paymentPeriodCode;
	
	@Column(length = 30, nullable = false, name = "description")
	private String description;
	
	@Column(nullable = false, name = "tot_year")
	private Integer totYear;
	
	@Column(nullable = false, name = "tot_month")
	private Integer totMonth;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}
