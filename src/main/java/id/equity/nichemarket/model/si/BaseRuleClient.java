package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_client")
public class BaseRuleClient extends Auditable<String> {

    @Column(unique = true, length = 6, nullable = false, name = "base_rule_client_code")
    private String baseRuleClientCode;

    @Column(nullable = false, length = 10, name = "product_code")
    private String productCode;

    @Column(nullable = false, length = 10, name = "as_code")
    private String asCode;

    @Column(nullable = false, name = "tot_client")
    private Integer totClient;

    @Column(nullable = false, name = "is_required")
    private boolean isRequired;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
