package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_redeem")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class BaseRuleRedeem extends Auditable<String> {
	
	@Column(unique = true, length = 6, nullable = false, name = "base_rule_redeem_code")
	private String baseRuleRedeemCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(name = "year_th")
	private Integer yearTh;
	
	@Column(name = "age_th")
	private Integer ageTh;
	
	@Column(name = "percent")
	private Double percent;
	
	@Column(name = "is_active")
	private boolean isActive;
}