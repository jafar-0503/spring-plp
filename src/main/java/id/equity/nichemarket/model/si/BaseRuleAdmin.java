package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_admin_fee")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class BaseRuleAdmin extends Auditable<String> {
	
	@Column(unique = true, length = 6, nullable = false, name = "base_rule_admin_fee_code")
	private String baseRuleAdminFeeCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 6, name = "valuta_code")
	private String valutaCode;
	
	@Column(length = 3, name = "payment_period_code")
	private String paymentPeriodCode;
	
	@Column(length = 3, name = "rate")
	private int rate;
	
	@Column(length = 2, name = "is_active")
	private boolean isActive;
}
