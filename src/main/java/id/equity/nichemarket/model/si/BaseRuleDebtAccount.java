package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_debt_account")
public class BaseRuleDebtAccount extends Auditable<String> {

    @Column(unique = true, length = 6, nullable = false, name = "base_rule_debt_account_code")
    private String baseRuleDebtAccountCode;

    @Column(length = 10, nullable = false, name = "product_code")
    private String productCode;

    @Column(nullable = false, name = "from_year_th")
    private Integer fromYearTh;

    @Column(nullable = false, name = "from_year_percen")
    private Double fromYearPercen;

    @Column(nullable = false, name = "to_year_th")
    private Integer toYearTh;

    @Column(nullable = false, name = "to_year_percen")
    private Double toYearPercen;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;

}
