package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import id.equity.nichemarket.audit.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_acquisition_fee")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class BaseRuleAcquisitionFee extends Auditable<String>{

	@Column(unique = true, length = 6, nullable = false, name = "base_rule_acquisition_fee_code")
	private String baseRuleAcquisitionFeeCode;

	@Column(nullable = false, length = 10, name = "product_code")
	private String productCode;

	@Column(nullable = false, length = 6, name = "valuta_code")
	private String valutaCode;

	@Column(nullable = false, length = 3, name = "payment_period_code")
	private String paymentPeriodCode;

	@Column(nullable = false, name = "year_th")
	private int yearTh;

	@Column(nullable = false, name = "rate")
	private Double rate;

	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}
