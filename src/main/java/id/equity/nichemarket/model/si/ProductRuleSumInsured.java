package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_sum_insured")
public class ProductRuleSumInsured extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_sum_insured_code")
	private String productRuleSumInsuredCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 6, name = "valuta_code")
	private String valutaCode;

	@Column(length = 10, name = "payment_period_code")
	private String paymentPeriodCode;

	@Column(length = 2, name = "as_code")
	private String asCode;

	@Column(nullable = false, name = "min_age")
	private Integer minAge;

	@Column(nullable = false, name = "max_age")
	private Integer maxAge;

	@Column(nullable = false, name = "min_premium_periodic")
	private Double minPremiumPeriodic;

	@Column(nullable = false, name = "max_premium_periodic")
	private Double maxPremiumPeriodic;

	@Column(nullable = false, name = "min_sum_insured_base")
	private Double minSumInsuredBase;

	@Column(nullable = false, name = "max_sum_insured_base")
	private Double maxSumInsuredBase;

	@Column(length = 12, name = "\"class\"")
	private String classes;

	@Column(length = 12, name = "risk")
	private String risk;

	@Column(length = 100, name = "min_sum_insured_formula")
	private String minSumInsuredFormula;

	@Column(length = 100, name = "max_sum_insured_formula")
	private String maxSumInsuredFormula;

	@Column(nullable = false, name = "step_sum_insured")
	private Double stepSumInsured;

	@Column(name = "is_active")
	private boolean isActive;
}