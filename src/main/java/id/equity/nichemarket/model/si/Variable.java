package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import id.equity.nichemarket.audit.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "s_variable")
public class Variable extends Auditable<String>{

	@Column(unique = true, length = 10, nullable = false, name = "variable_code")
	private String variableCode;

	@Column(length = 50, nullable = false, name = "description")
	private String description;

	@Column(length = 20, nullable = false, name = "data_type")
	private String dataType;

	@Column(length = 20, nullable = false, name = "block_name")
	private String blockName;

	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}
