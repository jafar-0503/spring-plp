package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import id.equity.nichemarket.audit.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_service_unit")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class ServiceUnit extends Auditable<String> {

	@Column(unique = true, length = 6, nullable = false, name = "service_unit_code")
	private String serviceUnitCode;

	@Column(length = 30, nullable = false, name = "description")
	private String description;

	@Column(nullable = false, name = "is_active")
	private boolean isActive;
	
}
