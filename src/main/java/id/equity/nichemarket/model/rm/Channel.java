package id.equity.nichemarket.model.rm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="m_channel")
public class Channel extends Auditable<String>{
	@Column(unique = true, length = 10, nullable = false, name = "channel_code")
	private String channelCode;

	@Column(length = 10, name = "channel_code_mapp")
	private String channelCodeMapp;

	@Column(length = 30, name = "description")
	private String description;

	@Column(name = "is_active")
	private boolean isActive;
}
