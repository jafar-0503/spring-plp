package id.equity.nichemarket.model.rm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="m_genderRM")
public class GenderRM extends Auditable<String>{
	@Column(unique = true, length = 1, nullable = false, name = "gender_code")
	private String genderCode;

	@Column(length = 100, name = "description")
	private String description;

	@Column(name = "is_active")
	private boolean isActive;
}
