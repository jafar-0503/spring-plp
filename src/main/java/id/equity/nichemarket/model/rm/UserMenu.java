package id.equity.nichemarket.model.rm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="m_user_menu")
public class UserMenu extends Auditable<String>{
	@Column(unique = true, length = 30, nullable = false, name = "user_menu_code")
	private String userMenuCode;

	@Column(length = 30, name = "username")
	private String username;

	@Column(length = 10, name = "menu_code")
	private String menuCode;

	@Column(name = "is_active")
	private boolean isActive;
}
