package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="m_gender")
public class Gender extends Auditable<String>{
	@Column(unique = true, length = 1, nullable = false, name = "gender_code")
	private String genderCode;

	@Column(length = 100, nullable = false, name = "description")
	private String description;

	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}
