package id.equity.nichemarket.model.es;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "m_religion")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Religion extends Auditable<String>{
	
	@Column(unique = true, length = 1, nullable = false, name = "religion_code")
    private String religionCode;
	
	@Column(length = 100, nullable = false, name = "description")
    private String description;
	
	@Column(length = 5, nullable = false, name = "is_active")
    private boolean isActive;
}