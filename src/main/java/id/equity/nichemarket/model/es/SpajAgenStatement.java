package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj_agen_statement")
public class SpajAgenStatement extends Auditable<String> {

    @Column(unique = true, length = 12, name = "spaj_agen_statement_code")
    private String spajAgenStatementCode;

    @Column(length = 12, name = "spaj_document_code")
    private String spajDocumentCode;

    @Column(length = 12, name = "spaj_no")
    private String spajNo;

    @Column(length = 60, name = "agen_name")
    private String agenName;

    @Column(length = 20, name = "agen_code")
    private String agenCode;

    @Column(length = 5, name = "position")
    private String position;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
