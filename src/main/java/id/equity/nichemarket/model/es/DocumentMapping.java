package id.equity.nichemarket.model.es;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "m_document_mapping")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DocumentMapping extends Auditable<String>{
	
	@Column(unique = true, length = 12, name = "document_mapping_code")
    private String documentMappingCode;
	
	@Column(length = 6, name = "mapping_code")
    private String mappingCode;
	
	@Column(length = 11, name = "description")
    private String description;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}