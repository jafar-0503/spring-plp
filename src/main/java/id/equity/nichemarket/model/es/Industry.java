package id.equity.nichemarket.model.es;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "m_industry")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Industry extends Auditable<String>{
	
	@Column(unique = true, length = 6, nullable = false, name = "industry_code")
    private String industryCode;
	
	@Column(length = 100, nullable = false, name = "description")
    private String description;
	
	@Column(nullable = false, name = "is_active")
    private boolean isActive;
}