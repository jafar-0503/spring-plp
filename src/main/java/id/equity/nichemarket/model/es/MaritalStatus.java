package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_marital_status")
public class MaritalStatus extends Auditable<String> {

    @Column(unique = true, length = 2, nullable = false, name = "marital_status_code")
    private String maritalStatusCode;
    @Column(length = 30, nullable = false, name = "description")
    private String description;
    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
