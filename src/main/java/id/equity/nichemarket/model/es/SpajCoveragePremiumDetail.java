package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj_coverage_premium_detail")
public class SpajCoveragePremiumDetail extends Auditable<String> {

    @Column(unique = true, length = 12, name = "spaj_coverage_premium_detail_code")
    private String spajCoveragePremiumDetailCode;

    @Column(length = 12, name = "spaj_document_code")
    private String spajDocumentCode;

    @Column(length = 12, name = "spaj_no")
    private String spajNo;

    @Column(length = 10, name = "currency")
    private String currency;

    @Column(length = 2, name = "premium_payment_period")
    private String premiumPaymentPeriod;

    @Column(length = 50, name = "p_unit_link_premium")
    private String pUnitLinkPremium;

    @Column(length = 50, name = "p_unit_link_total_premium")
    private String pUnitLinkTotalPremium;

    @Column(length = 50, name = "p_unit_link_basic_plan")
    private String pUnitLinkBasicPlan;

    @Column(length = 50, name = "p_unit_link_sum_insured")
    private String pUnitLinkSumInsured;

    @Column(length = 2, name = "p_unit_link_premium_payment_period")
    private String pUnitLinkPremiumPaymentPeriod;

    @Column(length = 30, name = "p_unit_link_contract_period")
    private String pUnitLinkContractPeriod;

    @Column(length = 60, name = "p_unit_link_rider_1")
    private String pUnitLinkRider1;

    @Column(length = 50, name = "p_unit_link_sum_insured_rider_1")
    private String pUnitLinkSumInsuredRider1;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
