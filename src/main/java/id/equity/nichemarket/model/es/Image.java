package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_image")
public class Image extends Auditable<String> {

    @Column(unique = true, length = 10, nullable = false, name = "image_code")
    private String imageCode;
    @Column(length = 50, nullable = false, name = "description")
    private String description;
    @Column(nullable = false, name = "object_image")
    private String objectImage;
    @Column(nullable = false, name = "is_active")
    private boolean isActive;

}
