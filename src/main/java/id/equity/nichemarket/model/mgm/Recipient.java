package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_recipient")
public class Recipient extends Auditable<String> {
		
	@Column(unique = true, length = 10, nullable = false, name = "recipient_code")
	private String recipientCode;
	
	@Column(nullable = false, length = 255, name = "recipient_name")
	private String recipientName;

	@Column(nullable = false, length = 100, name = "email_address")
	private String emailAddress;

	@Column(nullable = false, length = 3, name = "recipient_type_code")
	private String recipientTypeCode;

	@Column(nullable = false, length = 100, name = "partner_name")
	private String partnerName;

	@Column(nullable = false, length = 3, name = "email_type_code")
	private String emailTypeCode;

	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}