package id.equity.nichemarket.config.response;

import lombok.Data;

@Data
public class NewBaseResponse<T> {
    private Integer status;
    private String proposal_no;
    private String spaj_no;

    public NewBaseResponse(Integer status, String spaj_no, String proposal_no) {
        this.status = status;
        this.spaj_no = spaj_no;
        this.proposal_no = proposal_no;
    }
}
