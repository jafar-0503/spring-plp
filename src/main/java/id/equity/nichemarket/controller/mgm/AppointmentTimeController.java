package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.AppointmentTime.CreateAppointmentTime;
import id.equity.nichemarket.dto.mgm.AppointmentTime.AppointmentTimeDto;
import id.equity.nichemarket.service.mgm.AppointmentTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/appointment-times")
public class AppointmentTimeController {

	@Autowired
	private AppointmentTimeService appointmentTimeService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<AppointmentTimeDto>> listAppointmentTime(){
		return appointmentTimeService.listAppointmentTime();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<AppointmentTimeDto> getAppointmentTimeById(@PathVariable Long id) {
		return appointmentTimeService.getAppointmentTimeById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<AppointmentTimeDto> addAppointmentTime(@RequestBody CreateAppointmentTime newAppointmentTime) {
		return appointmentTimeService.addAppointmentTime(newAppointmentTime);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<AppointmentTimeDto> editAppointmentTime(@RequestBody CreateAppointmentTime updateAppointmentTime, @PathVariable Long id) {
		return appointmentTimeService.editAppointmentTime(updateAppointmentTime, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<AppointmentTimeDto> deleteInvest(@PathVariable Long id) {
		return appointmentTimeService.deleteAppointmentTime(id);
	}	
}