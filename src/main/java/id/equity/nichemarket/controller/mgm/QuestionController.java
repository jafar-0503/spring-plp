package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.Question.QuestionDto;
import id.equity.nichemarket.dto.mgm.Question.CreateQuestion;
import id.equity.nichemarket.service.mgm.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/questions")
public class QuestionController {

	@Autowired
	private QuestionService questionService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<QuestionDto>> listQuestion(){
		return questionService.listQuestion();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<QuestionDto> getQuestionById(@PathVariable Long id) {
		return questionService.getQuestionById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<QuestionDto> addQuestion(@RequestBody CreateQuestion newQuestion) {
		return questionService.addQuestion(newQuestion);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<QuestionDto> editQuestion(@RequestBody CreateQuestion updateQuestion, @PathVariable Long id) {
		return questionService.editQuestion(updateQuestion, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<QuestionDto> deleteInvest(@PathVariable Long id) {
		return questionService.deleteQuestion(id);
	}	
}