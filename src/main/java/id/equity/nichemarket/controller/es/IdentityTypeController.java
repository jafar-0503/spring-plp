package id.equity.nichemarket.controller.es;

import java.util.List;


import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.IdentityType.CreateIdentityType;
import id.equity.nichemarket.dto.es.IdentityType.IdentityTypeDto;
import id.equity.nichemarket.service.es.IdentityTypeService;
@RestController
@RequestMapping("api/v1/identity-types")
public class IdentityTypeController {
	
	@Autowired
	private IdentityTypeService identityTypeService;
	
	//get All IdentityType
	@GetMapping
	public ResponseEntity<List<IdentityTypeDto>> listIdentityType(){
		return identityTypeService.listIdentityType();
	}
	
	//Get IdentityType By Id
	@GetMapping("{id}")
	public ResponseEntity<IdentityTypeDto> getIdentityTypeById(@PathVariable Long id) {
		return identityTypeService.getIdentityTypeById(id);
	}
	
	//Post IdentityType
	@PostMapping
	public ResponseEntity<IdentityTypeDto> addIdentityType(@RequestBody CreateIdentityType newIdentityType) {
		return identityTypeService.addIdentityType(newIdentityType);
	}
	
	//Put IdentityType
	@PutMapping("{id}")
	public ResponseEntity<IdentityTypeDto> editIdentityType(@RequestBody CreateIdentityType updateIdentityType, @PathVariable Long id) {
		return identityTypeService.editIdentityType(updateIdentityType, id);
	}
	
	//Delete IdentityType
	@DeleteMapping("{id}")
	public ResponseEntity<IdentityTypeDto> deleteIdentityType(@PathVariable Long id) {
		return identityTypeService.deleteIdentityType(id);
	}
}