package id.equity.nichemarket.controller.es;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.CardIssuer.CardIssuerDto;
import id.equity.nichemarket.dto.es.CardIssuer.CreateCardIssuer;
import id.equity.nichemarket.service.es.CardIssuerService;

@RestController
@RequestMapping("api/v1/card-issuers")
public class CardIssuerController {
	
	@Autowired
	private CardIssuerService cardIssuerService;
	
	//get All CardIssuer
	@GetMapping
	public ResponseEntity<List<CardIssuerDto>> listCardIssuer(){
		return cardIssuerService.listCardIssuer();
	}
	
	//Get CardIssuer By Id
	@GetMapping("{id}")
	public ResponseEntity<CardIssuerDto> getCardIssuerById(@PathVariable Long id) {
		return cardIssuerService.getCardIssuerById(id);
	}
	
	//Post CardIssuer
	@PostMapping
	public ResponseEntity<CardIssuerDto> addCardIssuer(@RequestBody CreateCardIssuer newCardIssuer) {
		return cardIssuerService.addCardIssuer(newCardIssuer);
	}
	
	//Put CardIssuer
	@PutMapping("{id}")
	public ResponseEntity<CardIssuerDto> editCardIssuer(@RequestBody CreateCardIssuer updateCardIssuer, @PathVariable Long id) {
		return cardIssuerService.editCardIssuer(updateCardIssuer, id);
	}
	
	//Delete CardIssuer
	@DeleteMapping("{id}")
	public ResponseEntity<CardIssuerDto> deleteCardIssuer(@PathVariable Long id) {
		return cardIssuerService.deleteCardIssuer(id);
	}
}