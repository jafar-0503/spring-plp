package id.equity.nichemarket.controller.es;

import java.util.List;


import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.OccupationGroup.CreateOccupationGroup;
import id.equity.nichemarket.dto.es.OccupationGroup.OccupationGroupDto;
import id.equity.nichemarket.service.es.OccupationGroupService;

@RestController
@RequestMapping("api/v1/occupation-groups")
public class OccupationGroupController {
	
	@Autowired
	private OccupationGroupService occupationGroupService;
	
	//get All OccupationGroup
	@GetMapping
	public ResponseEntity<List<OccupationGroupDto>> listOccupationGroup(){
		return occupationGroupService.listOccupationGroup();
	}
	
	//Get OccupationGroup By Id
	@GetMapping("{id}")
	public ResponseEntity<OccupationGroupDto> getOccupationGroupById(@PathVariable Long id) {
		return occupationGroupService.getOccupationGroupById(id);
	}
	
	//Post OccupationGroup
	@PostMapping
	public ResponseEntity<OccupationGroupDto> addOccupationGroup(@RequestBody CreateOccupationGroup newOccupationGroup) {
		return occupationGroupService.addOccupationGroup(newOccupationGroup);
	}
	
	//Put OccupationGroup
	@PutMapping("{id}")
	public ResponseEntity<OccupationGroupDto> editOccupationGroup(@RequestBody CreateOccupationGroup updateOccupationGroup, @PathVariable Long id) {
		return occupationGroupService.editOccupationGroup(updateOccupationGroup, id);
	}
	
	//Delete OccupationGroup
	@DeleteMapping("{id}")
	public ResponseEntity<OccupationGroupDto> deleteOccupationGroup(@PathVariable Long id) {
		return occupationGroupService.deleteOccupationGroup(id);
	}
}