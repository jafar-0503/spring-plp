package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.dto.es.Income.CreateIncome;
import id.equity.nichemarket.dto.es.Income.IncomeDto;
import id.equity.nichemarket.service.es.IncomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/incomes")
public class IncomeController {

    @Autowired
    private IncomeService incomeService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<IncomeDto>> listIncome(){
        return incomeService.listIncome();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<IncomeDto> getIncomeById(@PathVariable Long id) {
        return incomeService.getIncomeById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<IncomeDto> addIncome (@RequestBody CreateIncome newIncome) {
        return incomeService.addIncome(newIncome);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<IncomeDto> editIncome(@RequestBody CreateIncome updateIncome, @PathVariable Long id) {
        return incomeService.editIncome(updateIncome, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<IncomeDto> deleteIncome(@PathVariable Long id) {
        return incomeService.deleteIncome(id);
    }
}