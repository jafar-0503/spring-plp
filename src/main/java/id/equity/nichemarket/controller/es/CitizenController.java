package id.equity.nichemarket.controller.es;

import java.util.List;


import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.Citizen.CitizenDto;
import id.equity.nichemarket.dto.es.Citizen.CreateCitizen;
import id.equity.nichemarket.service.es.CitizenService;

@RestController
@RequestMapping("api/v1/citizens")
public class CitizenController {
	
	@Autowired
	private CitizenService citizenService;
	
	//get All Citizen
	@GetMapping
	public ResponseEntity<List<CitizenDto>> listCitizen(){
		return citizenService.listCitizen();
	}
	
	//Get Citizen By Id
	@GetMapping("{id}")
	public ResponseEntity<CitizenDto> getCitizenById(@PathVariable Long id) {
		return citizenService.getCitizenById(id);
	}
	
	//Post Citizen
	@PostMapping
	public ResponseEntity<CitizenDto> addCitizen(@RequestBody CreateCitizen newCitizen) {
		return citizenService.addCitizen(newCitizen);
	}
	
	//Put Citizen
	@PutMapping("{id}")
	public ResponseEntity<CitizenDto> editCitizen(@RequestBody CreateCitizen updateCitizen, @PathVariable Long id) {
		return citizenService.editCitizen(updateCitizen, id);
	}
	
	//Delete Citizen
	@DeleteMapping("{id}")
	public ResponseEntity<CitizenDto> deleteCitizen(@PathVariable Long id) {
		return citizenService.deleteCitizen(id);
	}
}