package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.DocumentMapping.CreateDocumentMapping;
import id.equity.nichemarket.dto.es.DocumentMapping.DocumentMappingDto;
import id.equity.nichemarket.service.es.DocumentMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/document-mapping")
public class DocumentMappingController {
	
	@Autowired
	private DocumentMappingService documentMappingService;
	
	//Get All DocumentMapping
	@GetMapping
	public ResponseEntity<List<DocumentMappingDto>> listDocumentMapping(){
		return documentMappingService.listDocumentMapping();
	}
	
	//Get DocumentMapping By Id
	@GetMapping("{id}")
	public ResponseEntity<DocumentMappingDto> getDocumentMappingById(@PathVariable Long id) {
		return documentMappingService.getDocumentMappingById(id);
	}
	
	//Post DocumentMapping
	@PostMapping
	public ResponseEntity<DocumentMappingDto> addDocumentMapping(@RequestBody CreateDocumentMapping newDocumentMapping) {
		return documentMappingService.addDocumentMapping(newDocumentMapping);
	}
	
	//Put DocumentMapping
	@PutMapping("{id}")
	public ResponseEntity<DocumentMappingDto> editDocumentMapping(@RequestBody CreateDocumentMapping updateDocumentMapping, @PathVariable Long id) {
		return documentMappingService.editDocumentMapping(updateDocumentMapping, id);
	}
	
	//Delete DocumentMapping
	@DeleteMapping("{id}")
	public ResponseEntity<DocumentMappingDto> deleteDocumentMapping(@PathVariable Long id) {
		return documentMappingService.deleteDocumentMapping(id);
	}	
}