package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.dto.si.Gender.CreateGender;
import id.equity.nichemarket.dto.si.Gender.GenderDto;
import id.equity.nichemarket.service.es.GenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/genders")
public class GenderController {

	@Autowired
	private GenderService genderService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<GenderDto>> listGender(){
		return genderService.listGender();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<GenderDto> getGenderById(@PathVariable Long id) {
		return genderService.getGenderById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<GenderDto> addGender(@RequestBody CreateGender newGender) {
		return genderService.addGender(newGender);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<GenderDto> editGender(@RequestBody CreateGender updateGender, @PathVariable Long id) {
		return genderService.editGender(updateGender, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<GenderDto> deleteInvest(@PathVariable Long id) {
		return genderService.deleteGender(id);
	}	
}