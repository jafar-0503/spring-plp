package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajAgenStatement.CreateSpajAgenStatement;
import id.equity.nichemarket.dto.es.SpajAgenStatement.SpajAgenStatementDto;
import id.equity.nichemarket.service.es.SpajAgenStatementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-agen-statements")
public class SpajAgenStatementController {

    @Autowired
    private SpajAgenStatementService spajAgenStatementService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajAgenStatementDto>> listSpajAgenStatement(){
        return spajAgenStatementService.listSpajAgenStatement();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajAgenStatementDto> getSpajAgenStatementById(@PathVariable Long id) {
        return spajAgenStatementService.getSpajAgenStatementById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajAgenStatementDto> addSpajAgenStatement (@RequestBody CreateSpajAgenStatement newSpajAgenStatement) {
        return spajAgenStatementService.addSpajAgenStatement(newSpajAgenStatement);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajAgenStatementDto> editSpajAgenStatement(@RequestBody CreateSpajAgenStatement updateSpajAgenStatement, @PathVariable Long id) {
        return spajAgenStatementService.editSpajAgenStatement(updateSpajAgenStatement, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajAgenStatementDto> deleteSpajAgenStatement(@PathVariable Long id) {
        return spajAgenStatementService.deleteSpajAgenStatement(id);
    }
}