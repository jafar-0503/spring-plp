package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.Purpose.CreatePurpose;
import id.equity.nichemarket.dto.es.Purpose.PurposeDto;
import id.equity.nichemarket.service.es.PurposeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/purposes")
public class PurposeController {
    @Autowired
    private PurposeService PurposeService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<PurposeDto>> listPurpose(){
        return PurposeService.listPurpose();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<PurposeDto> getPurposeById(@PathVariable Long id) {
        return PurposeService.getPurposeById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<PurposeDto> addPurpose (@RequestBody CreatePurpose newPurpose) {
        return PurposeService.addPurpose(newPurpose);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<PurposeDto> editPurpose(@RequestBody CreatePurpose updatePurpose, @PathVariable Long id) {
        return PurposeService.editPurpose(updatePurpose, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<PurposeDto> deletePurpose(@PathVariable Long id) {
        return PurposeService.deletePurpose(id);
    }
}