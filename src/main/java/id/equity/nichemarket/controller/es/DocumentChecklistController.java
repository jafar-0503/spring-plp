package id.equity.nichemarket.controller.es;

import java.util.List;


import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.DocumentChecklist.CreateDocumentChecklist;
import id.equity.nichemarket.dto.es.DocumentChecklist.DocumentChecklistDto;
import id.equity.nichemarket.service.es.DocumentChecklistService;

@RestController
@RequestMapping("api/v1/document-checklists")
public class DocumentChecklistController {
	
	@Autowired
	private DocumentChecklistService documentChecklistService;
	
	//Get All DocumentChecklist
	@GetMapping
	public ResponseEntity<List<DocumentChecklistDto>> listDocumentChecklist(){
		return documentChecklistService.listDocumentChecklist();
	}
	
	//Get DocumentChecklist By Id
	@GetMapping("{id}")
	public ResponseEntity<DocumentChecklistDto> getDocumentChecklistById(@PathVariable Long id) {
		return documentChecklistService.getDocumentChecklistById(id);
	}
	
	//Post DocumentChecklist
	@PostMapping
	public ResponseEntity<DocumentChecklistDto> addDocumentChecklist(@RequestBody CreateDocumentChecklist newDocumentChecklist) {
		return documentChecklistService.addDocumentChecklist(newDocumentChecklist);
	}
	
	//Put DocumentChecklist
	@PutMapping("{id}")
	public ResponseEntity<DocumentChecklistDto> editDocumentChecklist(@RequestBody CreateDocumentChecklist updateDocumentChecklist, @PathVariable Long id) {
		return documentChecklistService.editDocumentChecklist(updateDocumentChecklist, id);
	}
	
	//Delete DocumentChecklist
	@DeleteMapping("{id}")
	public ResponseEntity<DocumentChecklistDto> deleteDocumentChecklist(@PathVariable Long id) {
		return documentChecklistService.deleteDocumentChecklist(id);
	}	
}