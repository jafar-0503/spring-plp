package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.Currency.CurrencyDto;
import id.equity.nichemarket.dto.es.Currency.CreateCurrency;
import id.equity.nichemarket.service.es.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/currencies")
public class CurrencyController {
    @Autowired
    private CurrencyService currencyService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<CurrencyDto>> listCurrency(){
        return currencyService.listCurrency();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<CurrencyDto> getCurrencyById(@PathVariable Long id) {
        return currencyService.getCurrencyById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<CurrencyDto> addCurrency (@RequestBody CreateCurrency newCurrency) {
        return currencyService.addCurrency(newCurrency);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<CurrencyDto> editCurrency(@RequestBody CreateCurrency updateCurrency, @PathVariable Long id) {
        return currencyService.editCurrency(updateCurrency, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<CurrencyDto> deleteCurrency(@PathVariable Long id) {
        return currencyService.deleteCurrency(id);
    }
}