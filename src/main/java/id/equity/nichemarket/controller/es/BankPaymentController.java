package id.equity.nichemarket.controller.es;

import java.util.List;


import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.BankPayment.BankPaymentDto;
import id.equity.nichemarket.dto.es.BankPayment.CreateBankPayment;
import id.equity.nichemarket.service.es.BankPaymentService;

@RestController
@RequestMapping("api/v1/bank-payments")
public class BankPaymentController {
	
	@Autowired
	private BankPaymentService bankPaymentService;
	
	//Get All BankPayment
	@GetMapping
	public ResponseEntity<List<BankPaymentDto>> listBankPayment(){
		return bankPaymentService.listBankPayment();
	}
	
	//Get BankPayment By Id
	@GetMapping("{id}")
	public ResponseEntity<BankPaymentDto> getBankPaymentById(@PathVariable Long id) {
		return bankPaymentService.getBankPaymentById(id);
	}
	
	//Post BankPayment
	@PostMapping
	public ResponseEntity<BankPaymentDto> addBankPayment(@RequestBody CreateBankPayment newBankPayment) {
		return bankPaymentService.addBankPayment(newBankPayment);
	}
	
	//Put BankPayment
	@PutMapping("{id}")
	public ResponseEntity<BankPaymentDto> editBankPayment(@RequestBody CreateBankPayment updateBankPayment, @PathVariable Long id) {
		return bankPaymentService.editBankPayment(updateBankPayment, id);
	}
	
	//Delete BankPayment
	@DeleteMapping("{id}")
	public ResponseEntity<BankPaymentDto> deleteBankPayment(@PathVariable Long id) {
		return bankPaymentService.deleteBankPayment(id);
	}	
}