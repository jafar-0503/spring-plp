package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajSourceType.CreateSpajSourceType;
import id.equity.nichemarket.dto.es.SpajSourceType.SpajSourceTypeDto;
import id.equity.nichemarket.service.es.SpajSourceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-source-types")
public class SpajSourceTypeController {
    @Autowired
    private SpajSourceTypeService spajSourceTypeService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajSourceTypeDto>> listSpajSourceType(){
        return spajSourceTypeService.listSpajSourceType();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajSourceTypeDto> getSpajSourceTypeById(@PathVariable Long id) {
        return spajSourceTypeService.getSpajSourceTypeById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajSourceTypeDto> addSpajSourceType (@RequestBody CreateSpajSourceType newSpajSourceType) {
        return spajSourceTypeService.addSpajSourceType(newSpajSourceType);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajSourceTypeDto> editSpajSourceType(@RequestBody CreateSpajSourceType updateSpajSourceType, @PathVariable Long id) {
        return spajSourceTypeService.editSpajSourceType(updateSpajSourceType, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajSourceTypeDto> deleteSpajSourceType(@PathVariable Long id) {
        return spajSourceTypeService.deleteSpajSourceType(id);
    }
}
