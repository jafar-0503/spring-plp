package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SourceFund.CreateSourceFund;
import id.equity.nichemarket.dto.es.SourceFund.SourceFundDto;
import id.equity.nichemarket.service.es.SourceFundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/source-funds")
public class SourceFundController {
    @Autowired
    private SourceFundService sourceFundService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SourceFundDto>> listSourceFund(){
        return sourceFundService.listSourceFund();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SourceFundDto> getSourceFundById(@PathVariable Long id) {
        return sourceFundService.getSourceFundById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SourceFundDto> addSourceFund (@RequestBody CreateSourceFund newSourceFund) {
        return sourceFundService.addSourceFund(newSourceFund);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SourceFundDto> editSourceFund(@RequestBody CreateSourceFund updateSourceFund, @PathVariable Long id) {
        return sourceFundService.editSourceFund(updateSourceFund, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SourceFundDto> deleteSourceFund(@PathVariable Long id) {
        return sourceFundService.deleteSourceFund(id);
    }
}