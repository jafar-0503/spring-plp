package id.equity.nichemarket.controller.es;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.Proposal.CreateProposal;
import id.equity.nichemarket.dto.es.Proposal.ProposalDto;
import id.equity.nichemarket.service.es.ProposalService;

@RestController
@RequestMapping("api/v1/proposals")
public class ProposalController {
	
	@Autowired
	private ProposalService proposalService;
	
	//Get All Proposal
	@GetMapping
	public ResponseEntity<List<ProposalDto>> listProposal(){
		return proposalService.listProposal();
	}
	
	//Get Proposal By Id
	@GetMapping("{id}")
	public ResponseEntity<ProposalDto> getProposalById(@PathVariable Long id) {
		return proposalService.getProposalById(id);
	}
	
	//Post Proposal
	@PostMapping
	public ResponseEntity<ProposalDto> addProposal(@RequestBody CreateProposal newProposal) {
		return proposalService.addProposal(newProposal);
	}
	
	//Put Proposal
	@PutMapping("{id}")
	public ResponseEntity<ProposalDto> editProposal(@RequestBody CreateProposal updateProposal, @PathVariable Long id) {
		return proposalService.editProposal(updateProposal, id);
	}
	
	//Delete Proposal
	@DeleteMapping("{id}")
	public ResponseEntity<ProposalDto> deleteProposal(@PathVariable Long id) {
		return proposalService.deleteProposal(id);
	}	
}