package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.dto.es.Image.CreateImage;
import id.equity.nichemarket.dto.es.Image.ImageDto;
import id.equity.nichemarket.service.es.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/images")
public class ImageController {
    @Autowired
    private ImageService imageService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<ImageDto>> listImage(){
        return imageService.listImage();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<ImageDto> getImageById(@PathVariable Long id) {
        return imageService.getImageById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<ImageDto> addImage (@RequestBody CreateImage newImage) {
        return imageService.addImage(newImage);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<ImageDto> editImage(@RequestBody CreateImage updateImage, @PathVariable Long id) {
        return imageService.editImage(updateImage, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<ImageDto> deleteImage(@PathVariable Long id) {
        return imageService.deleteImage(id);
    }
}