package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajBeneficiary.CreateSpajBeneficiary;
import id.equity.nichemarket.dto.es.SpajBeneficiary.SpajBeneficiaryDto;
import id.equity.nichemarket.service.es.SpajBeneficiaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-beneficiaries")
public class SpajBeneficiaryController {
    @Autowired
    private SpajBeneficiaryService spajBeneficiaryService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajBeneficiaryDto>> listSpajBeneficiary(){
        return spajBeneficiaryService.listSpajBeneficiary();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajBeneficiaryDto> getSpajBeneficiaryById(@PathVariable Long id) {
        return spajBeneficiaryService.getSpajBeneficiaryById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajBeneficiaryDto> addSpajBeneficiary (@RequestBody CreateSpajBeneficiary newSpajBeneficiary) {
        return spajBeneficiaryService.addSpajBeneficiary(newSpajBeneficiary);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajBeneficiaryDto> editSpajBeneficiary(@RequestBody CreateSpajBeneficiary updateSpajBeneficiary, @PathVariable Long id) {
        return spajBeneficiaryService.editSpajBeneficiary(updateSpajBeneficiary, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajBeneficiaryDto> deleteSpajBeneficiary(@PathVariable Long id) {
        return spajBeneficiaryService.deleteSpajBeneficiary(id);
    }
}