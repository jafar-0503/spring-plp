package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajHealth.CreateSpajHealth;
import id.equity.nichemarket.dto.es.SpajHealth.SpajHealthDto;
import id.equity.nichemarket.service.es.SpajHealthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-healths")
public class SpajHealthController {
    @Autowired
    private SpajHealthService spajHealthService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajHealthDto>> listSpajHealth(){
        return spajHealthService.listSpajHealth();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajHealthDto> getSpajHealthById(@PathVariable Long id) {
        return spajHealthService.getSpajHealthById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajHealthDto> addSpajHealth (@RequestBody CreateSpajHealth newSpajHealth) {
        return spajHealthService.addSpajHealth(newSpajHealth);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajHealthDto> editSpajHealth(@RequestBody CreateSpajHealth updateSpajHealth, @PathVariable Long id) {
        return spajHealthService.editSpajHealth(updateSpajHealth, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajHealthDto> deleteSpajHealth(@PathVariable Long id) {
        return spajHealthService.deleteSpajHealth(id);
    }
}