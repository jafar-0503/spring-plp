package id.equity.nichemarket.controller.rm;

import id.equity.nichemarket.dto.rm.Menu.CreateMenu;
import id.equity.nichemarket.dto.rm.Menu.MenuDto;
import id.equity.nichemarket.service.rm.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/menus")
public class MenuController {

	@Autowired
	private MenuService menuService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<MenuDto>> listMenu(){
		return menuService.listMenu();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<MenuDto> getMenuById(@PathVariable Long id) {
		return menuService.getMenuById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<MenuDto> addMenu(@RequestBody CreateMenu newMenu) {
		return menuService.addMenu(newMenu);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<MenuDto> editMenu(@RequestBody CreateMenu updateMenu, @PathVariable Long id) {
		return menuService.editMenu(updateMenu, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<MenuDto> deleteMenu(@PathVariable Long id) {
		return menuService.deleteMenu(id);
	}	
}