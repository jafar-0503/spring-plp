package id.equity.nichemarket.controller.rm;

import id.equity.nichemarket.dto.rm.UserMenu.CreateUserMenu;
import id.equity.nichemarket.dto.rm.UserMenu.UserMenuDto;
import id.equity.nichemarket.service.rm.UserMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/user-menus")
public class UserMenuController {

	@Autowired
	private UserMenuService userMenuService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<UserMenuDto>> listUserMenu(){
		return userMenuService.listUserMenu();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<UserMenuDto> getUserMenuById(@PathVariable Long id) {
		return userMenuService.getUserMenuById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<UserMenuDto> addUserMenu(@RequestBody CreateUserMenu newUserMenu) {
		return userMenuService.addUserMenu(newUserMenu);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<UserMenuDto> editUserMenu(@RequestBody CreateUserMenu updateUserMenu, @PathVariable Long id) {
		return userMenuService.editUserMenu(updateUserMenu, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<UserMenuDto> deleteUserMenu(@PathVariable Long id) {
		return userMenuService.deleteUserMenu(id);
	}	
}