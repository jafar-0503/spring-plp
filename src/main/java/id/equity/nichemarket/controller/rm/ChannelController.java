package id.equity.nichemarket.controller.rm;

import id.equity.nichemarket.dto.rm.Channel.CreateChannel;
import id.equity.nichemarket.dto.rm.Channel.ChannelDto;
import id.equity.nichemarket.service.rm.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/channels")
public class ChannelController {

	@Autowired
	private ChannelService channelService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<ChannelDto>> listChannel(){
		return channelService.listChannel();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<ChannelDto> getChannelById(@PathVariable Long id) {
		return channelService.getChannelById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<ChannelDto> addChannel(@RequestBody CreateChannel newChannel) {
		return channelService.addChannel(newChannel);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<ChannelDto> editChannel(@RequestBody CreateChannel updateChannel, @PathVariable Long id) {
		return channelService.editChannel(updateChannel, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<ChannelDto> deleteInvest(@PathVariable Long id) {
		return channelService.deleteChannel(id);
	}	
}