package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRuleCore.CreateProductRuleCore;
import id.equity.nichemarket.dto.si.ProductRuleCore.ProductRuleCoreDto;
import id.equity.nichemarket.service.si.ProductRuleCoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-cores")
public class ProductRuleCoreController {
	
	@Autowired
	private ProductRuleCoreService productRuleCoreService;
	
	//get All ProductRuleCore
	@GetMapping
	public ResponseEntity<List<ProductRuleCoreDto>> listProductRuleCore(){
		return productRuleCoreService.listProductRuleCore();
	}
	
	//Get ProductRuleCore By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleCoreDto> getProductRuleCoreById(@PathVariable Long id) {
		return productRuleCoreService.getProductRuleCoreById(id);
	}
	
	//Post ProductRuleCore
	@PostMapping
	public ResponseEntity<ProductRuleCoreDto> addProductRuleCore(@RequestBody CreateProductRuleCore id) {
		return productRuleCoreService.addProductRuleCore(id);
	}
	
	//Edit ProductRuleCore
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleCoreDto> editProductRuleCore(@RequestBody CreateProductRuleCore updateProductRuleCore, @PathVariable Long id) {
		return productRuleCoreService.editProductRuleCore(updateProductRuleCore, id);
	}
	
	//Delete ProductRuleCore
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleCoreDto> deleteProductRuleCore(@PathVariable Long id) {
		return productRuleCoreService.deleteProductRuleCore(id);
	}
}