package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.PaymentPeriod.CreatePaymentPeriod;
import id.equity.nichemarket.dto.si.PaymentPeriod.PaymentPeriodDto;
import id.equity.nichemarket.service.si.PaymentPeriodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/payment-periods")
public class PaymentPeriodController {

	@Autowired
	private PaymentPeriodService paymentPeriodService;
	
	//Get Payment Period
	@GetMapping
	public ResponseEntity<List<PaymentPeriodDto>> listPaymentPeriod() {
		return paymentPeriodService.listPaymentPeriod();
	}
	
	//Get Payment Period Id
	@GetMapping("{id}")
	public ResponseEntity<PaymentPeriodDto> getPaymentPeriodById(@PathVariable Long id) {
		return paymentPeriodService.getPaymentPeriodById(id);
	}
	
	//Post Payment Period
	@PostMapping
	public ResponseEntity<PaymentPeriodDto> addPaymentPeriod(@RequestBody CreatePaymentPeriod newPaymentPeriod) {
		return paymentPeriodService.addPaymentPeriod(newPaymentPeriod);
	}
	
	//Put Payment Period
	@PutMapping("{id}")
	public ResponseEntity<PaymentPeriodDto> editPaymentPeriod(@RequestBody CreatePaymentPeriod updatePaymentPeriod, @PathVariable Long id) {
		return paymentPeriodService.editPaymentPeriod(updatePaymentPeriod, id);
	}
	
	//Delete Payment Period
	@DeleteMapping("{id}")
	public ResponseEntity<PaymentPeriodDto> deletePaymentPeriod(@PathVariable Long id) {
		return paymentPeriodService.deletePaymentPeriod(id);
	}
}