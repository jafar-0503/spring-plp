package id.equity.nichemarket.controller.si;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import id.equity.nichemarket.dto.si.BaseRuleAs.BaseRuleAsDto;
import id.equity.nichemarket.dto.si.BaseRuleAs.CreateBaseRuleAs;
import id.equity.nichemarket.service.si.BaseRuleAsService;

@RestController
@RequestMapping("api/v1/base-rule-as")
public class BaseRuleAsController {
	
	@Autowired
	private BaseRuleAsService baseRuleAsService;
	
	//Get All BaseRuleAs
	@GetMapping
	public ResponseEntity<List<BaseRuleAsDto>> listBaseRuleAs(){
		return baseRuleAsService.listBaseRuleAs();
	}
	
	//Get BaseRuleAs By Id
	@GetMapping("{id}")
	public ResponseEntity<BaseRuleAsDto> getBaseRuleAsById(@PathVariable Long id) {
		return baseRuleAsService.getBaseRuleAsById(id);
	}
	
	//Post BaseRuleAs
	@PostMapping
	public ResponseEntity<BaseRuleAsDto> addBaseRuleAs(@RequestBody CreateBaseRuleAs newBaseRuleAs) {
		return baseRuleAsService.addBaseRuleAs(newBaseRuleAs);
	}
	
	//Put BaseRuleAs
	@PutMapping("{id}")
	public ResponseEntity<BaseRuleAsDto> editBaseRuleAs(@RequestBody CreateBaseRuleAs updateBaseruleAs, @PathVariable Long id) {
		return baseRuleAsService.editBaseRuleAs(updateBaseruleAs, id);
	}
	
	//Delete BaseRuleAs
	@DeleteMapping("{id}")
	public ResponseEntity<BaseRuleAsDto> deleteBaseRuleAs(@PathVariable Long id) {
		return baseRuleAsService.deleteBaseRuleAs(id);
	}	
}