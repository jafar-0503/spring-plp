package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRuleBenefitItem.CreateProductRuleBenefitItem;
import id.equity.nichemarket.dto.si.ProductRuleBenefitItem.ProductRuleBenefitItemDto;
import id.equity.nichemarket.service.si.ProductRuleBenefitItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-benefit-items")
public class ProductRuleBenefitItemController {

	@Autowired
	private ProductRuleBenefitItemService productRuleBenefitItemService;

	//get All ProductRuleBenefitItem
	@GetMapping
	public ResponseEntity<List<ProductRuleBenefitItemDto>> listProductRuleBenefitItem(){
		return productRuleBenefitItemService.listProductRuleBenefitItem();
	}

	//Get ProductRuleBenefit By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleBenefitItemDto> getProductRuleBenefitItemById(@PathVariable Long id) {
		return productRuleBenefitItemService.getProductRuleBenefitItemById(id);
	}

	//Post ProductRuleBenefit
	@PostMapping
	public ResponseEntity<ProductRuleBenefitItemDto> addProductRuleBenefitItem(@RequestBody CreateProductRuleBenefitItem id) {
		return productRuleBenefitItemService.addProductRuleBenefitItem(id);
	}

	//Edit ProductRuleBenefit
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleBenefitItemDto> editProductRuleBenefitItem(@RequestBody CreateProductRuleBenefitItem updateProductRuleBenefitItem, @PathVariable Long id) {
		return productRuleBenefitItemService.editProductRuleBenefitItem(updateProductRuleBenefitItem, id);
	}

	//Delete ProductRuleBenefit
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleBenefitItemDto> deleteProductRuleBenefitItem(@PathVariable Long id) {
		return productRuleBenefitItemService.deleteProductRuleBenefitItem(id);
	}
}