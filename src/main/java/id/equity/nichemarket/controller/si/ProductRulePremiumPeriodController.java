package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRulePremiumPeriod.CreateProductRulePremiumPeriod;
import id.equity.nichemarket.dto.si.ProductRulePremiumPeriod.ProductRulePremiumPeriodDto;
import id.equity.nichemarket.service.si.ProductRulePremiumPeriodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-premium-periods")
public class ProductRulePremiumPeriodController {
	
	@Autowired
	private ProductRulePremiumPeriodService productRulePremiumPeriodService;
	
	//get All ProductRulePremiumPeriod
	@GetMapping
	public ResponseEntity<List<ProductRulePremiumPeriodDto>> listProductRulePremiumPeriod(){
		return productRulePremiumPeriodService.listProductRulePremiumPeriod();
	}
	
	//Get ProductRulePremiumPeriod By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRulePremiumPeriodDto> getProductRulePremiumPeriodById(@PathVariable Long id) {
		return productRulePremiumPeriodService.getProductRulePremiumPeriodById(id);
	}
	
	//Post ProductRulePremiumPeriod
	@PostMapping
	public ResponseEntity<ProductRulePremiumPeriodDto> addProductRulePremiumPeriod(@RequestBody CreateProductRulePremiumPeriod id) {
		return productRulePremiumPeriodService.addProductRulePremiumPeriod(id);
	}
	
	//Edit ProductRulePremiumPeriod
	@PutMapping("{id}")
	public ResponseEntity<ProductRulePremiumPeriodDto> editProductRulePremiumPeriod(@RequestBody CreateProductRulePremiumPeriod updateProductRulePremiumPeriod, @PathVariable Long id) {
		return productRulePremiumPeriodService.editProductRulePremiumPeriod(updateProductRulePremiumPeriod, id);
	}
	
	//Delete ProductRulePremiumPeriod
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRulePremiumPeriodDto> deleteProductRulePremiumPeriod(@PathVariable Long id) {
		return productRulePremiumPeriodService.deleteProductRulePremiumPeriod(id);
	}
}