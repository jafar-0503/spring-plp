package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRuleSumInsured.CreateProductRuleSumInsured;
import id.equity.nichemarket.dto.si.ProductRuleSumInsured.ProductRuleSumInsuredDto;
import id.equity.nichemarket.service.si.ProductRuleSumInsuredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-sum-insureds")
public class ProductRuleSumInsuredController {
	
	@Autowired
	private ProductRuleSumInsuredService productRuleSumInsuredService;
	
	//get All ProductRuleSumInsured
	@GetMapping
	public ResponseEntity<List<ProductRuleSumInsuredDto>> listProductRuleSumInsured(){
		return productRuleSumInsuredService.listProductRuleSumInsured();
	}
	
	//Get ProductRuleSumInsured By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleSumInsuredDto> getProductRuleSumInsuredById(@PathVariable Long id) {
		return productRuleSumInsuredService.getProductRuleSumInsuredById(id);
	}
	
	//Post ProductRuleSumInsured
	@PostMapping
	public ResponseEntity<ProductRuleSumInsuredDto> addProductRuleSumInsured(@RequestBody CreateProductRuleSumInsured id) {
		return productRuleSumInsuredService.addProductRuleSumInsured(id);
	}
	
	//Edit ProductRuleSumInsured
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleSumInsuredDto> editProductRuleSumInsured(@RequestBody CreateProductRuleSumInsured updateProductRuleSumInsured, @PathVariable Long id) {
		return productRuleSumInsuredService.editProductRuleSumInsured(updateProductRuleSumInsured, id);
	}
	
	//Delete ProductRuleSumInsured
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleSumInsuredDto> deleteProductRuleSumInsured(@PathVariable Long id) {
		return productRuleSumInsuredService.deleteProductRuleSumInsured(id);
	}
}