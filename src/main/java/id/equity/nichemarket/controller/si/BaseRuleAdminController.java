package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRuleAdmin.BaseRuleAdminDto;
import id.equity.nichemarket.dto.si.BaseRuleAdmin.CreateBaseRuleAdmin;
import id.equity.nichemarket.service.si.BaseRuleAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-admin")
public class BaseRuleAdminController {
	
	@Autowired
	private BaseRuleAdminService baseRuleAdminService;
	
	//Get All BaseRule
	@GetMapping
	public ResponseEntity<List<BaseRuleAdminDto>> listBaseRuleAdmin(){
		return baseRuleAdminService.listBaseRuleAdmin();
	}
	
	//Get BaseRule By Id
	@GetMapping("{id}")
	public ResponseEntity<BaseRuleAdminDto> getBaseRuleAdminById(@PathVariable Long id) {
		return baseRuleAdminService.getBaseRuleAdminById(id);
	}
	
	//Post BaseRule
	@PostMapping
	public ResponseEntity<BaseRuleAdminDto> addBaseRuleAdmin(@RequestBody CreateBaseRuleAdmin newBaseRule) {
		return baseRuleAdminService.addBaseRuleAdmin(newBaseRule);
	}
	
	//Put BaseRule
	@PutMapping("{id}")
	public ResponseEntity<BaseRuleAdminDto> editBaseRuleAdmin(@RequestBody CreateBaseRuleAdmin updateBaserule, @PathVariable Long id) {
		return baseRuleAdminService.editBaseRuleAdmin(updateBaserule, id);
	}
	
	//Delete BaseRule
	@DeleteMapping("{id}")
	public ResponseEntity<BaseRuleAdminDto> deleteBaseRuleAdmin(@PathVariable Long id) {
		return baseRuleAdminService.deleteBaseRuleAdmin(id);
	}	
}