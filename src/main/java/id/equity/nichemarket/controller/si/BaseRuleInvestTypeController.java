package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.BaseRuleInvestType.BaseRuleInvestTypeDto;
import id.equity.nichemarket.dto.si.BaseRuleInvestType.CreateBaseRuleInvestType;
import id.equity.nichemarket.service.si.BaseRuleInvestTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-invest-types")
public class BaseRuleInvestTypeController {
    @Autowired
    private BaseRuleInvestTypeService baseRuleInvestTypeService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<BaseRuleInvestTypeDto>> listBaseRuleInvestType(){
        return baseRuleInvestTypeService.listBaseRuleInvestType();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<BaseRuleInvestTypeDto> getBaseRuleInvestTypeById(@PathVariable Long id) {
        return baseRuleInvestTypeService.getBaseRuleInvestTypeById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<BaseRuleInvestTypeDto> addBaseRuleInvestType(@RequestBody CreateBaseRuleInvestType newBaseRuleInvestType) {
        return baseRuleInvestTypeService.addBaseRuleInvestType(newBaseRuleInvestType);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<BaseRuleInvestTypeDto> editBaseRuleInvestType(@RequestBody CreateBaseRuleInvestType updateBaseRuleInvestType, @PathVariable Long id) {
        return baseRuleInvestTypeService.editBaseRuleInvestType(updateBaseRuleInvestType, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<BaseRuleInvestTypeDto> deleteBaseRuleInvestType(@PathVariable Long id) {
        return baseRuleInvestTypeService.deleteBaseRuleInvestType(id);
    }
}