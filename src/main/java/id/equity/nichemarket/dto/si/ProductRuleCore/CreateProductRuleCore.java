package id.equity.nichemarket.dto.si.ProductRuleCore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleCore extends ProductRuleCoreDto {

}
