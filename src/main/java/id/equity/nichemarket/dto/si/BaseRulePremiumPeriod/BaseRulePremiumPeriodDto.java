package id.equity.nichemarket.dto.si.BaseRulePremiumPeriod;

import lombok.Data;

@Data
public class BaseRulePremiumPeriodDto {
    private Long id;
    private String baseRulePremiumPeriodCode;
    private String productCode;
    private String paymentPeriodCode;
    private Integer min;
    private Integer max;
    private Integer step;
    private Integer maxAge;
    private boolean isActive;
}
