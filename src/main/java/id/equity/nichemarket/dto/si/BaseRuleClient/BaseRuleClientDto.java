package id.equity.nichemarket.dto.si.BaseRuleClient;

import lombok.Data;

import javax.persistence.Column;

@Data
public class BaseRuleClientDto {
    private Long id;
    private String baseRuleClientCode;
    private String productCode;
    private String asCode;
    private Integer totClient;
    private boolean isRequired;
    private boolean isActive;
}
