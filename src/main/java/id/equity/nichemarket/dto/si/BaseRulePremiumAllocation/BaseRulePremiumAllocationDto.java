package id.equity.nichemarket.dto.si.BaseRulePremiumAllocation;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseRulePremiumAllocationDto{

	private Long id;
	private String baseRulePremiumAllocationCode;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private Double premiumPercent;
	private Double topupPercent;
	private boolean isActive;
}
