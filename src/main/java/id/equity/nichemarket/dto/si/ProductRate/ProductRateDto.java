package id.equity.nichemarket.dto.si.ProductRate;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductRateDto{

	private long id;
	private String productRateCode;
	private String category;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private String asCode;
	private String genderCode;
	private Integer ageMin;
	private Integer ageMax;
	private Integer contractPeriod;
	private Integer premiumPeriod;
	private Integer classs;
	private Integer risk;
	private Double rate;
	private boolean isActive;
}