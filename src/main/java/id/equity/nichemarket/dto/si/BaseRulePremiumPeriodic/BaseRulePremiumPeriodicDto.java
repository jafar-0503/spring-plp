package id.equity.nichemarket.dto.si.BaseRulePremiumPeriodic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseRulePremiumPeriodicDto {
	
	private long id;
	private String baseRulePremiumPeriodicCode;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private Double minPremium;
	private Double maxPremium;
	private Double stepPremium;
	private boolean isActive;
}
