package id.equity.nichemarket.dto.si.ProductRuleGrouping;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleGrouping extends ProductRuleGroupingDto{

}
