package id.equity.nichemarket.dto.si.ProductRuleNote;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleNote extends ProductRuleNoteDto {

}
