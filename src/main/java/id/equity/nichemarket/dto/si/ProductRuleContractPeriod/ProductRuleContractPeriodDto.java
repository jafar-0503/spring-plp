package id.equity.nichemarket.dto.si.ProductRuleContractPeriod;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ProductRuleContractPeriodDto {
	
	private long id;
	private String productRuleContractPeriodCode;
	private String productCode;
	private String minContractPeriod;
	private String maxContractPeriod;
	private Integer stepContractPeriod;
	private Integer maxAgeContractPeriod;
	private boolean isActive;
}