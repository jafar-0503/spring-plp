package id.equity.nichemarket.dto.si.ProductRulePremiumPeriod;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRulePremiumPeriod extends ProductRulePremiumPeriodDto {

}
