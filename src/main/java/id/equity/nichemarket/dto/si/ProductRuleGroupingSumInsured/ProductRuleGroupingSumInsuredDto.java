package id.equity.nichemarket.dto.si.ProductRuleGroupingSumInsured;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductRuleGroupingSumInsuredDto {
	
	private long id;
	private String productRuleGroupingSumInsuredCode;
	private String productCode;
	private String groupingCode;
	private Double maxSumInsured;
	private boolean isActive;
}