package id.equity.nichemarket.dto.si.ProductChannel;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductChannel extends ProductChannelDto{

}
