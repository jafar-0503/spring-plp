package id.equity.nichemarket.dto.si.BaseRulePolicyFee;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseRulePolicyFeeDto {
	
	private long id;
	private String baseRulePolicyFeeCode;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private Double rate;
	private boolean isActive;
}
