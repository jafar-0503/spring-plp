package id.equity.nichemarket.dto.si.InvestType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateInvestType extends InvestTypeDto {
	
}
