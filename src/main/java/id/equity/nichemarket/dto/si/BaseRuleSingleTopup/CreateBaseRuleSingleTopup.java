package id.equity.nichemarket.dto.si.BaseRuleSingleTopup;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateBaseRuleSingleTopup extends BaseRuleSingleTopupDto{

}
