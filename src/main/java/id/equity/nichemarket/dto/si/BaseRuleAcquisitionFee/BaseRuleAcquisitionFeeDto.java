package id.equity.nichemarket.dto.si.BaseRuleAcquisitionFee;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseRuleAcquisitionFeeDto {
	private Long id;
	private String baseRuleAcquisitionFeeCode;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private int yearTh;
	private Double rate;
	private boolean isActive;
}
