package id.equity.nichemarket.dto.si.BaseRuleAcquisitionFee;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateBaseRuleAcquisitionFee extends BaseRuleAcquisitionFeeDto{

}
