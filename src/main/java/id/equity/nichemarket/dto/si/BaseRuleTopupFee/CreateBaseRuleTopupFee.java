package id.equity.nichemarket.dto.si.BaseRuleTopupFee;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateBaseRuleTopupFee extends BaseRuleTopupFeeDto {
}
