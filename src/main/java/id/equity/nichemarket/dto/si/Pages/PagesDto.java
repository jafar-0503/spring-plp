package id.equity.nichemarket.dto.si.Pages;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class PagesDto {
	private Long id;
	private String pageCode;
	private String description;
	private Integer order;
	private boolean isActive;
}
