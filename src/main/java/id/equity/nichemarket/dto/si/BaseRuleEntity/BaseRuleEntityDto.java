package id.equity.nichemarket.dto.si.BaseRuleEntity;

import lombok.Data;

@Data
public class BaseRuleEntityDto {
    private Long id;
    private String baseRuleEntitasCode;
    private String productCode;
    private boolean isPremiumPeriodic;
    private boolean isTopupPeriodic;
    private boolean isSingleTopup;
    private boolean isRetirement;
    private Integer retirementAge;
    private Boolean isInterestThp;
    private Integer interestThp;
    private boolean isAllocationFund;
    private boolean isMutationFund;
    private boolean isContractPeriod;
    private boolean isPremiumPeriod;
    private boolean isActive;
}
