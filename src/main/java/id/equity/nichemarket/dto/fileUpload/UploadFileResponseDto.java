package id.equity.nichemarket.dto.fileUpload;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UploadFileResponseDto {
    private String fileName;
    private String fileType;
    private Long size;

    public UploadFileResponseDto(String fileName, String fileType, Long size) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.size = size;
    }
}
