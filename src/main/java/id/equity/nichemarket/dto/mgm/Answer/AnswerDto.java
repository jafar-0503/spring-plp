package id.equity.nichemarket.dto.mgm.Answer;

import lombok.Data;

import javax.persistence.Column;

@Data
public class AnswerDto {
    private Long id;
    private String answerCode;
    private String description;
    private String questionCode;
    private Integer sequence;
    private boolean isActive;
}
