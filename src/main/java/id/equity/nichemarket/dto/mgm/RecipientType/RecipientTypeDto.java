package id.equity.nichemarket.dto.mgm.RecipientType;

import lombok.Data;

@Data
public class RecipientTypeDto {
    private Long id;
    private String recipientTypeCode;
    private String description;
    private boolean isInternal;
    private boolean isActive;
}