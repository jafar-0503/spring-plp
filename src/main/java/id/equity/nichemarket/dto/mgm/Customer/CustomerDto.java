package id.equity.nichemarket.dto.mgm.Customer;

import lombok.Data;

@Data
public class CustomerDto {
    private Long id;
    private String customerCode;
    private String customerName;
    private String genderCode;
    private String dateOfBirth;
    private String occupation;
    private String phoneNo;
    private String emailAddress;
    private boolean isActive;
}
