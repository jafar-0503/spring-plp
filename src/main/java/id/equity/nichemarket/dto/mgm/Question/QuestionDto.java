package id.equity.nichemarket.dto.mgm.Question;

import lombok.Data;

import javax.persistence.Column;

@Data
public class QuestionDto {
    private Long id;
    private String questionCode;
    private String description;
    private Integer sequence;
    private boolean isActive;
}
