package id.equity.nichemarket.dto.rm.Menu;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateMenu extends MenuDto {
}
