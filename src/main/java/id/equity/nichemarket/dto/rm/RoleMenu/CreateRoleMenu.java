package id.equity.nichemarket.dto.rm.RoleMenu;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateRoleMenu extends RoleMenuDto {
}
