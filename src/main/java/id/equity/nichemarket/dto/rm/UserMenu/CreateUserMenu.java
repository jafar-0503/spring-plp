package id.equity.nichemarket.dto.rm.UserMenu;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateUserMenu extends UserMenuDto {
}
