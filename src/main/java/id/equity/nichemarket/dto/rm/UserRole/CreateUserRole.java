package id.equity.nichemarket.dto.rm.UserRole;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateUserRole extends UserRoleDto {
}
