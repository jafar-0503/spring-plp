package id.equity.nichemarket.dto.rm.GenderRM;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateGenderRM extends GenderRMDto {
}
