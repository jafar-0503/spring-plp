package id.equity.nichemarket.dto.rm.Channel;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@NoArgsConstructor
public class ChannelDto {
	
	private Long id;
	private String channelCode;
	private String channelCodeMapp;
	private String description;
	private boolean isActive;
}
