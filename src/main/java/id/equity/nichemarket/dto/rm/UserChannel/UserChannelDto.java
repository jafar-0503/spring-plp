package id.equity.nichemarket.dto.rm.UserChannel;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserChannelDto {
	
	private Long id;
	private String userChannelCode;
	private String username;
	private String channelCode;
	private boolean isActive;
}
