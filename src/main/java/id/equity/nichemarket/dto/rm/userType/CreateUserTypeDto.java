package id.equity.nichemarket.dto.rm.userType;

import lombok.Data;

@Data
public class CreateUserTypeDto extends UserTypeDto {
}
