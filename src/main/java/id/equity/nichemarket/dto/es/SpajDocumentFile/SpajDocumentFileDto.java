package id.equity.nichemarket.dto.es.SpajDocumentFile;

import lombok.Data;

@Data
public class SpajDocumentFileDto {
    private Long id;
    private String noSpajDmsCode;
    private String spajDocumentFileCode;
    private String spajDocumentCode;
    private String spajNo;
    private String dmsCode;
    private String file;
    private String documentName;
    private boolean isActive;
}
