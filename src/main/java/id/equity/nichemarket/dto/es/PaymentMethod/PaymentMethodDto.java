package id.equity.nichemarket.dto.es.PaymentMethod;

import lombok.Data;

@Data
public class PaymentMethodDto {
    
	private Long id;
	private String paymentMethodCode;
    private String description;
    private boolean isActive;
}