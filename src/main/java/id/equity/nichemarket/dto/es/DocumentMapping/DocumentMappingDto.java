package id.equity.nichemarket.dto.es.DocumentMapping;

import lombok.Data;

@Data
public class DocumentMappingDto {
    
	private Long id;
	private String documentMappingCode;
    private String mappingCode;
    private String description;
    private boolean isActive;
}