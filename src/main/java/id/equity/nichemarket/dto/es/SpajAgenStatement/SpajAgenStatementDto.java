package id.equity.nichemarket.dto.es.SpajAgenStatement;

import lombok.Data;

@Data
public class SpajAgenStatementDto {

    private Long id;
    private String spajAgenStatementCode;
    private String spajDocumentCode;
    private String spajNo;
    private String agenName;
    private String agenCode;
    private String position;
    private boolean isActive;
}
