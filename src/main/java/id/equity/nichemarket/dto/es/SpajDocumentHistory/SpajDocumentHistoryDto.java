package id.equity.nichemarket.dto.es.SpajDocumentHistory;

import lombok.Data;

@Data
public class SpajDocumentHistoryDto {

    private Long id;
    private String spajDocumentHistoryCode;
    private String spajDocumentCode;
    private String status;
    private String description;
    private boolean isActive;
}