package id.equity.nichemarket.dto.es.SpajBeneficiary;

import lombok.Data;

@Data
public class SpajBeneficiaryDto {
    private Long id;
    private String spajBeneficiaryCode;
    private String spajDocumentCode;
    private String spajNo;
    private String beneficiary1;
    private String relationship1;
    private String dateOfBirth1;
    private String gender1;
    private String percentage1;
    private boolean isActive;
}
