package id.equity.nichemarket.dto.es.AgenRelation;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateAgenRelation extends AgenRelationDto{ 

}
