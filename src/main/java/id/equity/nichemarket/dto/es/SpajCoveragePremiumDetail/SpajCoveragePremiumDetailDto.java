package id.equity.nichemarket.dto.es.SpajCoveragePremiumDetail;

import lombok.Data;

@Data
public class SpajCoveragePremiumDetailDto {
    private Long id;
    private String spajCoveragePremiumDetailCode;
    private String spajDocumentCode;
    private String spajNo;
    private String currency;
    private String premiumPaymentPeriod;
    private String pUnitLinkPremium;
    private String pUnitLinkTotalPremium;
    private String pUnitLinkBasicPlan;
    private String pUnitLinkSumInsured;
    private String pUnitLinkPremiumPaymentPeriod;
    private String pUnitLinkContractPeriod;
    private String pUnitLinkRider1;
    private String pUnitLinkSumInsuredRider1;
    private boolean isActive;
}
