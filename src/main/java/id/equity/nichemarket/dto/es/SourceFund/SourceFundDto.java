package id.equity.nichemarket.dto.es.SourceFund;

import lombok.Data;

@Data
public class SourceFundDto {
    private Long id;
    private String sourceFundCode;
    private String description;
    private boolean isActive;
}
