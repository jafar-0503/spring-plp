package id.equity.nichemarket.dto.es.Citizen;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CitizenDto {
	
	private long id;
	private String citizenCode;
	private String description;
	private boolean isActive;
}
