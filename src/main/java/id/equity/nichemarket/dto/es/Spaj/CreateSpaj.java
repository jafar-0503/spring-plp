package id.equity.nichemarket.dto.es.Spaj;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateSpaj extends SpajDto{

    public CreateSpaj(boolean is_active) {
        this.setActive(is_active);
    }
}
