package id.equity.nichemarket.dto.es.Image;

import lombok.Data;

@Data
public class ImageDto {
    private Long id;
    private String imageCode;
    private String description;
    private String objectImage;
    private boolean isActive;
}
