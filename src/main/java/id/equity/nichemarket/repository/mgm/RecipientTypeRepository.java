package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.RecipientType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipientTypeRepository extends JpaRepository<RecipientType, Long> {
}