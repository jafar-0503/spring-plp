package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SpajAdditionalStat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpajAdditionalStatRepository extends JpaRepository<SpajAdditionalStat, Long> {
}
