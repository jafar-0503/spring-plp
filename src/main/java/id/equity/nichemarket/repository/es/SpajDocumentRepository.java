package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.model.es.SpajDocumentFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpajDocumentRepository extends JpaRepository<SpajDocument, Long> {

    @Query("select s from SpajDocument s where s.spajDocumentCode = ?1")
    SpajDocument findBySpajDocumentCode(String spajDocumentCode);

    @Query("select s from SpajDocument s where s.spajNo = ?1")
    SpajDocument findBySpajNo(String spajNo);
}
