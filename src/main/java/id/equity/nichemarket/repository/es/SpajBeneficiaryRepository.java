package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SpajBeneficiary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpajBeneficiaryRepository extends JpaRepository<SpajBeneficiary, Long> {
}
