package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.AddressType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressTypeRespository extends JpaRepository<AddressType, Long> {
}
