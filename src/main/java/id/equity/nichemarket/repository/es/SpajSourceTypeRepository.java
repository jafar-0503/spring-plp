package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SpajSourceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpajSourceTypeRepository extends JpaRepository<SpajSourceType, Long> {
}
