package id.equity.nichemarket.repository.rm;

import id.equity.nichemarket.model.es.Gender;
import id.equity.nichemarket.model.rm.GenderRM;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenderRMRepository extends JpaRepository<GenderRM, Long> {

}
