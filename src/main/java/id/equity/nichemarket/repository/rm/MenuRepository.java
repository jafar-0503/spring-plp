package id.equity.nichemarket.repository.rm;

import id.equity.nichemarket.model.rm.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Long> {

}
