package id.equity.nichemarket.repository.rm;

import id.equity.nichemarket.model.rm.UserType;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTypeRepository extends PagingAndSortingRepository<UserType, Long> {
    UserType findByCode(String code);
}
