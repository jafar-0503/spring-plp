package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductRuleContractPeriod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRuleContractPeriodRepository extends JpaRepository<ProductRuleContractPeriod, Long>{

}
