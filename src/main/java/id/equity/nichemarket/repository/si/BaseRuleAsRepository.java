package id.equity.nichemarket.repository.si;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.si.BaseRuleAs;

@Repository
public interface BaseRuleAsRepository extends JpaRepository<BaseRuleAs, Long>{

}
