package id.equity.nichemarket.repository.si;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import id.equity.nichemarket.model.si.PaymentPeriod;

@Repository
public interface PaymentPeriodRepository extends JpaRepository<PaymentPeriod, Long> {

}
