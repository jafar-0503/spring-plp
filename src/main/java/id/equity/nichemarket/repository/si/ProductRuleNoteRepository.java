package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductRuleNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRuleNoteRepository extends JpaRepository<ProductRuleNote, Long>{

}
