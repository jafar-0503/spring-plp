package id.equity.nichemarket.validator.usertype;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = ExistUserTypeValidatorImpl.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface ExistUserTypeValidator {
    public String message() default "User type doesn't exist!";

    public Class<?>[] groups() default {};

    public Class<? extends Payload>[] payload() default{};
}
