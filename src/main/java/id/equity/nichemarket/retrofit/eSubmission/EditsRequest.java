package id.equity.nichemarket.service.eSubmissionStore.Interfaces;

import lombok.Data;

@Data
public class EditsRequest<T> {
    private T data;

    public EditsRequest(T data) {
        this.data = data;
    }
}
