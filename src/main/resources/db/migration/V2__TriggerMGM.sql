/*trigger Customer*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_customer_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_customer varchar;
DECLARE check_customer varchar;

BEGIN
    SELECT INTO check_customer
        LPAD(cast(cast(substring(customer_code, 8) AS integer) + 1 AS varchar), 4, '0')
    FROM t_customer
    ORDER BY id
        DESC LIMIT 1;

    IF check_customer IS NULL THEN
        new.customer_code := concat('C', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        new.customer_code := concat('C', to_char(now(), 'yymmdd' ), LPAD(check_customer, 4, '0'));
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_customer_code
    ON public.t_customer;
CREATE TRIGGER generate_customer_code
    BEFORE INSERT
    ON public.t_customer
    FOR EACH ROW
EXECUTE FUNCTION generate_customer_code();

/*trigger Answer*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_answer_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_answer varchar;
DECLARE check_answer varchar;

BEGIN
    SELECT INTO check_answer
        LPAD(cast(cast(substring(answer_code, 3) AS integer) + 1 AS varchar), 6, '0')
    FROM t_answer
    ORDER BY id
        DESC LIMIT 1;

    IF check_answer IS NULL THEN
        new.answer_code := concat('AN', LPAD('1', 6, '0'));
    ELSE
        new.answer_code := concat('AN', LPAD(check_answer, 6, '0'));
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_answer_code
    ON public.t_answer;
CREATE TRIGGER generate_answer_code
    BEFORE INSERT
    ON public.t_answer
    FOR EACH ROW
EXECUTE FUNCTION generate_answer_code();

/*trigger Log Message*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_log_message_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_log_message varchar;
DECLARE check_log_message varchar;

BEGIN
    SELECT INTO check_log_message
        LPAD(cast(cast(substring(log_message_code, 3) AS integer) + 1 AS varchar), 6, '0')
    FROM t_log_message
    ORDER BY id
        DESC LIMIT 1;

    IF check_log_message IS NULL THEN
        new.log_message_code := concat('LM', LPAD('1', 6, '0'));
    ELSE
        new.log_message_code := concat('LM', LPAD(check_log_message, 6, '0'));
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_log_message_code
    ON public.t_log_message;
CREATE TRIGGER generate_log_message_code
    BEFORE INSERT
    ON public.t_log_message
    FOR EACH ROW
EXECUTE FUNCTION generate_log_message_code();